import { HttpClientModule } from '@angular/common/http';
import { GitComponent } from './git/git.component';
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { AppPortfolioComponent } from "./app-portfolio/app-portfolio.component";
import { RouterModule } from "@angular/router";
import { appRoutes } from "./routes";
import { AppBlogSingleComponent } from "./app-blog-single/app-blog-single.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { FooterComponent } from "./footer/footer.component";
import { FormsModule } from "@angular/forms";
import { ChartsModule } from "ng2-charts";
import { RadarChartComponent } from "./aadar-chart/radar-chart.component";
import { AppPitsComponent } from "./app-pits/app-pits.component";
import { AppService } from '../service/app.service';

@NgModule({
  declarations: [
    AppComponent,
    AppPortfolioComponent,
    AppBlogSingleComponent,
    NavbarComponent,
    FooterComponent,
    RadarChartComponent,
    AppPitsComponent,
    GitComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    ChartsModule,
    HttpClientModule
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule {}
