import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-radar-chart",
  templateUrl: "./radar-chart.component.html"
})
export class RadarChartComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

    // PolarArea
    public polarAreaChartLabels:string[] = ['SQL','Dotnet Core','Download Sales', 'In-Store Sales', 'Mail Sales', 'Telesales', 'Corporate Sales'];
    public polarAreaChartData:number[] = [89,96,89, 87, 98, 95, 97];
    public polarAreaLegend:boolean = true;
   
    public polarAreaChartType:string = 'polarArea';
   
    // events
    public chartClicked(e:any):void {
      console.log(e);
    }
   
    public chartHovered(e:any):void {
      console.log(e);
    }
}
