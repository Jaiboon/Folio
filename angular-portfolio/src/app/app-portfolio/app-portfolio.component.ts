import { AppService } from "../../service/app.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-portfolio",
  templateUrl: "./app-portfolio.component.html"
})
export class AppPortfolioComponent implements OnInit {
  currentPercentage: any;
  about: About = {
    des:
      "คนที่ประสบความสำเร็จมากที่สุดในโลก คือ คนที่พบเจอความผิดพลาดและความล้มเหลวมามากกว่าคนอื่น จุดเด่นของผมคือ ชอบการเรียนรู้ พัฒนาตนเองอยู่ตลอด ความมุ่งมั่นสูง อดทน ขยัน ความฝันของผมคือยากอยู่ในจุดที่สูงที่สุด คติประจำใจคือ ความมุ่งมั่น สามารถชนะได้ทุกอย่าง ",
    des_sub:
      "ผมเติบโตในครอบครัวที่มีฐานะปานกลาง เป็นครอบครัวที่อบอุ่น พ่อแม่ของผมมีอาชีพเกษตรกร จะบอกว่าชาวนาก็ไม่ถูกเพราะที่บ้านทำเกษตรหลายอย่าง ผมมีพี่น้องสามคน ผมเป็นพี่คนโต ผมมีฮีโร่เป็นแบบอย่างของผมคือคุณตาของผม เพราะว่าท่านเป็นผู้ก่อตั้งหมู่บ้าน ผมจึงอยากทำให้ได้ในแนวทางแบบนั้น คือ ก่อตั้งกิจการ หรือบริษัท"
  };

  model: any = [
    {
      Icon: "ion-social-windows",
      Title: "DOT NET TECHNOLOGY",
      Detail:
        "เชี่ยวชาญด้านเทคโนโลยีของ Microsoft เช่น NET Core ,C#, DOTNET MVC, MSSQL, SQL Reporting service, Entity Framework, ASP, IIS"
    },
    {
      Icon: "ion-social-angular-outline",
      Title: "ANGULAR",
      Detail:
        "มีความชำนาญเกี่ยวกับการ Client website คือการทำงาน Web single page โดยใช้ Angular 2+"
    },
    {
      Icon: "ion-social-css3",
      Title: "WEB DESIGN",
      Detail:
        "ชำนาญการออกแบบเวปแบบ Responsive โดยใช้ Class พื้นฐานของ Bootstrap"
    },
    {
      Icon: "ion-ios-medkit",
      Title: "RESTFUL API",
      Detail:
        "ชำนาญด้าน Web API เพื่อให้ Client เรียกใช้งานอย่างเช่น การแปลงเป็น JSON แล้วนำไปใช้"
    },
    {
      Icon: "ion-planet",
      Title: "ARCHITECTURE",
      Detail:
        "ชำนาญด้านการออกแบบสถาปัตยกรรม ระบบ เช่น N-TIER, Onion Architecture"
    },
    {
      Icon: "ion-pull-request",
      Title: "GIT",
      Detail:
        "ชำนาญการใช้งาน GIT เพื่อจัดการ Version ของ ระบบให้สามารถ ทำงานได้แบบมีแบบแผนแล้วเป็นระบบ"
    }
  ];

  skill = [
    {
      name: "C#",
      progress: "92"
    },
    {
      name: "ASP.NET",
      progress: "90"
    },
    {
      name: "Microsoft SQL Server",
      progress: "93"
    },
    {
      name: "Angular",
      progress: "86"
    },
    {
      name: "Bootstrap",
      progress: "92"
    },
    {
      name: "Git",
      progress: "95"
    },
    {
      name: "MySql",
      progress: "86"
    },
    {
      name: "JavaScript",
      progress: "63"
    },
    {
      name: "Json",
      progress: "83"
    },
    {
      name: "Wordpress",
      progress: "86"
    },
    {
      name: "Entity Framework",
      progress: "89"
    },
    {
      name: "N-Tier",
      progress: "92"
    },
    {
      name: "Dotnet Core",
      progress: "93"
    },
    {
      name: "Restfut API",
      progress: "91"
    },
    {
      name: "SQL Server Reporting Services",
      progress: "93"
    },
    {
      name: "Internet Information Services (IIS)",
      progress: "96"
    },
    {
      name: "Node Js",
      progress: "69"
    },
    {
      name: "Swagger",
      progress: "90"
    }
  ];

  dataTest: any;
  constructor(private _appService: AppService) {
    this.currentPercentage = '60';
    this.Getdata();
  }
  ngOnInit() {
    this.GetSkill();
  }

  GetSkill() {
    this._appService.GetSkill().subscribe(
      data => {
        this.skill = data;
      },
      error => { }
    );
  }

  Getdata() {
    this._appService.GetJsonplaceholder().subscribe(
      data => {

        this.dataTest = data;
      },
      error => { }
    );
  }

  GetWaahaha() {
    this._appService.GetWaahaha().subscribe(
      data => {
        this.dataTest = data;
      },
      error => { }
    );
  }
  journal: any = [
    {
      id: '1',
      img: 'assets/images/nipon.png',
      link: 'blog',
      title: 'เรื่องราวการทำงานกับ Isabaya co.,ltd.',
      description: 'นี้คือจุเริ่มต้นชของีวิตนักพัฒนาระบบคอมพิวเตอร์ และเป็นวิถีทางการพัฒนาตนเองจากนักศึกษาจบใหม่ กลายเป็นผู้ชำนาญ'
    },
    {
      id: '2',
      img: 'assets/images/angula.png',
      link: 'pits',
      title: 'สร้าง Project แบบง่ายด้วย AngularJS 5+ และ Bootstrap',
      description: 'สิ่งที่จะได้รับเมื่ออ่านบทความนี้ จะได้รู้พื้นฐานการสร้าง Project AngularJS 5+ ส่วนของการสร้าง Component การจัดการ Selector, TemplateUrl, Routes เป็นต้น จะได้เรียนรู้วิธีการติดตั้ง Bootstrap 4'
    },
    {
      id: '3',
      img: 'assets/images/git.png',
      link: 'git',
      title: 'GIT จัดการงานให้มีขั้นตอน',
      description: 'Git คือตัวจัดการ Version ที่ออกแบบมาเพื่อจัดการกับทุกอย่างไม่ว่าจะเล็กหรือใหญ่ สามารถเข้าไปอ่านเพิ่มเติมได้ที่ Git-scm.com'
    }
  ]

}

export class About {
  des: string;
  des_sub: string;
}
