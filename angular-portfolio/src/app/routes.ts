import { Routes } from "@angular/router";
import { AppPortfolioComponent } from "./app-portfolio/app-portfolio.component";
import { AppBlogSingleComponent } from "./app-blog-single/app-blog-single.component";
import { AppPitsComponent } from "./app-pits/app-pits.component";
import { GitComponent } from "./git/git.component";

export const appRoutes: Routes = [
  { path: "home", component: AppPortfolioComponent },
  { path: "blog", component: AppBlogSingleComponent },
  { path: "pits", component: AppPitsComponent },
  { path: "git", component: GitComponent },

  { path: "**", redirectTo: "home", pathMatch: "full" }
];
