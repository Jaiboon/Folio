// import { Injectable } from "@angular/core";
// import { Observable } from "../../../node_modules/rxjs";
// import { HttpClient, HttpErrorResponse  } from "@angular/common/http";

import { Injectable } from "@angular/core";
import {
  Observable,
  Subject,
  asapScheduler,
  pipe,
  of,
  from,
  interval,
  merge,
  fromEvent,
  throwError
} from "rxjs";
import { map, filter, scan, catchError } from "rxjs/operators";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";
import { HttpClientModule } from "@angular/common/http";
import { HttpModule } from "@angular/http";

@Injectable({
  providedIn: "root"
})
export class AppService {
  API_URL: any = "http://localhost:5000";
  constructor(private http: HttpClient) {}

  public GetWaahaha(): Observable<any> {
    return this.http.get(`${"http://waahaha.xyz/api/?p=login"}`);
  }

  public GetData(): Observable<any> {
    return this.http.get(`${"http://jsonplaceholder.typicode.com/users"}`);
  }

  public GetSkill(): Observable<any> {
    return this.http.get(`${this.API_URL+'/api/skill'}`);
  }

  public GetJsonplaceholder(): Observable<any> {
    return this.http.get(`${"http://jsonplaceholder.typicode.com/todos"}`);
  }
}
