import { Angular.WebPage } from './app.po';

describe('angular.web App', () => {
  let page: Angular.WebPage;

  beforeEach(() => {
    page = new Angular.WebPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
