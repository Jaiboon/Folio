export const environment = {
  production: false,
  API_SERVER: 'http://localhost:5000',
  MEDIA_SERVER: "http://localhost:5000/MediaFiles",
  whitelistedDomains:
    [
      "localhost:5000",
      "localhost:4200"
    ]
};
