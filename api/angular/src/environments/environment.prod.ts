export const environment = {
  production: false,
  API_SERVER: 'http://45.122.50.144/backend',
  MEDIA_SERVER: "http://45.122.50.144/backend/MediaFiles",
  whitelistedDomains:
    [
      "ci.oie.go.th",
      "45.122.50.144"
    ]
};
