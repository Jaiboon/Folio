export const environment = {
  production: true,
  API_SERVER: 'http://203.151.83.60:4400/backend',
  MEDIA_SERVER: "http://203.151.83.60:4400/backend/MediaFiles",
  whitelistedDomains:
    [
      "203.151.83.60:4400",
      "203.151.83.60:4400/backend"
    ]
};
