﻿// globals.ts
import { Injectable } from '@angular/core';
import { OIEService } from './app/services';
import { environment } from './environments/environment';
import { Observable, BehaviorSubject, ObservableLike, AsyncSubject, of } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Config } from './config';
import { map, tap, catchError } from 'rxjs/operators';

export class Setting {
    industry: any[] = [];
    industryCountry: any[] = [];
    pillar: any[] = [];
    year: any[] = [];
    industryIndicator: any[] = [];
};

@Injectable()
export class Globals {

    summernote_options: SummernoteOptions = {
        toolbar: [
            ['font', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'hr']],
            ['view', ['fullscreen', 'codeview']],
            ['help', ['help']]
        ]
    };

    // Radar
    pillarLabels: any = {
        'mobileLabels': ["1", "2", "3", "4", "5", "6", "7", "8", "9"],
        'desktopLabels': [
            "Business Environment & Strategy",
            "Factor of Production",
            "Technology & Innovation",
            "Production",
            "Sustainability",
            "Management",
            "Products & Markets",
            "Performance",
            "Future Prospect"
        ]
    };


    constructor(
        private http: HttpClient,
        private chartService: OIEService
    ) {
        this.getAppSettings();
    }

    private cache = new Map<string, Observable<Setting>>();

    public currentDocument: Observable<Setting>;

    public getAppSettings() {
        if (!this.cache.has('app-setting')) {
            this.cache.set('app-setting', this.fetchSettings('app-setting'));
        }
        return this.cache.get('app-setting')!;
    }

    private fetchSettings(id: string): Observable<Setting> {
        const subject = new AsyncSubject<Setting>();

        this.http
            .get<Setting>(`${Config.API_URL}/api/setting/app-setting`)
            .pipe(
                map((res: any) => { return of(res).subscribe(subject); }
                ), catchError((error: HttpErrorResponse) => { return of({}); })
            ).subscribe(subject);

        return subject.asObservable();
    }

    getFullMediaUrl(url: string) {
        if (url == '')
            return null;
        return environment.MEDIA_SERVER + '/' + url;
    }


}