﻿import { Router } from '@angular/router';
import swal from 'sweetalert2';

import { AuthenticationService } from '../services/authentication.service';

/**
 * Provides signin method to signin & signup components.
 */
export class 
Signin {

    model: any = {};

    constructor(
        protected router: Router,
        protected authenticationService: AuthenticationService
    ) { }

    signin(): void {
        this.authenticationService.signin(this.model.username, this.model.password)
            .subscribe(
                () => {
                    // Strategy for refresh token through a scheduler.
                    // this.authenticationService.scheduleRefresh();
                    // Gets user's data.
                    this.authenticationService.getUserInfo().subscribe(
                        (userInfo: any) => {
                            this.authenticationService.changeUser(userInfo);

                            // Gets the redirect URL from authentication service.
                            // If no redirect has been set, uses the default.
                            const redirect: string = this.authenticationService.redirectUrl
                                ? this.authenticationService.redirectUrl
                                : '/home';
                            // Redirects the user.
                            window.location.href = redirect;

                        });
                },
                (error: any) => {
                    swal({ title: '', text: error, type: 'error', confirmButtonColor: '#d33' });
                });
    }

    clearMessages(): void {
    }

}
