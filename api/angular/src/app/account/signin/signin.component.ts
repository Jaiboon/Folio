﻿import { Component } from '@angular/core';

import { AuthenticationService } from '../../services/authentication.service';
import { User } from '../../models/user';
import { ApplicationService } from '../../services';

@Component({
    selector: 'signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.scss']
})
export class SigninComponent {
    disableLogin: boolean = false;
    model: any = {};
    isSignedIn: boolean = false;
    signingin: boolean = false;
    username: string = '';
    user: User;

    constructor(
        private authenticationService: AuthenticationService,
        // private navigationService: NavigationService,
        private applicationService: ApplicationService
        // private ktzService: KTZService,
        // private translate: TranslateService
    ) {
        // // Preloads data for live example.
        // this.model.username = "270320I";
        // this.model.password = "22222222";

        authenticationService.isSignedIn().subscribe((isSignedIn) => {
            this.isSignedIn = isSignedIn;
        });
        authenticationService.userChanged().subscribe((user) => {
            this.user = user;
        });
    }
    signout(): void {
        this.authenticationService.signout();
    }

    signin(): void {
        if (!this.signingin) {
            this.signingin = true;
            this.authenticationService.signin(this.model.username, this.model.password)
                .subscribe(
                    () => {
                        // Strategy for refresh token through a scheduler.
                        // this.authenticationService.scheduleRefresh();
                        // Gets user's data.
                        this.authenticationService.getUserInfo().subscribe(
                            (userInfo: any) => {
                                this.authenticationService.changeUser(userInfo);
                                this.isSignedIn = true;
                                this.signingin = false;
                            });
                        this.signingin = false;

                    },
                    (error: any) => {
                        this.signingin = false;
                        this.applicationService.showError('', error);
                    });

        }
    }
}