import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';

import { SurveyRoutingModule } from './survey-routing.module';
import { SurveyComponent } from './survey/survey.component';



@NgModule({
    imports: [
        SurveyRoutingModule,
        SharedModule
    ],
    declarations: [
        SurveyComponent
    ],
    exports: [
        SurveyComponent
    ]
})
export class SurveyModule { }
