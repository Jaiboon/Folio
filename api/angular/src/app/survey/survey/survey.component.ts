import { Component, OnInit } from '@angular/core';
import { JsonFileService } from '../../services/jsonfile.service';
import { OIEService, ApplicationService } from '../../services/index';
import swal from 'sweetalert2';
import { NodeCompatibleEventEmitter } from 'rxjs/internal/observable/fromEvent';

class Company {
  industryId: number;
  companyName: string;
  companyNameTh: string;
  companyCard: string;
  addressNo: string;
  building: string;
  floor: string;
  soi: string;
  street: string;
  tumbon: string;
  amphur: string;
  province: string;
  postCode: string;
  phone: string;
  fax: string;
  website: string;
  contactName: string;
  contactSurname: string;
  contactPosition: string;
  contactNo: string;
  contactEmail: string;
  industrialEstate: boolean;
  industrialEstInfo: string;
  mainProduct: string;
  otherProduct: string;
  thaiPercentage: number;
  foreignPercentage: number;
  employeeNo: string;
  valueChainRawMat: boolean;
  valueChainComponent: boolean;
  valueChainFinishgood: boolean;
  valueChainServices: boolean;
}
class PrimaryData {
  year: number = new Date().getFullYear();
  industryId: number;
  countryId: number;
  companyId: number;
  indicatorId: number;
  score: number;
  remark: number;
}
class PrimaryDataHead {
  year: number = new Date().getFullYear();
  industryId: number;
  companyId: number;
  RecPeople: string;
  RecMkt: string;
  RecCapital: string;
  RecTech: string;
  RecRegulation: string;
}
class Survey {
  UserId: number;
  company: Company = new Company();
  primaryData: PrimaryData[] = [];
  primaryDataHead: PrimaryDataHead = new PrimaryDataHead();
}

@Component({
  selector: 'survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.css']
})
export class SurveyComponent implements OnInit {

  model: Survey = new Survey();
  isPreviewReport: boolean = false;
  SumPercentage: number = 0;
  saving: boolean = false;
  loading: boolean = false;
  tempSurvey: any = null;
  constructor(
    private jsonFileService: JsonFileService,
    private oieService: OIEService,
    private applicationService: ApplicationService,
  ) {
    this.loadData();
  }

  loadData() {
    if (!this.loading) {
      this.loading = true;
      if (this.tempSurvey == null) {
        this.jsonFileService.getDocument('/survey.json')
          .subscribe((json: any) => {
            this.tempSurvey = json;
            this.model = JSON.parse(JSON.stringify(this.tempSurvey));
            this.loading = false;
          });
      }
      else{
        this.model = JSON.parse(JSON.stringify(this.tempSurvey));
        this.loading = false;
      }
    }
  }

  ngOnInit() {
  }

  calPercentage() {
    this.SumPercentage = Number(this.model.company.thaiPercentage) + Number(this.model.company.foreignPercentage);
  }

  submit() {
    if (this.valid()) {
      let data: Survey = new Survey();

      this.model.primaryData.filter(o => o.indicatorId != 0).forEach(element => {
        let temp: PrimaryData = new PrimaryData();
        temp.year = new Date().getFullYear();
        temp.industryId = this.model.company.industryId;
        temp.indicatorId = element.indicatorId;
        temp.countryId = element.countryId;
        temp.remark = element.remark;
        temp.score = element.score;
        data.primaryData.push(temp);
      });
      data.primaryDataHead = this.model.primaryDataHead;
      data.primaryDataHead.year = new Date().getFullYear();
      data.company = this.model.company;

      if (!this.saving) {
        this.saving = true;
        this.oieService.SurveySave(data).subscribe(
          success => {
            this.applicationService.showSuccess('บันทึกข้อมูล', 'บันทึกข้อมูลเสร็จสิ้น');
            this.saving = false;
            this.loadData();
          },
        error => {
          this.saving = false;
          this.applicationService.showError('บันทึกข้อมูล', 'บันทึกข้อมูลไม่สำเร็จ');
        });
      }
    }
  }

  valid() {
    if (this.model.company.industryId == 0) {
      this.applicationService.showError('กรุณากรอกข้อมูล', 'กรุณาเลือกประเภทอุตสาหกรรมใด');
      return false;
    }
    return true;
  }
}
