﻿import { Component } from '@angular/core';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { OIEService } from '../../services';
import { Globals, Setting } from '../../../globals';
import swal from 'sweetalert2';
@Component({
    selector: 'data.export',
    templateUrl: './data.export.component.html',
    styleUrls: ['./data.export.component.scss']
})
export class DataExportComponent {
    year: number = new Date().getFullYear();
    arrYear: number[] = [];
    arrVersion: number[] = [];
    exporting: boolean = false;
    data: any = [
        {
            'id': 1,
            'name': "ฐานข้อมูลอุตสหากรรม (Master)",
            'detail': "",
            'dataName': "industry",
            'paramyear': '',
            'paramversion': '',
            'exporting': false,
            'viewname': 'V_M_Industry'

        },
        {
            'id': 2,
            'name': "ฐานข้อมูลประเทศ (Master)",
            'detail': "",
            'dataName': "country",
            'paramyear': '',
            'paramversion': '',
            'exporting': false,
            'viewname': 'V_M_Country'
        },
        {
            'id': 3,
            'name': "ฐานข้อมูลบริษัท (Master)",
            'detail': "",
            'dataName': "company",
            'paramyear': '',
            'paramversion': '',
            'exporting': false,
            'viewname': 'V_M_Company'
        },
        {
            'id': 4,
            'name': "ฐานข้อมูลดัชนีอุตสาหกรรม (Master)",
            'detail': "",
            'dataName': "index",
            'paramyear': '',
            'paramversion': '',
            'exporting': false,
            'viewname': 'V_M_Index'
        },
        {
            'id': 5,
            'name': "ฐานข้อมูลตัวชี้วัดอุตสาหกรรม (Master)",
            'detail': "",
            'dataName': "indicator",
            'paramyear': '',
            'paramversion': '',
            'exporting': false,
            'viewname': 'V_M_Indicator'
        }
        ,
        {
            'id': 6,
            'name': "ฐานข้อมูลตารางดัชนีอุตสาหกรรม (Master)",
            'detail': "",
            'dataName': "competitiveness",
            'paramyear': '',
            'paramversion': '',
            'exporting': false,
            'viewname': 'V_M_Industry'
        }
        ,
        {
            'id': 7,
            'name': "ข้อมูลทุติยภูมิ Thailand Only (Data)",
            'detail': "",
            'dataName': "secondary_thonly",
            'paramyear': '',
            'paramversion': '',
            'exporting': false,
            'viewname': 'V_D_SECONDARY_THONLY'
        }
        ,
        {
            'id': 8,
            'name': "ข้อมูลแบบสำรวจ (Data)",
            'detail': "",
            'dataName': "survey",
            'paramyear': this.year,
            'paramversion': '',
            'exporting': false,
            'viewname': 'V_D_SURVEY'
        }
        ,
        {
            'id': 9,
            'name': "ข้อมูลปฐมภูมิจากแบบสำรวจ (Data)",
            'detail': "",
            'dataName': "primary",
            'paramyear': this.year,
            'paramversion': '',
            'exporting': false,
            'viewname': 'V_D_PRIMARY'
        }
        ,
        {
            'id': 10,
            'name': "ข้อมูลทุติยภูมิ (Data)",
            'detail': "",
            'dataName': "secondary",
            'paramyear': this.year,
            'paramversion': '',
            'exporting': false,
            'viewname': 'V_D_SECONDARY'
        }
        ,
        {
            'id': 11,
            'name': "ข้อมูลแบบประเมิน (Data)",
            'detail': "",
            'dataName': "assessement",
            'paramyear': this.year,
            'paramversion': '',
            'exporting': false,
            'viewname': 'V_D_ASSESSEMENT'
        }
        ,
        {
            'id': 12,
            'name': "ข้อมูลแบบสำรวจ ส่วนข้อเสนอแนะ (Data)",
            'detail': "",
            'dataName': "survey_recomend",
            'paramyear': this.year,
            'paramversion': '',
            'exporting': false,
            'viewname': 'V_D_SURVEY_RECOMEND'
        }

        ,
        {
            'id': 13,
            'name': "ข้อมูล PMI ส่วนที่ 1 (Data)",
            'detail': "",
            'dataName': "pmi_prat1",
            'paramyear': this.year,
            'paramversion': '',
            'exporting': false,
            'viewname': 'V_D_PMI_PART1'
        }
        ,
        {
            'id': 14,
            'name': "ข้อมูล PMI ส่วนที่ 2 (Data)",
            'detail': "",
            'dataName': "pmi_prat2",
            'paramyear': this.year,
            'paramversion': '',
            'exporting': false,
            'viewname': 'V_D_PMI_PART2'
        }
        ,
        {
            'id': 15,
            'name': "ข้อมูลดัชนีอุตสาหกรรม (Result)",
            'detail': "",
            'dataName': "industry_indicator",
            'paramyear': this.year,
            'paramversion': '0',
            'exporting': false,
            'viewname': 'V_R_INDUSTRY_INDICATOR'
        }
        ,
        {
            'id': 16,
            'name': "ข้อมูลมาตราฐานแบบประเมิน (Result)",
            'detail': "",
            'dataName': "assessment_base",
            'paramyear': this.year,
            'paramversion': '0',
            'exporting': false,
            'viewname': 'V_R_ASSESSMENT_BASE'
        }
        ,
        {
            'id': 17,
            'name': "ข้อมูลจุดอ่อนจุดแข็งประเทศไทย (Result)",
            'detail': "",
            'dataName': "th_5sw",
            'paramyear': this.year,
            'paramversion': '0',
            'exporting': false,
            'viewname': 'V_R_TH_5SW'
        }
        ,
        {
            'id': 18,
            'name': "ตารางดัชนีอุตสาหกรรม (Result)",
            'detail': "",
            'dataName': "competitiveness_index",
            'paramyear': this.year,
            'paramversion': '0',
            'exporting': false,
            'viewname': 'V_R_COMPETITIVENESS_INDEX'
        }
    ]

    appSetting: Setting = new Setting();
    testErr: string = "ไม่พบข้อมูล";

    constructor(
        public global: Globals,
        private oieService: OIEService
    ) {
        // let currentYear: number = new Date().getFullYear();
        // let startYear: number = 2018;
        // for (let index = startYear; index <= currentYear; index++) {
        //     this.arrYear.push(index);
        // }

        // let currentversion: number = 1;
        // let startversion: number = 1;
        // for (let index = startversion; index <= currentversion + 6; index++) {
        //     this.arrVersion.push(index);
        // }
        this.global.getAppSettings()
            .subscribe(
                appSetting => {
                    this.appSetting = appSetting;
                }
            );
    }

    getYears() {
        return Array.from(new Set(this.appSetting.industryIndicator.map((item: any) => item.year)));
    }

    getVersions(year: any) {
        return Array.from(new Set(this.appSetting.industryIndicator.filter(v => v.year == year).map((item: any) => item.version)));
    }

    Error_Export() {
        swal({ title: '', text: this.testErr, type: 'error', confirmButtonColor: '#d33' });
        this.exporting = false;
    }

    Export_Data(exportInfo: any) {
        this.exporting = true;
        exportInfo.exporting = true;
        //Export Master
        if (exportInfo.paramyear == '' && exportInfo.paramversion == '') {
            this.oieService.GetExport_Master(exportInfo.viewname).subscribe((data: any) => { this.exporting = false; exportInfo.exporting = false; new Angular2Csv(data, exportInfo.dataName, { headers: Object.keys(data[0]) }); }, (error: any) => {
                this.Error_Export(); exportInfo.exporting = false;
            });
        }
        //Export Data
        else if (exportInfo.paramyear != '' && exportInfo.paramversion == '') {
            this.oieService.GetExport_Data(exportInfo.viewname, exportInfo.paramyear).subscribe((data: any) => { this.exporting = false; exportInfo.exporting = false; new Angular2Csv(data, exportInfo.dataName, { headers: Object.keys(data[0]) }); }, (error: any) => {
                this.Error_Export(); exportInfo.exporting = false;
            });
        }

        //Export  Result
        else if (exportInfo.paramyear != '' && exportInfo.paramversion != '') {
            this.oieService.GetExport_Result(exportInfo.viewname, exportInfo.paramyear, exportInfo.paramversion).subscribe((data: any) => { this.exporting = false; exportInfo.exporting = false; new Angular2Csv(data, exportInfo.dataName, { headers: Object.keys(data[0]) }); }, (error: any) => {
                this.Error_Export(); exportInfo.exporting = false;
            });
        }

        else {
            this.Error_Export(); exportInfo.exporting = false;
        }
    }
}
