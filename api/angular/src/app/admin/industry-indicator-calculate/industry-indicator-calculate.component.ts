import { ApplicationService } from "./../../services/app.service";
import { filter } from "rxjs/operators";
import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { JsonFileService } from "../../services/jsonfile.service";
import { OIEService } from "../../services/index";
import swal from "sweetalert2";

class DataIndustryAvg {
  id: number;
  year: number;
  version: number;
  countSurvey: number;
  countSecondary: number;
  avgIndex: number;
  useFlag: boolean;
}

@Component({
  selector: "industry-indicator-calculate",
  templateUrl: "./industry-indicator-calculate.component.html"
})
export class IndustryIndicatorCalculateComponent implements OnInit {
  data: any = [{}];
  result: any = [{}];
  adding: boolean = false;
  saving: boolean = false;
  process: boolean = false;
  dataIndustryAvg: DataIndustryAvg[];
  use: any;
  loading: boolean = true;
  year: number = new Date().getFullYear();
  arrYear: number[] = [];

  constructor(
    private jsonFileService: JsonFileService,
    private oieService: OIEService,
    private route: ActivatedRoute,
    private router: Router,
    private applicationService: ApplicationService
  ) {
    let currentYear: number = new Date().getFullYear();
    let startYear: number = 2018;
    for (let index = startYear; index <= currentYear + 1; index++) {
      this.arrYear.push(index);
    }
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.changYearFilter(this.year);
  }

  changYearFilter(year: number) {
    this.loading = true;
    this.oieService.GetListIndustryIndicator(year).subscribe(result => {
      this.loading = false;
      this.data = result;
      this.use = this.data.filter(o => o.useFlag == true)[0].id;
    });
  }

  calculate() {
    this.process = true;
    this.adding = true;
    this.oieService
      .GetIndustryIndicatorCalculate(this.year)
      .subscribe(result => {
        this.applicationService.showSuccess(
          "คำนวนข้อมูล",
          "คำนวนข้อมูลเสร็จสิ้น"
        );
        this.loadData();
        this.process = false;
        this.adding = false;
      }),
      error => {
        this.process = false;
        this.applicationService.showError("คำนวนข้อมูล", "ล้มเหลว");
      };
  }

  submit() {
    this.saving = true;
    this.oieService.IndustryIndicatorUseSave(this.year, this.use).subscribe(
      success => {
        this.applicationService.showSuccess(
          "บันทึกข้อมูล",
          "บันทึกข้อมูลเสร็จสิ้น"
        );
        this.saving = false;
      },
      error => {
        this.saving = false;
        this.applicationService.showError("บันทึกข้อมูล", "ล้มเหลว");
      }
    );
  }

  newIndustryIndicator() {
    this.adding = true;
    let industryIndicatorInYear = this.data.filter(d => d.year == this.year);

    let lastVersion = 0;
    industryIndicatorInYear.forEach(industryIndicator => {
      if (industryIndicator.version > lastVersion)
        lastVersion = industryIndicator.version;
    });

    this.oieService
      .GetIndustryIndicatorDataCount(this.year)
      .subscribe(result => {
        this.result = result;
        let newItem: any = {
          id: 0,
          year: this.year,
          version: this.result.nextVersion,
          countSurvey: this.result.cntSec,
          countSecondary: this.result.cntPri,
          avgIndex: -1,
          useFlag: false
        };
        this.data.push(newItem);
      });
  }
}
