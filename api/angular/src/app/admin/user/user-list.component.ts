﻿import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';

import { IdentityService } from '../../services/identity.service';

declare var jQuery: any;

@Component({
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
    public loading: boolean = true;
    data: any;
    userRole: string = 'Administrator';

    constructor(
        private identityService: IdentityService
    ) {
    }

    ngOnInit() {
        this.loading = true;
        this.identityService.users
            .subscribe(users => {
                this.loading = false;
                this.data = users;
            });
        this.onRoleChange(this.userRole);
    }
    onRoleChange(role) {
        this.loading = true;
        this.userRole = role;
        this.identityService.getAll(this.userRole);
    }

    getFilteredData() {
        return this.data;
    }

}
