﻿import { Component, ElementRef, ViewChild, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';

import swal from 'sweetalert2';

import { IdentityService } from '../../services/identity.service';
import { User } from '../../models/user';

declare let $: any;

@Component({
    templateUrl: './user-form.component.html',
    styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
    public id: string = '';
    public user: User = new User();
    public saving: boolean = false;

    constructor(
        private identityService: IdentityService,
        private route: ActivatedRoute,
        private router: Router) {
    }

    ngOnInit() {
        this.id = this.route.snapshot.paramMap.get('id');

        if (this.id != 'add') {

            this.identityService.get(this.id).subscribe((value) => {
                this.user = value;
            });
        }
    }


    // save(isApply: boolean){
    //     if(isApply==true){
    //         alert(1);
    //     }else{
    //         alert(2);
    //     }
    // }



    // .subscribe((value: any) => {
    //     this.users.next(value.json());
    // },
    // (error: any) => {
    // });
    save(isApply: boolean) {
        this.saving = true;
        this.user.roles = [];

        this.identityService.create(this.user)
            .subscribe(
                (res: any) => {
                    this.saving = false;

                    if (res.status == 200) {
                        swal({ title: '', text: 'บันทึกเรียบร้อย', type: "success", confirmButtonColor: "#5cb85c" });
                    }
                    if (!isApply)
                        this.router.navigate(['/admin/user']);
                },
                (error: any) => {
                    this.saving = false;
                    swal({ title: '', text: 'ไม่สามารถบันทึกข้อมูลได้', type: 'error', confirmButtonColor: '#d33' });
                });
    }
}