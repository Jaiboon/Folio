﻿import { Component, OnInit } from '@angular/core';
import { JsonFileService } from '../../services/jsonfile.service';
import { OIEService, ApplicationService } from '../../services/index';
import swal from 'sweetalert2';

class IndustryReview {
    id: number;
    year: number;
    industryName: number;
    industryNameThai: number;
    countryName: number;
    countryNameThai: number;
}


@Component({
    selector: 'industry-review',
    templateUrl: './industry-review.component.html',
    styleUrls: ['./industry-review.component.scss']
})
export class IndustryReviewComponent implements OnInit {

    loading: boolean = true;
    model: IndustryReview[];

    constructor(
        private applicationService: ApplicationService,
        private oieService: OIEService,
    ) {
        this.bindData();
    }

    ngOnInit() {

    }

    bindData() {
        this.loading = true;
        this.oieService.GetListIndustryReview()
            .subscribe(
                (data) => {
                    this.model = data;
                    this.loading = false;
                },
                (error) => {
                    this.loading = false;
                }
            );
    }

    delete(id: number) {

        swal({
            title: 'ลบข้อมูล',
            text: "คุณต้องการลบข้อมูลนี้ ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#d33',
            confirmButtonText: "ใช่ ต้องการลบข้อมูล",
            cancelButtonText: "ยกเลิก",
            showLoaderOnConfirm: true
        }).then((result) => {
            if (result.value) {
                this.oieService.DeleteIndustryReview(id)
                    .subscribe(
                        (data) => {
                            swal({ title: 'ลบข้อมูล', type: "success", text: "เสร็จสิ้น" }).then((result) => {
                                this.bindData();
                            });
                        },
                        (error) => {
                            this.applicationService.showError('ลบข้อมูล', 'ล้มเหลว');
                        }
                    );
            }
        });

    }
}
