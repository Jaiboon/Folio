﻿import { Component, OnInit } from '@angular/core';
import { JsonFileService } from '../../services/jsonfile.service';
import { OIEService, ApplicationService } from '../../services/index';
import { ActivatedRoute, Router } from '@angular/router';
import { Globals, Setting } from '../../../globals';
import swal from 'sweetalert2';

class IndustryReview {
    id: number;
    year: number = 0;
    industryId: number = 0;
    countryId: number = 0;
    htmlInformation: string;
}

@Component({
    selector: 'industry-review-form',
    templateUrl: './industry-review-form.component.html',
    styleUrls: ['./industry-review-form.component.scss']
})
export class IndustryReviewFormComponent implements OnInit {
    model: IndustryReview = new IndustryReview();
    appSetting: Setting;
    saving: boolean = false;
    constructor(
        public global: Globals,
        private applicationService: ApplicationService,
        private oieService: OIEService,
        private route: ActivatedRoute
    ) {
        global.getAppSettings()
            .subscribe(
                appSetting => {
                    this.appSetting = appSetting;
                }
            );
        let id = this.route.snapshot.paramMap.get('id');
        if (id == 'create') {
            //Create
        }
        else {
            //Edit
            oieService.GetIndustryReviewByID(parseInt(id))
                .subscribe(
                    (data) => {
                        this.model = data;
                    },
                    (error) => {

                    }
                );

        }

        // let currentYear: number = new Date().getFullYear();
        // let startYear: number = 2018;
        // for (let index = startYear; index <= currentYear + 1; index++) {
        //     this.arrYear.push(index);
        // }
    }

    ngOnInit() {

    }

    submit() {
        if (this.valid()) {
            if (!this.saving) {
                this.saving = true;
                this.oieService.SaveIndustryReview(this.model)
                    .subscribe(
                        (data) => {
                            this.saving = false;
                            this.applicationService.showSuccess('บันทึกข้อมูล', 'เสร็จสิ้น', '/admin/industry-review');
                        },
                        (error) => {
                            this.saving = false;
                            this.applicationService.showError('บันทึกข้อมูล', 'ล้มเหลว');
                        }
                    );
            }
        }
    }

    valid() {
        if (this.model.year == 0) {
            this.applicationService.showError('กรุณากรอกข้อมูล', 'กรุณาเลือกปี');
            return false;
        }
        else if (this.model.industryId == 0) {
            this.applicationService.showError('กรุณากรอกข้อมูล', 'กรุณาเลือกประเภทอุตสาหกรรม');
            return false;
        }
        else if (this.model.countryId == 0) {
            this.applicationService.showError('กรุณากรอกข้อมูล', 'กรุณาเลือกประเทศ');
            return false;
        }
        else if (this.model.htmlInformation == '') {
            this.applicationService.showError('กรุณากรอกข้อมูล', 'กรุณากรอกรายละเอียด');
            return false;
        }
        return true;
    }

    getIndustry() {
        return this.appSetting.industry;
    }

    getYear() {
        return this.appSetting.year;
    }

    getCountryByIndustry() {
        return this.appSetting.industryCountry.filter(ic => ic.industryID == this.model.industryId);
    }
}
