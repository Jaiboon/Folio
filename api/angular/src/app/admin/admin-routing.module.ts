import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../services/auth.guard';


import { UserListComponent } from './user/user-list.component';
import { UserFormComponent } from './user/user-form.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { DataExportComponent } from './data.export/data.export.component';
import { IndustryReviewComponent } from './industry-review/industry-review.component';
import { IndustryReviewFormComponent } from './industry-review/industry-review-form.component';
import { DocumentListComponent } from './document/document-list.component';
import { DocumentFormComponent } from './document/document-form.component';
import { IndustryIndicatorCalculateComponent } from './industry-indicator-calculate/industry-indicator-calculate.component';

const routes: Routes = [
    { path: "", component: AdminDashboardComponent },
    { path: 'user', component: UserListComponent },//, canActivate: [AuthGuard] },
    { path: 'user/:id', component: UserFormComponent },//, canActivate: [AuthGuard] },
    { path: "export", component: DataExportComponent },
    { path: "industry-review", component: IndustryReviewComponent },
    { path: "industry-review/:id", component: IndustryReviewFormComponent },
    { path: "document", component: DocumentListComponent },
    { path: "document/:id", component: DocumentFormComponent },
    { path: "industry-indicator", component: IndustryIndicatorCalculateComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule { }
