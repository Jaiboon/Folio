﻿import { Component, OnInit } from '@angular/core';
import { JsonFileService } from '../../services/jsonfile.service';
import { OIEService, ApplicationService } from '../../services/index';
import swal from 'sweetalert2';
import { DocumentModel } from '../../models/document-model';
import { Globals } from '../../../globals';

@Component({
    selector: 'document-list',
    templateUrl: './document-list.component.html',
    styleUrls: ['./document-list.component.scss']
})
export class DocumentListComponent implements OnInit {
    loading: boolean = true;
    model: DocumentModel[];

    constructor(
        public global: Globals,
        private applicationService: ApplicationService,
        private oieService: OIEService,
    ) {
        this.bindData();
    }

    ngOnInit() {

    }

    bindData() {
        this.loading = true;
        this.oieService.GetListDocument()
            .subscribe(
                (data) => {
                    this.loading = false;
                    this.model = data;
                },
                (error) => {
                    this.loading = false;
                }
            );
    }

    delete(id: number) {

        swal({
            title: 'ลบข้อมูล',
            text: "คุณต้องการลบข้อมูลนี้ ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#d33',
            confirmButtonText: "ใช่ ต้องการลบข้อมูล",
            cancelButtonText: "ยกเลิก",
            showLoaderOnConfirm: true
        }).then((result) => {
            if (result.value) {
                this.oieService.DeleteDocument(id)
                    .subscribe(
                        (data) => {
                            swal({ title: 'ลบข้อมูล', type: "success", text: "เสร็จสิ้น" }).then((result) => {
                                this.bindData();
                            });
                        },
                        (error) => {
                            this.applicationService.showError('ลบข้อมูล', 'ล้มเหลว');
                        }
                    );
            }
        });

    }
}
