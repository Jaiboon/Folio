﻿import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { JsonFileService } from '../../services/jsonfile.service';
import { OIEService, ApplicationService } from '../../services/index';
import { ActivatedRoute, Router } from '@angular/router';
import { Globals } from '../../../globals';
import swal from 'sweetalert2';
import { UploadService } from '../../services/upload.service';
import { DocumentModel } from '../../models/document-model';



@Component({
    selector: 'document-form',
    templateUrl: './document-form.component.html',
    styleUrls: ['./document-form.component.scss']
})
export class DocumentFormComponent implements OnInit {

    @ViewChild('fileInput') fileInput: ElementRef;
    @ViewChild('fileInputDocument') fileInputDocument: ElementRef;

    model: DocumentModel = new DocumentModel();
    documentList: DocumentModel[] = [];
    saving: boolean = false;
    uploading: boolean;
    constructor(
        public global: Globals,
        private applicationService: ApplicationService,
        private oieService: OIEService,
        private route: ActivatedRoute,
        public uploadService: UploadService
    ) {
        let id = this.route.snapshot.paramMap.get('id');
        
        oieService.GetListDocument()
            .subscribe(
                (data) => {
                    this.documentList = data.filter(o => o.id != id && o.level < 3);
                },
                (error) => {

                }
            );

        if (id == 'create') {
            //Create
        }
        else {
            //Edit
            oieService.GetDocumentByID(parseInt(id))
                .subscribe(
                    (data) => {
                        this.model = data;
                    },
                    (error) => {

                    }
                );

        }
    }

    ngOnInit() {

    }

    submit() {
        if (this.valid()) {
            if (!this.saving) {
                this.saving = true;
                this.oieService.SaveDocument(this.model)
                    .subscribe(
                        (data) => {
                            this.saving = false;
                            this.applicationService.showSuccess('บันทึกข้อมูล', 'เสร็จสิ้น', '/admin/document');
                        },
                        (error) => {
                            this.saving = false;
                            this.applicationService.showError('บันทึกข้อมูล', 'ล้มเหลว');
                        }
                    );
            }
        }
    }

    valid() {
        if (this.model.documentName == '') {
            this.applicationService.showError('กรุณากรอกข้อมูล', 'กรุณากรอกชื่อเอกสาร');
            return false;
        }
        // else if (this.model.documentFile == '') {
        //     this.applicationService.showError('กรุณากรอกข้อมูล', 'กรุณาเลือกไฟล์เอกสาร');
        //     return false;
        // }
        // else if (this.model.imageFile == '') {
        //     this.applicationService.showError('กรุณากรอกข้อมูล', 'กรุณาเลือกรูปภาพประกอบเอกสาร');
        //     return false;
        // }
        return true;
    }


    uploadFile(type: string): void {
        let fi: any;
        if (type == 'document'){
            fi = this.fileInputDocument.nativeElement;
        }
        else// if(type == 'image')
        {
            fi = this.fileInput.nativeElement;
        }

        if (!this.uploading && fi.value !== '') {
            this.uploading = true;
            if (fi.files && fi.files[0]) {
                const fileToUpload = fi.files[0];
                try {
                    this.uploadService
                        .upload(fileToUpload, type)
                        .subscribe(res => {
                            if(type == 'document'){
                                this.model.documentFile = res["fileUrl"];
                            }
                            else{
                                this.model.imageFile = res["fileUrl"];
                            }
                            this.uploading = false;
                        },error => {
                            this.uploading = false;
                            swal({
                                title: 'ขออภัย!',
                                text: 'กรุณาลองใหม่ในภายหลัง',
                                type: 'error',
                                confirmButtonColor: '#d33'
                            });
                        });
                } catch (magError) {
                    this.uploading = false;
                    swal({
                        title: '',
                        text: magError,
                        type: 'error',
                        confirmButtonColor: '#d33'
                    });
                }
            }
        }
    }

    remove(type: string) {
        this.model.imageFile = '';
        this.model.documentFile = '';
        this.fileInput.nativeElement.value = '';
        this.fileInputDocument.nativeElement.value = '';
    }

}
