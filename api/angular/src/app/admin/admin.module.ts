import { NgModule } from '@angular/core';

import { AdminRoutingModule } from './admin-routing.module';
import { SharedModule } from '../shared/shared.module';

import { UserListComponent } from './user/user-list.component';
import { UserFormComponent } from './user/user-form.component';
import { IndustryIndicatorCalculateComponent } from './industry-indicator-calculate/industry-indicator-calculate.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { DocumentListComponent } from './document/document-list.component';
import { DocumentFormComponent } from './document/document-form.component';
import { DataExportComponent } from './data.export/data.export.component';
import { IndustryReviewFormComponent } from './industry-review/industry-review-form.component';
import { IndustryReviewComponent } from './industry-review/industry-review.component';



@NgModule({
    imports: [
        AdminRoutingModule,
        SharedModule
    ],
    declarations: [
        AdminDashboardComponent,
        IndustryIndicatorCalculateComponent,
        UserListComponent,
        UserFormComponent,
        DocumentListComponent,
        DocumentFormComponent,
        DataExportComponent,
        IndustryIndicatorCalculateComponent,
        IndustryReviewComponent,
        IndustryReviewFormComponent
    ],
    exports: [
    ]
})
export class AdminModule { }
