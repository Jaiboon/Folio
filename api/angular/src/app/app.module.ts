﻿import { NgModule } from "@angular/core";

import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
// import { ChartsModule } from 'ng2-charts';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { AppRoutingModule } from "./app-routing.module";
import { SharedModule } from "./shared/shared.module";
import { JwtModule, JwtInterceptor } from "@auth0/angular-jwt";
import { AppComponent } from "./app.component";
// import { environment } from './environments/environment';

import {
    HomeComponent,
    AppHeaderComponent,
    AppFooterComponent,
    AppSidebarNavComponent,
    AppIndustryIndicatorsComponent,
    RadarIndicatorChartComponent,
    StrengthsChartComponent,
    WeaknessesChartComponent,
    LineChartComponent,
    DocumentHomeComponent,
    AboutComponent
} from "./components";

import {
    AuthGuard,
    AuthenticationService,
    ApplicationService,
    IdentityService,
    OIEService,
    BrowserStorage,
    CategoryService,
    UploadService
} from "./services";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { SigninComponent } from "./account/signin/signin.component";
import { RefreshTokenInterceptor } from "./services/refresh-token-interceptor";
import { environment } from "../environments/environment";
import { DashboardComponent } from "./dashboard/components/dashboard.component";

export function tokenGetter() {
    return localStorage.getItem("access_token");
}

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        SharedModule,
        HttpClientModule,
        SlimLoadingBarModule.forRoot(),
        JwtModule.forRoot({
            config: {
                tokenGetter: tokenGetter,
                whitelistedDomains: environment.whitelistedDomains
                // blacklistedRoutes: ['localhost:3001/auth/']
            }
        })
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        AppHeaderComponent,
        AppFooterComponent,
        AppSidebarNavComponent,
        AboutComponent,
        DocumentHomeComponent,
        SigninComponent,

        AppIndustryIndicatorsComponent,
        RadarIndicatorChartComponent,
        StrengthsChartComponent,
        WeaknessesChartComponent,
        LineChartComponent,

        DashboardComponent
    ],
    // exports: [SafeHtmlPipe],
    providers: [
        JwtInterceptor, // Providing JwtInterceptor allow to inject JwtInterceptor manually into RefreshTokenInterceptor
        {
            provide: HTTP_INTERCEPTORS,
            useExisting: JwtInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: RefreshTokenInterceptor,
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
