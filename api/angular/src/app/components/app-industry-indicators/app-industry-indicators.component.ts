import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ChartFilterModel } from '../../models/chart-filter-model';
import { debug } from 'util';
import { OIEService } from '../../services';

@Component({
  selector: 'app-industry-indicators',
  templateUrl: './app-industry-indicators.component.html',
  styleUrls: ["./app-industry-indicators.component.css"]

})
export class AppIndustryIndicatorsComponent implements OnInit {

  filterModel: ChartFilterModel;
  isDashobard: boolean = false;
  refreshDate: Date;
  review_information: string = '';

  @Input() set is_dashboard(isDashobard: boolean) { this.isDashobard = isDashobard; }
  @Output() onFilterChange = new EventEmitter<ChartFilterModel>();
  @Output() onPillarChange = new EventEmitter<number>();

  constructor(
    private oieService: OIEService
  ) {
  }

  ngOnInit() {
  }

  onFilterChanged(filterModel: any) {
    this.refreshDate = new Date();
    this.filterModel = filterModel;
    this.onFilterChange.emit(this.filterModel);
    this.oieService.GetIndustryReview(this.filterModel.year, this.filterModel.industry, this.filterModel.country)
      .subscribe(
        (data) => {
          this.review_information = data.htmlInformation;
        },
        (error) => {
          this.review_information = '';
        }
      );
  }

  onPillarChanged(pillarNo: number) {
    this.refreshDate = new Date();
    this.onPillarChange.emit(Number(pillarNo));
  }
}
