﻿import { Component } from "@angular/core";
import { OIEService } from '../../services/index';
import { DocumentModel } from '../../models/document-model';
import { Globals } from "../../../globals";

@Component({
  selector: "document-home",
  templateUrl: "./document.home.component.html"
})

export class DocumentHomeComponent {
  model: DocumentModel[] = [];
  constructor(
    public global: Globals,
    private oieService: OIEService) {
    oieService.GetListDocumentHomePage().subscribe(
      (data) => {
        this.model = data;
      },
      (error) => {

      }
    );
  }

  
}
