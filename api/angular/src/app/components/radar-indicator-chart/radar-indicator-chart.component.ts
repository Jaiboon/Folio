import { OIEService } from './../../services/oie.service';
import { Component, OnInit, HostListener, Output, EventEmitter, Input, ViewChild, ElementRef } from '@angular/core';
import { ChartFilterModel } from '../../models/chart-filter-model';
import { BrowserStorage } from '../../services';
import { ApplicationService } from '../../services/app.service';
import { Router } from '@angular/router';
import { Globals, Setting } from '../../../globals';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { BaseChartDirective } from 'ng2-charts';


import { Chart } from 'chart.js';
declare let $: any;

@Component({
  selector: 'radar-indicator-chart',
  templateUrl: './radar-indicator-chart.component.html',
  styleUrls: ['./radar-indicator-chart.component.css']
})
export class RadarIndicatorChartComponent implements OnInit {
  @ViewChild("focusdv", { read: ElementRef }) focusdv: ElementRef;

  innerWidth: any;
  TABLETS_SIZE = 768;

  radarChartStep: boolean = true;
  filterModel: ChartFilterModel;
  isDashobard: boolean = false;
  loading: boolean = false;
  
  barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    legend: {
      // position: 'left',
      labels: {
        fontFamily: 'Kanit',
        fontSize: 16,  //ขนาดตัวอักษรของแต่ละมุม
        fontColor: '#212529',
        usePointStyle: true
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          // beginAtZero: true
          fontFamily: 'Kanit'
        }
      }],
      xAxes: [{
        ticks: {
          autoSkip: false,
          fontFamily: 'Kanit'
        }
      }]
    }
  };



  radarChartoptions: any = {
    // responsive: true,
    fontFamily: 'Kanit',
    // maintainAspectRatio: false,
    legend: {
      // position: 'left',
      labels: {
        fontFamily: 'Kanit',
        fontSize: 16,  //ขนาดตัวอักษรของแต่ละมุม
        fontColor: '#212529',
        usePointStyle: true
      }
    },
    plugins: [{
      filler: {
        propagate: true
      }
    }]
    , elements:
    {
      point:
      {
        radius: 5, //ขนาดปุ่ม
        hitRadius: 5,
        hoverRadius: 5,
        hoverBorderWidth: 2
      },
    }
    , scale: {
      pointLabels: {
        fontSize: 13,  //ขนาดตัวอักษรของแต่ละมุม
        fontFamily: 'Kanit',
        fontColor: '#212529',
      },
      ticks: { //ปรับ Min Max ของแกน
        steps: 1,
        stepValue: 1,
        min: 0,
        max: 10,
        fontSize: 12,  //ขนาดตัวอักษรของแต่ละมุม
        fontFamily: 'Kanit',
        fontColor: 'rgb(0, 0, 0,1)',
      }
    }
    ,
    tooltips: {
      // Disable the on-canvas tooltip
      enabled: false,

      custom: function (tooltipModel) {
        // Tooltip Element
        var tooltipEl = document.getElementById('chartjs-tooltip');
        // Create element on first render
        if (!tooltipEl) {
          tooltipEl = document.createElement('div');
          tooltipEl.id = 'chartjs-tooltip';
          tooltipEl.innerHTML = "<table class='radarTooltip' style='background: rgba(0, 0, 0, 0.823); color:rgba(255, 255, 255, 0.823)'></table>";
          document.body.appendChild(tooltipEl);
        }

        // Hide if no tooltip
        if (tooltipModel.opacity === 0) {
          tooltipEl.style.opacity = '0';
          tooltipEl.style.pointerEvents = "none";
          return;
        }

        // Set caret Position
        tooltipEl.classList.remove('above', 'below', 'no-transform');
        if (tooltipModel.yAlign) {
          tooltipEl.classList.add(tooltipModel.yAlign);
        } else {
          tooltipEl.classList.add('no-transform');
        }

        function getBody(bodyItem) {
          return bodyItem.lines;
        }

        // Set Text
        if (tooltipModel.body) {
          var titleLines = tooltipModel.title || [];
          var bodyLines = tooltipModel.body.map(getBody);

          var innerHtml = '<thead>';

          for (let i = 0; i < titleLines.length; i = i + 2) {
            let ntitle = titleLines[i].substring(0, 1);
            // innerHtml += '<tr><th>' + RadarIndicatorChartComponent.PillarLabels[2][ntitle - 1] + '</th></tr>';
            // innerHtml += '<tr><th>' + RadarIndicatorChartComponent.RadarChartDesktopLabels[0][ntitle - 1][1] + '</th></tr>';
          }
          innerHtml += '</thead><tbody>';

          bodyLines.forEach(function (body, i) {
            var colors = tooltipModel.labelColors[i];
            var style = ' ';
            style += ' border-color:' + colors.borderColor;
            style += '; border-width: 2px';
            var span = '<span style="' + style + '"></span>';
            innerHtml += '<tr><td>' + span + body + '</td></tr>';
          });
          innerHtml += '</tbody>';

          var tableRoot = tooltipEl.querySelector('table');
          tableRoot.innerHTML = innerHtml;
        }

        var position = this._chart.canvas.getBoundingClientRect();
        tooltipEl.style.opacity = '1';
        tooltipEl.style.position = 'absolute';
        tooltipEl.style.left = position.left + tooltipModel.caretX + 'px';
        tooltipEl.style.top = ($('canvas').offset().top) + tooltipModel.caretY + 'px';
        tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
        tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
        tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
        tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';

      }
    }
  };

  @Input() set is_dashboard(isDashobard: boolean) {
    this.isDashobard = isDashobard;
    if (!this.isDashobard) {
      this.filterModel = new ChartFilterModel();
    }
    else
      this.filterModel = this.applicationService.getFilter();
    this.applicationService.setFilter(this.filterModel);
  }
  @Output() onFilterChange = new EventEmitter<ChartFilterModel>();
  @Output() onPillarChange = new EventEmitter<number>();

  appSetting: Setting = new Setting();
  constructor(
    private chartService: OIEService,
    public global: Globals,
    private slimLoadingBarService: SlimLoadingBarService,
    private applicationService: ApplicationService,
    private router: Router
  ) {
  }

  public SwitchChart(type: any) {
   
    this.radarChartStep = true;
   
    if (type != 'radar') {
      this.radarChartStep = false
    
    }
 
  }

  onFilterChanged() {
    this.loading = true;
    let countries = this.appSetting.industryCountry.filter(ic => ic.industryID == this.filterModel.industry && ic.countryID == this.filterModel.country);
    if (countries.length == 0) {
      countries = this.appSetting.industryCountry.filter(ic => ic.industryID == this.filterModel.industry);
      if (countries.length > 0)
        this.filterModel.country = countries[0].countryID;
    }
    this.applicationService.setFilter(this.filterModel);
    this.onFilterChange.emit(this.filterModel);
    this.slimLoadingBarService.start();

    this.chartService.GetRadar(this.filterModel.year, this.filterModel.industry, this.filterModel.country)
      .subscribe(
        result => {
          this.radarChartData = result.data;
          this.radarChartLabels = result.lables;
          this.barChartLabels = result.lables;
          this.slimLoadingBarService.stop();
          this.slimLoadingBarService.complete();
          this.loading = false;
          this.focusdv.nativeElement.scrollTo(150, 0);
        },
        (error: any) => {
          this.slimLoadingBarService.stop();
          this.slimLoadingBarService.complete();
          this.loading = false;
        }
      );
  }

  ngOnInit() {
    this.innerWidth = window.innerWidth;
    this.deviceSizeSwitch();

    this.global.getAppSettings()
      .subscribe(
        appSetting => {
          this.appSetting = appSetting;
          this.onFilterChanged();
        }
      );
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
    this.deviceSizeSwitch();

  }

  deviceSizeSwitch() {
    // if (this.innerWidth < this.TABLETS_SIZE)
    //   this.radarChartLabels = RadarIndicatorChartComponent.PillarLabels[1];//Mobile
    // else
    //   this.radarChartLabels = RadarIndicatorChartComponent.PillarLabels[2];//Desktop
  }

  // Radar
  // static PillarLabels: any = [
  //   [5, 6, 7, 8, 9, 10, 11, 12, 13],
  //   ["1", "2", "3", "4", "5", "6", "7", "8", "9"],
  //   [
  //     "Business Environment & Strategy",
  //     "Factor of Production",
  //     "Technology & Innovation",
  //     "Production",
  //     "Sustainability",
  //     "Management",
  //     "Products & Markets",
  //     "Performance",
  //     "Future Prospect"
  //   ]
  // ];


  public radarChartLabels: any = this.global.pillarLabels.desktopLabels;
  public barChartLabels: any = this.global.pillarLabels.desktopLabels;

  public radarChartData: any = [
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0], label: '' },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0], label: '' }
  ];

  public radarChartColors: any = [
    {
      backgroundColor: 'rgba(0,0,0,0)',
      borderColor: 'rgba(255, 128, 62,1)',
      pointBackgroundColor: 'rgba(255, 128, 62,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(255, 128, 62,1)',
      fill: false  //สีข้างในกราฟ
    },
    {
      backgroundColor: 'rgba(0,0,0,0)',
      borderColor: 'rgba(56,163,235,1)',
      pointBackgroundColor: 'rgba(56,163,235,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(56,163,235,1)',
      fill: false  //สีข้างในกราฟ
    }
  ];

  public barChartColors: Array<any> = [
    { backgroundColor: 'rgba(255, 128, 62,1)' }, { backgroundColor: 'rgba(56,163,235,1)' }
  ]


  pillarClick(pillarIndex: number) {
    if (this.isDashobard)
      this.onPillarChange.emit(pillarIndex);
    else
      this.router.navigate([`/dashboard/${pillarIndex}`]);
  }


  getCountryByIndustry(competitiveType: string) {
    return this.appSetting.industryCountry.filter(ic => ic.industryID == this.filterModel.industry && ic.competitiveType == competitiveType);
  }
  @ViewChild('radarChart') private myRadarChart: BaseChartDirective;

  // events
  public chartClicked(event: any): void {
    let e = event.event;

    var helpers = Chart.helpers;
    var chart = this.myRadarChart.chart;

    var eventPosition = helpers.getRelativePosition(e, chart);
    var mouseX = eventPosition.x;
    var mouseY = eventPosition.y;

    var activePoints = [];
    helpers.each(chart.scale.ticks, function (label, index) {
      for (var i = this.ticks.length - 1; i >= 0; i--) {
        var pointLabelPosition = this.getPointPosition(i, this.getDistanceFromCenterForValue(this.options.reverse ? this.min : this.max) + 5);

        var pointLabelFontSize = helpers.getValueOrDefault(this.options.pointLabels.fontSize, Chart.defaults.global.defaultFontSize);
        var pointLabeFontStyle = helpers.getValueOrDefault(this.options.pointLabels.fontStyle, Chart.defaults.global.defaultFontStyle);
        var pointLabeFontFamily = helpers.getValueOrDefault(this.options.pointLabels.fontFamily, Chart.defaults.global.defaultFontFamily);
        var pointLabeFont = helpers.fontString(pointLabelFontSize, pointLabeFontStyle, pointLabeFontFamily);
        chart.ctx.font = pointLabeFont;

        var labelsCount = this.pointLabels.length,
          halfLabelsCount = this.pointLabels.length / 2,
          quarterLabelsCount = halfLabelsCount / 2,
          upperHalf = (i < quarterLabelsCount || i > labelsCount - quarterLabelsCount),
          exactQuarter = (i === quarterLabelsCount || i === labelsCount - quarterLabelsCount);
        var width = chart.ctx.measureText(this.pointLabels[i]).width;
        var height = pointLabelFontSize;

        var x, y;

        if (i === 0 || i === halfLabelsCount)
          x = pointLabelPosition.x - width / 2;
        else if (i < halfLabelsCount)
          x = pointLabelPosition.x;
        else
          x = pointLabelPosition.x - width;

        if (exactQuarter)
          y = pointLabelPosition.y - height / 2;
        else if (upperHalf)
          y = pointLabelPosition.y - height;
        else
          y = pointLabelPosition.y

        if ((mouseY >= y && mouseY <= y + height) && (mouseX >= x && mouseX <= x + width))
          activePoints.push({ index: i, label: this.pointLabels[i] });
      }
    }, chart.scale);

    var firstPoint = activePoints[0];
    if (firstPoint !== undefined) {
      this.pillarClick(this.appSetting.pillar[firstPoint.index].id);
    }
  }

  public chartHovered(e: any): void {
    // console.log(e);
  }
}
