import { Component, OnInit } from '@angular/core';
import { OIEService, AuthenticationService } from '../../services';
import { User } from '../../models/user';
import { Globals } from '../../../globals';

class Page {
  html: string;
  constructor() {
    this.html = '';
  }
}
@Component({
  selector: 'about',
  templateUrl: './about.component.html'
})
export class AboutComponent implements OnInit {

  page: any = new Page();
  editing: boolean = false;
  isAdmin: boolean = false;

  constructor(
    public globals: Globals,
    private oieService: OIEService,
    private authenticationService: AuthenticationService
  ) {

    this.authenticationService.userChanged().subscribe(
      (user: User) => {
        this.isAdmin = this.authenticationService.isInRole("Administrator");
      });

    oieService.GetHomeContent()
      .subscribe(page => {
        this.page = page;
      });
  }

  ngOnInit() {
  }

  saveContent() {
    this.oieService.UpdateHomeContent(this.page)
      .subscribe(
        result => {
          this.editing = false;
        },
        (error: any) => {
        });
  }
}
