﻿import { Component, Input } from '@angular/core';
import { OIEService, ApplicationService } from '../../services';
import { ChartFilterModel } from '../../models/chart-filter-model';

@Component({
  selector: 'line-chart',
  templateUrl: './line-chart.component.html'
})

export class LineChartComponent {

  // lineChart
  public lineChartData: any[] = [{ data: [], label: "" }];
  public lineChartLabels: any = [];
 
  public lableChart: string = "";

  public lineChartColors: Array<any> = [
    {
      backgroundColor: 'rgba(255, 128, 62,0.8)',
      borderColor: 'rgba(255, 128, 62,0.8)',
      pointBackgroundColor: 'rgba(218,75,1,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)',
      fill: false  //สีข้างในกราฟ
    }
  ]
  private getLegendCallback = (function(self) {
  
})(this);

  lineChartOptions: any = {
    legend: {
      labels: {
        fontFamily: 'Kanit',
        fontColor: '#212529',
        usePointStyle: true,
        // generateLabels: function (chart) {
        //   return chart.data.datasets.map(function (dataset, i) {
        //     return {
        //       text: dataset.label , // + '<html>1111<br>55555555555555</html>',
        //       fillStyle: dataset.backgroundColor,
        //       hidden: dataset.hidden,
        //       lineCap: dataset.borderCapStyle,
        //       lineDash: dataset.borderDash,
        //       lineDashOffset: dataset.borderDashOffset,
        //       lineJoin: dataset.borderJoinStyle,
        //       lineWidth: dataset.borderWidth,
        //       strokeStyle: dataset.borderColor,
        //       pointStyle: dataset.pointStyle,
        //       datasetIndex: i
        //     };
        //   });
        // },
    
      }
    },
    legendCallback: this.getLegendCallback,
    elements:
    {
      point:
      {
        radius: 5, //ขนาดปุ่ม
        hitRadius: 5,
        hoverRadius: 5,
        hoverBorderWidth: 2
      },
      line: {
        tension: 0 //โค้ง
      }
    }
    , scales: {
      yAxes: [
        {

          scaleLabel: { display: true, labelString: 'Score', fontSize: 18, fontFamily: 'Kanit', fontColor: '#212529' },
          display: true,
          ticks: {
            fontSize: 15,
            fontFamily: "kanit" ,
            callback: function(value, index, values) {
              if(parseInt(value) >= 1000){
                 return  value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              } else {
                 return  value;
              }
         }       
          }
        }

      ],
      xAxes: [
        {
          scaleLabel: { display: true, labelString: 'Year', fontSize: 18, fontFamily: 'Kanit', fontColor: '#212529' },
          display: true,
          ticks: {
            fontSize: 15,
            fontFamily: "kanit"
          }
        }
      ],

    }

  };


  @Input('chart-no')
  chartNo: number = 0;

  @Input()
  set refresh(refreshDate: Date) {
    if (this.chartNo != 0) {
      let filterModel: ChartFilterModel = this.appService.getFilter();
      this.oieService.GetTransIndicatorGraph(this.chartNo, filterModel.industry)
        .subscribe(result => {
          for (let lIndex = 0; lIndex < result.labels.length; lIndex++) {
            this.lineChartLabels[lIndex] = result.labels[lIndex];
          }
          this.lineChartData = result.data;
          for (let dIndex = 0; dIndex < this.lineChartData[0].data.length; dIndex++) {
            if (this.lineChartData[0].data[dIndex] == -1) this.lineChartData[0].data[dIndex] = NaN;
          }
        
          if(result.data[0].label.length>0){
            this.lableChart=result.data[0].label;
          }
         
        });
    }
  }

  constructor(private oieService: OIEService,
    private appService: ApplicationService
  ) {
    
  }

}
