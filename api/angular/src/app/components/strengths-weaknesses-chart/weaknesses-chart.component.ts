import { Component, OnInit, Input } from "@angular/core";
import { OIEService, ApplicationService } from "../../services";
import { ChartFilterModel } from "../../models/chart-filter-model";

@Component({
  selector: "weaknesses-chart",
  templateUrl: "./weaknesses-chart.component.html"
})
export class WeaknessesChartComponent implements OnInit {
  @Input()
  set refresh(refreshDate: Date) {
    let filterModel: ChartFilterModel = this.appService.getFilter();
    this.chartService.GetWeaknesses(filterModel.year, filterModel.industry, filterModel.country)
      .subscribe(result => {
        this.barChartLabels.splice(0, this.barChartLabels.length);
        for (let lIndex = 0; lIndex < result.labels.length; lIndex++) {
          this.barChartLabels[lIndex] = result.labels[lIndex];
        }

        this.barChartData = result.data;
      });
  }

  constructor(private chartService: OIEService,
    private appService: ApplicationService
  ) { }

  ngOnInit() {
  }

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    legend: {
      display: false
    },
    scales: {
      yAxes: [
        {
          display: true,
          ticks: {
            fontSize: 13,
            fontFamily: "kanit"
          }
        }
      ],
      xAxes: [
        {
          display: true,
          ticks: {
            fontSize: 13,
            fontFamily: "kanit"
          }
        }
      ]
    }
  };
  public barChartLabels: any = [];
  public barChartType: string = "horizontalBar";
  public barChartLegend: boolean = true;

  public barChartData: any[] = [{ data: [], label: "" }];

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  public barChartColors: Array<any> = [
    {
      backgroundColor: "rgba(255, 0, 50, 0.5)"
    }
  ];

}
