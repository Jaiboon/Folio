import { Component, OnInit, Input, SimpleChanges } from "@angular/core";
import { OIEService, ApplicationService } from "../../services";
import { ChartFilterModel } from "../../models/chart-filter-model";

@Component({
  selector: "strengths-chart",
  templateUrl: "./strengths-chart.component.html"
})
export class StrengthsChartComponent implements OnInit {

  @Input()
  set refresh(refreshDate: Date) {
    let filterModel: ChartFilterModel = this.appService.getFilter();
    this.chartService.GetStrengths(filterModel.year, filterModel.industry, filterModel.industry)
      .subscribe(
        result => {
          this.barChartLabels.splice(0, this.barChartLabels.length);
          for (let lIndex = 0; lIndex < result.labels.length; lIndex++) {
            this.barChartLabels[lIndex] = result.labels[lIndex];
          }
          this.barChartData = result.data;
        }
      );
  }

  constructor(private chartService: OIEService,
    private appService: ApplicationService
  ) { }

  ngOnInit() {

  }

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    legend: {
      display: false
    },
    scales: {
      yAxes: [
        {
          display: true,
          ticks: {
            fontSize: 13,
            fontFamily: "kanit"
          }
        }
      ],
      xAxes: [
        {
          display: true,
          ticks: {
            fontSize: 13,
            fontFamily: "kanit"
          }
        }
      ]
    }
  };

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  public barChartLabels: any = [];
  public barChartType: string = "horizontalBar";
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
    { data: [], label: "" }
  ];

  public barChartColors: Array<any> = [
    {
      backgroundColor: "rgba(0, 192, 189, 0.7)"
    }
  ];
}
