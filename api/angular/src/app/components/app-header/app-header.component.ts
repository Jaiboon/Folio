import { Component, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Location, PopStateEvent } from "@angular/common";

import { AuthenticationService } from '../../services/authentication.service';
import { User } from '../../models/user';
declare let $: any;

@Component({
	selector: 'app-header',
	templateUrl: './app-header.component.html'
})
export class AppHeaderComponent implements AfterViewInit {
	user: any;
	signedIn: boolean = false;
	// signedIn: Observable<boolean>;

	name: string;
	isAdmin: boolean;
	isRegisteredUser: boolean;
	isOIEOfficer: boolean;

	constructor(
		private location: Location,
		private authenticationService: AuthenticationService,
		private router: Router
	) { }
	ngOnInit() {

		// this.signedIn = this.authenticationService.isSignedIn();

		this.authenticationService.userChanged().subscribe(
			(user: User) => {
				this.user = user;
				this.signedIn = user.username != undefined;

				if (user.firstname == null)
					this.name = user.username;
				else
					this.name = user.firstname + ' ' + user.lastname;
				this.isAdmin = this.authenticationService.isInRole("Administrator");
				this.isOIEOfficer = this.authenticationService.isInRole("OIE Officer");
				this.isRegisteredUser = this.authenticationService.isInRole("Registered User");
			});

		// Strategy for refresh token through a scheduler.
		// this.authenticationService.startupTokenRefresh();
	}

	private lastPoppedUrl: string;
	private yScrollStack: number[] = [];

	ngAfterViewInit() {

		this.location.subscribe((ev: PopStateEvent) => {
			this.lastPoppedUrl = ev.url;
		});
		this.router.events.subscribe((ev: any) => {
			if (ev instanceof NavigationStart) {
				if (ev.url != this.lastPoppedUrl)
					this.yScrollStack.push(window.scrollY);
			} else if (ev instanceof NavigationEnd) {
				if (ev.url == this.lastPoppedUrl) {
					this.lastPoppedUrl = undefined;
					window.scrollTo(0, this.yScrollStack.pop());
				} else if (ev.url.indexOf('#') == -1 && ev.url.indexOf('%23') == -1) {
					try {
						$("html,body").stop(true, false).animate({ scrollTop: 0 }, 500);
					} catch (error) {
					}
				}
			}
		});
	}

	signout(): void {
		this.authenticationService.signout();

		this.router.navigate(['/home']);
	}

}
