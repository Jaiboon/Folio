import { Component } from '@angular/core';
import { AuthenticationService } from '../../services';

@Component({
	selector: 'app-sidebar-nav',
	templateUrl: './app-sidebar-nav.component.html'
})

export class AppSidebarNavComponent {
	isAdministrator: boolean = false;
	isEquity: boolean = false;
	isDerivatives: boolean = false;
	isIntermarkets: boolean = false;

	constructor(
		protected authenticationService: AuthenticationService
	) {
		this.isAdministrator = this.authenticationService.isInRole("Administrator");
		this.isEquity = this.authenticationService.isInRole("Equity");
		this.isDerivatives = this.authenticationService.isInRole("Derivatives");
		this.isIntermarkets = this.authenticationService.isInRole("Intermarkets");

	}
}