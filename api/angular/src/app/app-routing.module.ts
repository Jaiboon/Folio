﻿import { NgModule } from "@angular/core";
import { Routes, RouterModule, PreloadAllModules } from "@angular/router";

import { AuthGuard } from "./services/auth.guard";

import { HomeComponent } from "./components";
import { DashboardComponent } from "./dashboard/components/dashboard.component";
import { SelfAssessmentComponent } from "./self-assessment/self-assessment/self-assessment.component";
import { PMIComponent } from "./pmi/pmi/pmi.component";
import { SurveyComponent } from "./survey/survey/survey.component";

// We use PathLocationStrategy - the default "HTML 5 pushState" style:
// - https://angular.io/docs/ts/latest/guide/router.html#!#browser-url-styles
// - Router on the server (see Startup.cs) must match the router on the client to use PathLocationStrategy
// and Lazy Loading Modules:
// - https://angular.io/guide/ngmodule#lazy-loading-modules-with-the-router
const routes: Routes = [
  { path: "", redirectTo: "home", pathMatch: "full" },
  { path: "home", component: HomeComponent },
  { path: "dashboard/:id", component: DashboardComponent },
  { path: "dashboard", component: DashboardComponent },
  { path: "download", loadChildren: "./document/document.module#DocumentModule" },

  { path: "survey", loadChildren: "./survey/survey.module#SurveyModule", canActivate: [AuthGuard] },
  { path: "pmi", loadChildren: "./pmi/pmi.module#PMIModule", canActivate: [AuthGuard] },
  { path: "self-assessment", loadChildren: "./self-assessment/self-assessment.module#SelfAssessmentModule", canActivate: [AuthGuard] },
  { path: "admin", loadChildren: "./admin/admin.module#AdminModule", canActivate: [AuthGuard] },
  { path: "account", loadChildren: "./account/account.module#AccountModule" }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes
      //     , {
      //     preloadingStrategy: PreloadAllModules
      // }
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
