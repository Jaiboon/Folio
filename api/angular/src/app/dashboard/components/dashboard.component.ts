import { Component, OnInit } from "@angular/core";
import { OIEService } from "../../services";
import { ChartFilterModel } from "../../models/chart-filter-model";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  filterModel: ChartFilterModel;
  pillarNo: number = 5;
  CompareCountry: string;
  items: any =
    {
      title: "ดัชนีหลัก",
      value1: "",
      value2: "",
      subIndexItems: []
    };

  pillarByIndustry: any =
    [
    ]

  constructor(private oieService: OIEService,
    private route: ActivatedRoute
  ) {
    let id = this.route.snapshot.paramMap.get("id");
    if (id != null) {
      this.pillarNo = Number(id);
    }
  }

  ngOnInit() {
  }

  onFilterChanged(filterModel: ChartFilterModel) {
    this.filterModel = filterModel;
    this.oieService.GetCountryCompareIndicatorScore(filterModel.year, filterModel.industry, filterModel.country)
      .subscribe(
        result => {
          this.items = result;
          this.CompareCountry = this.items.compareCountry;
          this.onPillarChanged(this.pillarNo);
        }
      );
  }

  onPillarChanged(pillarNo: number) {
    this.pillarNo = pillarNo;
    this.applyCollapseShowPillar();
    this.changePillar(pillarNo);
  }



  changePillar(pillarNo: number) {
    this.oieService.GetPillarScoreGroupByIndustryAndCountry(this.filterModel.year, pillarNo)
      .subscribe(
        result => {
          this.pillarByIndustry = result;
        }
      );
  }



  applyCollapseShowPillar() {
    this.items.subIndexItems.forEach(subindex => {
      subindex.pillarItems.forEach(pillar => {
        pillar.show = pillar.groupid == this.pillarNo;
      });
    });
  }
}
