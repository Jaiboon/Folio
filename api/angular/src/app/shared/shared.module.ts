import { NgModule } from '@angular/core';
import { HttpModule, Http } from '@angular/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JwtModule } from '@auth0/angular-jwt';

import { SummernoteModule } from "./../summernote/summernote.module";
import { NgxPaginationModule } from 'ngx-pagination';

import {
    AuthGuard,
    AuthenticationService,
    IdentityService,
    OIEService,
    BrowserStorage,
    CategoryService,
    UploadService,
    ApplicationService
} from './../services';
import { SafeHtmlPipe } from '../pipes';
import { JsonFileService } from '../services/jsonfile.service';
import { Globals } from '../../globals';
import { LineChartComponent, WeaknessesChartComponent, StrengthsChartComponent, RadarIndicatorChartComponent, AppIndustryIndicatorsComponent } from '../components';
import { ChartsModule } from 'ng2-charts';


@NgModule({
    declarations: [
        SafeHtmlPipe,
        
        // AppIndustryIndicatorsComponent,
        // RadarIndicatorChartComponent,
        // StrengthsChartComponent,
        // WeaknessesChartComponent,
        // LineChartComponent
    ],
    imports: [
        // BrowserModule,
        // BrowserAnimationsModule,
        HttpModule,
        CommonModule,
        FormsModule,
        ChartsModule,
        SummernoteModule
    ],
    providers: [
        Title,
        AuthGuard,
        AuthenticationService,
        IdentityService,
        OIEService,
        CategoryService,
        UploadService,
        BrowserStorage,
        // {
        //     provide: AuthHttp,
        //     useFactory: getAuthHttp,
        //     deps: [Http]
        // }
        
        ApplicationService,
        JsonFileService,
        Globals
    ],
    exports: [
        // BrowserModule,
        // BrowserAnimationsModule,
        HttpModule,
        CommonModule,
        FormsModule,
        ChartsModule,
        SummernoteModule,

        SafeHtmlPipe,
        
        // AppIndustryIndicatorsComponent,
        // RadarIndicatorChartComponent,
        // StrengthsChartComponent,
        // WeaknessesChartComponent,
        // LineChartComponent
    ]
})

export class SharedModule { }
