import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { DocumentRoutingModule } from './document-routing.module';
import { DocumentComponent } from './document/document.component';


@NgModule({
    imports: [
        DocumentRoutingModule,
        SharedModule
    ],
    declarations: [
        DocumentComponent
    ],
    exports: [
    ]
})
export class DocumentModule { }
