﻿import { Component } from '@angular/core';
import { OIEService } from '../../services/index';
import { DocumentModel } from '../../models/document-model';
import { ActivatedRoute, Router } from '@angular/router';
import { Globals } from '../../../globals';
@Component({
    selector: 'document',
    templateUrl: './document.component.html'
})
export class DocumentComponent {

    model: DocumentModel[] = [];
    id: any;
    constructor(
        public global: Globals,
        private oieService: OIEService,
        private route: ActivatedRoute) {
        this.id = this.route.snapshot.paramMap.get('id');
        oieService.GetListDocumentDownload().subscribe(
            (data) => {
                this.model = data;
                console.log(this.model);
            },
            (error) => {

            }
        );
    }
    isShow(currentNode: DocumentModel) {
        if (this.id == currentNode.id) {
            return true;
        }
        else {
            return currentNode.documentChild.filter(o => o.id == this.id).length > 0 ? true : false;
        }
    }
    // documents: any =
    //     [
    //         {
    //             "id": 1, "type": "book", "title": "รายงานความสามารถในการแข่งขันด้านเศรษฐกิจอุตสาหกรรม", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [
    //                 { "id": 11, "type": "chapter", "title": "รายงานฉบับสมบูรณ์", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] },
    //                 { "id": 12, "type": "chapter", "title": "รายงานฉบับย่อ", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] },
    //                 { "id": 13, "type": "chapter", "title": "Methodology", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] }
    //             ]
    //         },
    //         {
    //             "id": 2, "type": "shelf", "title": "รายงานความสามารถในการแข่งขันด้านเศรษฐกิจอุตสาหกรรม (แยกรายอุตสาหกรรม)", "thumbnail": "", "url": "assets/pdf.pdf", "documents":
    //                 [
    //                     {
    //                         "id": 21, "type": "book", "title": "อุตสาหกรรมยานยนต์", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [
    //                             { "id": 211, "type": "chapter", "title": "บทวิเคราะห์ปัจจัยที่มีผลต่อการเพิ่มขีดความสามารถในการแข่งขัน", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] },
    //                             { "id": 212, "type": "chapter", "title": "การวิเคราะห์ปัจจัยแห่งความสำเร็จ (Key Success Factor)", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] },
    //                             { "id": 213, "type": "chapter", "title": "ดัชนีวัดความสามารถในการแข่งขัน", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] }
    //                         ]
    //                     },
    //                     {
    //                         "id": 22, "type": "book", "title": "อุตสาหกรรมอิเล็กทรอนิกส์อัจฉริยะ ", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [
    //                             { "id": 221, "type": "chapter", "title": "บทวิเคราะห์ปัจจัยที่มีผลต่อการเพิ่มขีดความสามารถในการแข่งขัน", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] },
    //                             { "id": 222, "type": "chapter", "title": "การวิเคราะห์ปัจจัยแห่งความสำเร็จ (Key Success Factor)", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] },
    //                             { "id": 223, "type": "chapter", "title": "ดัชนีวัดความสามารถในการแข่งขัน", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] }
    //                         ]
    //                     },
    //                     {
    //                         "id": 23, "type": "book", "title": "อุตสาหกรรมเทคโนโลยีชีวภาพ ", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [
    //                             { "id": 231, "type": "chapter", "title": "บทวิเคราะห์ปัจจัยที่มีผลต่อการเพิ่มขีดความสามารถในการแข่งขัน", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] },
    //                             { "id": 232, "type": "chapter", "title": "การวิเคราะห์ปัจจัยแห่งความสำเร็จ (Key Success Factor)", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] },
    //                             { "id": 233, "type": "chapter", "title": "ดัชนีวัดความสามารถในการแข่งขัน", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] }
    //                         ]
    //                     },
    //                     {
    //                         "id": 24, "type": "book", "title": "อุตสาหกรรมการแปรรูปอาหาร ", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [
    //                             { "id": 241, "type": "chapter", "title": "บทวิเคราะห์ปัจจัยที่มีผลต่อการเพิ่มขีดความสามารถในการแข่งขัน", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] },
    //                             { "id": 242, "type": "chapter", "title": "การวิเคราะห์ปัจจัยแห่งความสำเร็จ (Key Success Factor)", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] },
    //                             { "id": 243, "type": "chapter", "title": "ดัชนีวัดความสามารถในการแข่งขัน", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] }
    //                         ]
    //                     },
    //                     {
    //                         "id": 25, "type": "book", "title": "อุตสาหกรรมระบบอัตโนมัติและหุ่นยนต์ ", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [
    //                             { "id": 251, "type": "chapter", "title": "บทวิเคราะห์ปัจจัยที่มีผลต่อการเพิ่มขีดความสามารถในการแข่งขัน", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] },
    //                             { "id": 252, "type": "chapter", "title": "การวิเคราะห์ปัจจัยแห่งความสำเร็จ (Key Success Factor)", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] },
    //                             { "id": 253, "type": "chapter", "title": "ดัชนีวัดความสามารถในการแข่งขัน", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] }
    //                         ]
    //                     },
    //                     {
    //                         "id": 26, "type": "book", "title": "อุตสาหกรรมอากาศยาน", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [
    //                             { "id": 261, "type": "chapter", "title": "บทวิเคราะห์ปัจจัยที่มีผลต่อการเพิ่มขีดความสามารถในการแข่งขัน", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] },
    //                             { "id": 262, "type": "chapter", "title": "การวิเคราะห์ปัจจัยแห่งความสำเร็จ (Key Success Factor)", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] },
    //                             { "id": 263, "type": "chapter", "title": "ดัชนีวัดความสามารถในการแข่งขัน", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] }
    //                         ]
    //                     }
    //                 ]
    //         },
    //         {
    //             "id": 3, "type": "book", "title": "ดัชนีผู้จัดการฝ่ายจัดซื้อ (Purchasing Managers Index : PMI)", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [
    //                 { "id": 31, "type": "chapter", "title": "รายงานดัชนีผู้จัดการฝ่ายจัดซื้อ (Purchasing Managers Index : PMI) เดือน.......ปี 2561", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] },
    //                 { "id": 31, "type": "chapter", "title": "Methodology", "thumbnail": "", "url": "assets/pdf.pdf", "documents": [] }
    //             ]
    //         }
    //     ];



}
