/// <reference path="./summernote.d.ts" />

//import * as $ from 'jquery'; // old code. doesn't work
declare let $: any;
import { UploadService } from '../services/upload.service';

import {
    Component,
    ElementRef,
    forwardRef,
    Input,
    Output,
    EventEmitter,
    OnDestroy,
    OnInit,
    SimpleChanges
} from '@angular/core';

import {
    NG_VALUE_ACCESSOR,
    ControlValueAccessor
} from '@angular/forms';

const SUMMERNOTE_VALUE_ACCESSOR = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => SummernoteComponent),
    multi: true
};

@Component({
    selector: 'summernote',
    template: '<div class="summernote"></div>',
    providers: [SUMMERNOTE_VALUE_ACCESSOR]
})
export class SummernoteComponent implements OnInit, OnDestroy, ControlValueAccessor {
    @Input()
    set options(options: SummernoteOptions) {
        this._options = options;
        this.addCallbacks();
        this.refreshOptions();
    }

    get options(): SummernoteOptions {
        return this._options || {};
    }

    @Input()
    set disabled(disabled: boolean) {
        if (disabled != null) {
            this._disabled = disabled;
            $(this.element.nativeElement).find('.summernote').summernote(disabled ? 'disable' : 'enable');
            this.refreshOptions();
        }
    }

    get disabled(): boolean {
        return this._disabled;
    }

    private _empty;

    @Output() emptyChange: EventEmitter<boolean> = new EventEmitter<boolean>();

    get empty() {
        return this._empty;
    }
    set empty(value: boolean) {
        if (this._empty != value) {
            this._empty = value;
            this.emptyChange.emit(value);
        }
    }

    private _disabled: boolean = false;

    private _options: SummernoteOptions;

    private onTouched = () => { };
    private onChange: (value: string) => void = () => { };

    constructor(private element: ElementRef
        , private uploadService: UploadService
    ) {

    }
    private _value: string;

    set value(value: string) {
        this._value = value;
    }
    get value(): string {
        return this._value;
    }

    private refreshOptions() {
        $(this.element.nativeElement).find('.summernote').summernote(this.options);
        if (this.options.tooltip != undefined && !this.options.tooltip)
            (<any>$(this.element.nativeElement).find('.note-editor button.note-btn')).tooltip('destroy');
    }

    private addCallbacks() {
        this.options.callbacks = {
            onChange: (contents, $editable) => {
                this.refreshEmpty();
                this.onChange(contents);
            },
            onBlur: () => {
                this.onTouched();
            },
            onImageUpload: (files) => {
                this.uploadService.upload(files[0])
                    .subscribe(res => {
                        if (res["fileType"] == 'image') { //Image
                            $(this.element.nativeElement).find('.summernote').summernote('insertImage', res["fileUrl"], res["fileName"]);
                        }
                        else {
                            var fileName = res["fileName"];
                            var extension = fileName.slice(-3)
                            var newlink = document.createElement('a');
                            newlink.innerText = fileName;
                            newlink.setAttribute('target', '_blank');
                            newlink.setAttribute('class', 'document-' + extension);
                            newlink.setAttribute('href', res["fileUrl"]);
                            $(this.element.nativeElement).find('.summernote').summernote('insertNode', newlink);

                            // $(this.element.nativeElement).find('.summernote').summernote('createLink', {
                            //     text: ''+res.json()["fileName"],
                            //     url: res.json()["fileUrl"],
                            //     isNewWindow: true
                            // });
                        }
                    });
            }
        };
    }

    private refreshEmpty() {
        this.empty = <boolean>(<any>$(this.element.nativeElement).find('.summernote').summernote('isEmpty'));
    }

    ngOnInit() {
        if (this.options == null) {
            this.options = {};
        }
        this.refreshEmpty();
    }

    ngOnDestroy() {
        $(this.element.nativeElement).find('.summernote').summernote('destroy');
    }

    writeValue(code: string) {
        this.value = code;

        $(this.element.nativeElement).find('.summernote').summernote('code', code);
        this.refreshEmpty();
    }

    getCode(): string {
        return $(this.element.nativeElement).find('.summernote').summernote('code');
    }

    registerOnChange(fn: any) {
        this.onChange = fn;
    }

    registerOnTouched(fn: any) {
        this.onTouched = fn;
    }



    @Input()
    imgUrl: string;
    @Input()
    document: any;

    ngOnChanges(changes: SimpleChanges) {
        if (changes['imgUrl']) {
            $(this.element.nativeElement).find('.summernote').summernote('insertImage', this.imgUrl, this.imgUrl);
        } if (changes['document']) {
            if (this.document != null)
                $(this.element.nativeElement).find('.summernote').summernote('createLink', this.document);
        }
    }
}