import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../services/auth.guard';

import { PMIComponent } from './pmi/pmi.component';

const routes: Routes = [
    { path: "", component: PMIComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PMIRoutingModule { }
