import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { PMIRoutingModule } from './pmi-routing.module';
import { PMIComponent } from './pmi/pmi.component';



@NgModule({
    imports: [
        PMIRoutingModule,
        SharedModule
    ],
    declarations: [
        PMIComponent
    ],
    exports: [
        PMIComponent
    ]
})
export class PMIModule { }
