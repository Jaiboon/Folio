import { Component, OnInit } from '@angular/core';
import { JsonFileService } from '../../services/jsonfile.service';
import { OIEService } from '../../services/index';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import swal from 'sweetalert2';

class PMI_DATA {
  ID: number;
  PMI_ID : number;
  Code_ID : number;
  Month_CurrentLast_Change: String;
  Month_CurrentLast_Percent: number;
  Month_CurrentLast_Remark: String;
  Month_CurrentNext_Change: String;
  Month_CurrentNext3_Change: String;
  NameList :String;
}


class PMI_HAED {
  ID: number;
  Year: number;
  Industry_ID: number;
  FormDate: Date;
  FormNo: String;
  InformantFName: String;
  InformantLName: String;
  InformantPosition: String;
  InformantCompany: String;
  Product: String;
  SaleDomestic: number;
  SaleOversea: number;
  ExportCountry1: String;
  ExportCountry2: String;
  ExportCountry3: String;
  Phone: String;
  Fax: String;
  Email: String;
  Address: String;
  IndustrySize: String;
  CountWorker: String;
  CapitalValue: String;
  Comment_CurrentMonth_Positive: String;
  Comment_CurrentMonth_Negative: String;
  Comment_NextMonth_Positive: String;
  Comment_NextMonth_Negative: String;
  Evaluation: String;
  InterviewerFName: String;
  InterviewerLName: String;
  
}
class PMI {
  PMIHEAD: PMI_HAED;
  PMIDATA: PMI_DATA[];
  constructor() {
    this.PMIHEAD = new PMI_HAED();

  }
}

@Component({
  selector: 'pmi',
  templateUrl: './pmi.component.html'
})
export class PMIComponent implements OnInit {

  data: any = [
    {
      'Code_ID': 1,
      'NameList': '1. New Order : เปรียบเทียบจำนวนคำสั่งซื้อทั้งหมด <br>(ในประเทศและต่างประเทศ)',
      'Month_CurrentLast_Change': '',
      'Month_CurrentLast_Percent': 0,
      'Month_CurrentLast_Remark': '',
      'Month_CurrentNext_Change': '',
      'Month_CurrentNext3_Change': '',

    },
    {
      'Code_ID': 2,
      'NameList': '2. New Export Orders: เปรียบเทียบจำนวนคำสั่งซื้อใหม่จากต่างประเทศ (เฉพาะบริษัท ที่ส่งออก)',
      'Month_CurrentLast_Change': '',
      'Month_CurrentLast_Percent':0,
      'Month_CurrentLast_Remark': '',
      'Month_CurrentNext_Change': '',
      'Month_CurrentNext3_Change': '',
    },
    {
      'Code_ID': 3,
      'NameList': '3. Out Put: เปรียบเทียบผลผลิต',
      'Month_CurrentLast_Change': '',
      'Month_CurrentLast_Percent':0,
      'Month_CurrentLast_Remark': '',
      'Month_CurrentNext_Change': '',
      'Month_CurrentNext3_Change': '',
    }
    ,
    {
      'Code_ID': 4,
      'NameList': '4. Employment : เปรียบเทียบจำนวนการจ้างงาน',
      'Month_CurrentLast_Change': '',
      'Month_CurrentLast_Percent': 0,
      'Month_CurrentLast_Remark': '',
      'Month_CurrentNext_Change': '',
      'Month_CurrentNext3_Change': '',
    }
    ,
    {
      'Code_ID': 5,
      'NameList': '5. Suppliers, Delivery Times: เปรียบเทียบเวลาในการส่งวัตถุดิบของผู้ขายวัตถุดิบถึงบริษัทท่าน',
      'Month_CurrentLast_Change': '',
      'Month_CurrentLast_Percent': 0,
      'Month_CurrentLast_Remark': '',
      'Month_CurrentNext_Change': '',
      'Month_CurrentNext3_Change': '',
    }
    ,
    {
      'Code_ID': 6,
      'NameList': '6. Stocks of Purchases: เปรียบเทียบจำนวนสินค้าคงคลังของวัตถุดิบ',
      'Month_CurrentLast_Change': '',
      'Month_CurrentLast_Percent': 0,
      'Month_CurrentLast_Remark': '',
      'Month_CurrentNext_Change': '',
      'Month_CurrentNext3_Change': '',
    }
    ,
    {
      'Code_ID': 7,
      'NameList': '7. Stocks of Finished Goods. เปรียบเทียบจำนวนสินค้าคงคลังของสินค้าคงคลังของสินค้าสำเร็จรูป',
      'Month_CurrentLast_Change': '',
      'Month_CurrentLast_Percent': 0,
      'Month_CurrentLast_Remark': '',
      'Month_CurrentNext_Change': '',
      'Month_CurrentNext3_Change': '',
    }
    ,
    {
      'Code_ID': 8,
      'NameList': '8. Input Prices: เปรียบเทียบราคาต้นทุนการผลิต <br>( วัตถุดิบ ค่าขนส่ง ค่าจ้าง ฯ)',
      'Month_CurrentLast_Change': '',
      'Month_CurrentLast_Percent': 0,
      'Month_CurrentLast_Remark': '',
      'Month_CurrentNext_Change': '',
      'Month_CurrentNext3_Change': '',
    }
    ,
    {
      'Code_ID': 9,
      'NameList': '9. Output Prices: เปรียบเทียบราคาขายสินค้า',
      'Month_CurrentLast_Change': '',
      'Month_CurrentLast_Percent': 0,
      'Month_CurrentLast_Remark': '',
      'Month_CurrentNext_Change': '',
      'Month_CurrentNext3_Change': '',
    }
    ,
    {
      'Code_ID': 10,
      'NameList': '10. Backlogs of Work: เปรียบเทียบจำนวนสินค้าที่ยังผลิตไม่เสร็จ',
      'Month_CurrentLast_Change': '',
      'Month_CurrentLast_Percent': 0,
      'Month_CurrentLast_Remark': '',
      'Month_CurrentNext_Change': '',
      'Month_CurrentNext3_Change': '',
    }
    ,
    {
      'Code_ID': 11,
      'NameList': '11. Quantity of Purchases: เปรียบเทียบจำนวนการซื้อวัตถุดิบ',
      'Month_CurrentLast_Change': '',
      'Month_CurrentLast_Percent': 0,
      'Month_CurrentLast_Remark': '',
      'Month_CurrentNext_Change': '',
      'Month_CurrentNext3_Change': '',
    }
  ]


  yearNow: number = new Date().getFullYear();
  model: PMI = new PMI();
  constructor(
    private jsonFileService: JsonFileService,
    private oieService: OIEService,
    private route: ActivatedRoute,
    private router: Router
  ) {
   if(this.model.PMIHEAD.Industry_ID  == undefined || String(this.model.PMIHEAD.Industry_ID)  == '' ){
  
      this.model.PMIHEAD.Industry_ID =0;
   }
  }
  claer(){
    this.model = new PMI();
    this.data.forEach(pmidt => {
      pmidt.Month_CurrentLast_Change = "";
      pmidt.Month_CurrentLast_Percent = 0;
      pmidt.Month_CurrentLast_Remark = "";
      pmidt.Month_CurrentNext_Change = "";
      pmidt.Month_CurrentNext3_Change = "";

   });
  }
  ngOnInit() {
    
  }
  submit(){
    this.model.PMIDATA = this.data;
    this.model.PMIHEAD.Industry_ID = Number(this.model.PMIHEAD.Industry_ID);
    this.model.PMIHEAD.Year = this.yearNow;
    let dataSave = JSON.parse(JSON.stringify(this.model));
    let dataTem = dataSave;


    dataTem.PMIDATA.forEach(pmiList => {
       pmiList.NameList = "";
    });


    this.oieService.PostPMI(dataTem)
    .subscribe(
      (result) => {
        swal({ title: '', text: 'บันทึกเรียบร้อย', type: "success", confirmButtonColor: "#5cb85c" });
      },
      (error: any) => {
        swal({ title: '', text: error.message, type: 'error', confirmButtonColor: '#d33' });
      });
  }

}
