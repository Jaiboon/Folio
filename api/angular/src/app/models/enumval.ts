﻿export class EnumVal {
    public displayType: string[] = ['news', 'whatsnews', 'video', 'faq'];
    public language: string[] = ['th', 'en'];
}