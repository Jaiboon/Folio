﻿export class User {

    /**
     * Profile data as in ApplicationUser.cs.
     */
    public firstname: string;
    public lastname: string;
    /**
     * From OpenID.
     */
    public username: string;
    public password: string;
    
    /**
     * Identity resource added in Config.cs.
     */
    public roles: string[];

    public isAdministrator: boolean;
    public isOIEOfficer: boolean;
    public isRegisteredUser: boolean;


}

export class Role {
    public name: string;
}