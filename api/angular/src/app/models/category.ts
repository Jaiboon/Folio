﻿export class Category {
    public id: string;
    public displayType: string;
    public categoryCode: string;
    public categoryName: string;
}
