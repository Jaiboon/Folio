﻿export class DocumentModel {
    id: number = 0;
    documentName: string = '';
    documentFile: string = '';
    imageFile: string = '';
    parentId: number = 0;
    showHomePage: boolean = false;
    parentDocumentName: string = '';
    level: number = 0;
    documentChild: DocumentModel[];
}