﻿export class ChartFilterModel {
    public year: number;
    public industry: number;
    public country: number;
    constructor() {
        this.year = new Date().getFullYear();
        this.industry = 99;
        this.country = 98;
    }
} 