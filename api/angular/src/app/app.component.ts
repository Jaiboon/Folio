﻿import { Component, OnInit } from '@angular/core';
// import { Title } from '@angular/platform-browser';
import { Router, NavigationStart, NavigationEnd, NavigationCancel } from '@angular/router';
import { Observable } from 'rxjs';

import { AuthenticationService } from './services/authentication.service';
import { User } from './models/user';
import { setTheme } from 'ngx-bootstrap/utils';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { Globals } from '../globals';

@Component({
    selector: 'body',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

    signedIn: Observable<boolean>;
    loading: boolean = true;
    name: string;
    isAdmin: boolean;

    constructor(
        // public title: Title,
        private globals: Globals,
        private authenticationService: AuthenticationService,
        private slimLoadingBarService: SlimLoadingBarService,
        private router: Router
    ) {
        setTheme('bs4');

        //Set Default language by url
        this.router.events.subscribe((ev: any) => {
            if (ev instanceof NavigationStart) {
                this.slimLoadingBarService.start();
                this.loading = true;
            }
            else if (
                ev instanceof NavigationEnd ||
                ev instanceof NavigationCancel
            ) {
                this.slimLoadingBarService.stop();
                this.slimLoadingBarService.complete();
                this.loading = false;
            }
        });
    }

    ngOnInit() {
        // this.title.setTitle('KTZ CMS WebAPI');

        this.signedIn = this.authenticationService.isSignedIn();

        this.authenticationService.userChanged().subscribe(
            (user: User) => {
                this.name = user.firstname;
                this.isAdmin = this.authenticationService.isInRole("Administrator");
            });

        // Strategy for refresh token through a scheduler.
        // this.authenticationService.startupTokenRefresh();
    }

    signout(): void {
        this.authenticationService.signout();

        this.router.navigate(['/home']);
    }

}
