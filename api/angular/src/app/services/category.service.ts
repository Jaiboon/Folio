﻿import { Injectable } from '@angular/core';

import { Observable, BehaviorSubject, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent, timer } from 'rxjs';
import { map, filter, scan, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Config } from '../../config';
import { Category } from '../models/category';
export { Category } from '../models/category';

const API_NAME = 'api/category';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
}

/**
 * Category service
 */
@Injectable() export class CategoryService {

    public carpusel: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);

    constructor(private authHttp: HttpClient) {
    }

    public getAll(): Observable<any> {

        let url: string;
        url = `${Config.API_URL}/${API_NAME}/all`;

        return this.authHttp.get(url)
            .pipe(
                map((res: Response) => {
                    return res.json();
                }), catchError((error: any) => {
                    return Observable.throw(error);
                })
            );
    }

    public get(id: string): Observable<Category> {

        let url: string;
        url = `${Config.API_URL}/${API_NAME}?id=${id}`;

        return this.authHttp.get<Category>(url)
            .pipe(
                map((res: Response) => {
                    return res.json();
                }), catchError((error: any) => {
                    return Observable.throw(error);
                })
            );

        // .map((res: Response) => {
        //     return res.json();
        // })
        // .catch((error: any) => {
        //     return Observable.throw(error);
        // });
    }

    public save(model: any): Observable<any> {
        const body: string = JSON.stringify(model);

        return this.authHttp.post(`${Config.API_URL}/${API_NAME}`, body, httpOptions);
            // .catch((error: any) => {
            //     return Observable.throw(error);
            // });
    }

    public delete(id: string): Observable<any> {

        return this.authHttp.delete(`${Config.API_URL}/${API_NAME}/${id}`, httpOptions);
            // .catch((error: any) => {
            //     return Observable.throw(error);
            // });
    }


}
