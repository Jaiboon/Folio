﻿import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent } from 'rxjs';
import { map, filter, scan, catchError } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { Config } from '../../config';

const API_NAME = 'api/FileUpload';
declare var jQuery: any;

/**
 * Upload service
 */
@Injectable()
export class UploadService {

    public DOCUMENT_FILE_TYPE = [, 'doc', 'xls', 'mpp', 'pdf', 'ppt', 'tiff', 'bmp', 'docx', 'xlsx', 'pptx', 'ps', 'odt', 'ods', 'odp', 'odg'];

    public IMAGE_FILE_TYPE = ['png', 'gif', 'jpg', 'jpeg'];
    public VIDEO_FILE_TYPE = ['mp4', 'webm', 'ogg'];

    constructor(
        private authHttp: HttpClient
    ) {
    }

    public upload(fileToUpload: any, type: string = '') {//type[video,image,document]
        let arrFile = (fileToUpload.name).split('.');
        let extension = arrFile[arrFile.length - 1];
        if (type == 'image') {
            if (jQuery.inArray(extension.toLowerCase(), this.IMAGE_FILE_TYPE) < 0) {
                throw "Please Upload Image File!";
            }
        }
        else if (type == 'video') {
            if (jQuery.inArray(extension.toLowerCase(), this.VIDEO_FILE_TYPE) < 0) {
                throw "Please Upload Video File!";
            }
        }
        else if (type == 'document') {
            if (jQuery.inArray(extension.toLowerCase(), this.DOCUMENT_FILE_TYPE) < 0) {
                throw "Please Upload Document File!";
            }
        }

        let input = new FormData();
        input.append("file", fileToUpload);

        return this.authHttp
            .post(`${Config.API_URL}/${API_NAME}`, input).pipe(
                map((res: Response) => {
                    return res;
                }), catchError((error: any) => {
                    return Observable.throw(error);
                })
            );
    }

    public getFilename(link: string) {
        if (link == null) {
            return 'Choose file';
        }
        else {
            var arrLink = link.split('/');
            return arrLink[arrLink.length - 1];
        }
    }

}
