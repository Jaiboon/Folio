import { Injectable } from "@angular/core";
import { Observable, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent, throwError } from 'rxjs';
import { map, filter, scan, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { Config } from "../../config";
import { AuthenticationService } from "./authentication.service";

@Injectable({
  providedIn: "root"
})
export class OIEService {
  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService
  ) { }

  public GetRadar(year: number, industry: number, country: number): Observable<any> {
    return this.http.get(
      `${Config.API_URL}/api/report/${year}/${industry}/${country}`
    );
  }


  public GetTransIndicatorGraph(chartNo:number, industry: number): Observable<any> {
    return this.http.get(
      `${Config.API_URL}/api/report/transindigraph/${chartNo}/${industry}`
    );
  }

  public GetCountryCompareIndicatorScore(year: number, industry: number, country: number): Observable<any> {
    return this.http.get(
      `${Config.API_URL}/api/report/countrycompareindicatorscore/${year}/${industry}/${country}`
    );
  }

  public GetPillarScoreGroupByIndustryAndCountry(year: number, pillar: number): Observable<any> {
    return this.http.get(
      `${Config.API_URL}/api/report/pillarscoregroupbyindustryandcountry/${year}/${pillar}`
    );
  }

  public GetStrengths(year: number, industry: number, country: number): Observable<any> {
    return this.http.get(`${Config.API_URL}/api/report/strengths/${year}/${industry}/${country}`);
  }

  public GetWeaknesses(year: number, industry: number, country: number): Observable<any> {
    return this.http.get(`${Config.API_URL}/api/report/weaknesses/${year}/${industry}/${country}`);
  }
  public GetSelfAssessmentReport(year: number): Observable<any> {
    return this.http.get(`${Config.API_URL}/api/report/selfassessment/${this.authenticationService.getUserId()}/${year}`);
  }

  public PostAssessment(data: any): Observable<{}> {
    let userJson = { 'UserId': this.authenticationService.getUserId() };
    const body = Object.assign(userJson, data);

    return this.http.post(`${Config.API_URL}/api/SelfAssessment`, body);
  }

  public GetAssessment(year: number): Observable<any> {
    return this.http.get(`${Config.API_URL}/api/SelfAssessment?userId=${this.authenticationService.getUserId()}&year=${year}`);
  }

  //#############################################################
  //###################### Industry Review ######################
  //#############################################################
  public GetListIndustryReview(): Observable<any> {
    return this.http.get(`${Config.API_URL}/api/IndustryReview`);
  }

  public GetIndustryReview(year: number, industryId: number, countryId: number): Observable<any> {
    return this.http.get(`${Config.API_URL}/api/IndustryReview/${year}/${industryId}/${countryId}`);
  }

  public GetIndustryReviewByID(id: number): Observable<any> {
    return this.http.get(`${Config.API_URL}/api/IndustryReview/${id}`);
  }

  public SaveIndustryReview(data: any): Observable<{}> {
    let userJson = { 'UserId': this.authenticationService.getUserId() };
    const body = Object.assign(userJson, data);

    return this.http.post(`${Config.API_URL}/api/IndustryReview`, body);
  }

  public DeleteIndustryReview(id: number): Observable<{}> {
    let userJson = { 'UserId': this.authenticationService.getUserId() };
    const body = Object.assign(userJson, id);
    return this.http.delete(`${Config.API_URL}/api/IndustryReview/${id}`, body);
  }

  //#############################################################
  //################### IndustryIndicator #######################
  //#############################################################

  public GetListIndustryIndicator(year: number): Observable<any> {
    return this.http.get(`${Config.API_URL}/api/IndustryIndicator/get/${year}`);
  }

  public GetIndustryIndicatorDataCount(year: number): Observable<any> {
    return this.http.get(`${Config.API_URL}/api/IndustryIndicator/${year}`);
  }

  public GetIndustryIndicatorCalculate(year: number): Observable<any> {
    return this.http.post(`${Config.API_URL}/api/IndustryIndicator/${year}/${this.authenticationService.getUserId()}`,null);
  }

  public IndustryIndicatorUseSave(year: number,Inds_Id: number): Observable<any> {
    return this.http.put(`${Config.API_URL}/api/IndustryIndicator/${year}/${Inds_Id}`,null);
  }


  //#############################################################
  //########################## Document #########################
  //#############################################################
  public GetListDocument(): Observable<any> {
    return this.http.get(`${Config.API_URL}/api/Document`);
  }

  public GetDocumentByID(id: number): Observable<any> {
    return this.http.get(`${Config.API_URL}/api/Document/${id}`);
  }

  public SaveDocument(data: any): Observable<{}> {
    let userJson = { 'UserId': this.authenticationService.getUserId() };
    const body = Object.assign(userJson, data);

    return this.http.post(`${Config.API_URL}/api/Document`, body);
  }

  public DeleteDocument(id: number): Observable<{}> {
    let userJson = { 'UserId': this.authenticationService.getUserId() };
    const body = Object.assign(userJson, id);
    return this.http.delete(`${Config.API_URL}/api/Document/${id}`, body);
  }

  public GetListDocumentHomePage(): Observable<any> {
    return this.http.get(`${Config.API_URL}/api/Document/HomePage`);
  }

  public GetListDocumentDownload(): Observable<any> {
    return this.http.get(`${Config.API_URL}/api/Document/Download`);
  }

  private handleError(errorResponse: HttpErrorResponse) {
    if (errorResponse.error instanceof ErrorEvent) {
      return throwError('An error occurred:' + errorResponse.error.message);
    } else {
      return throwError(
        `Backend returned code ${errorResponse.status}, ` +
        `body was: ${errorResponse.message}`);
    }
  }

  public UpdateHomeContent(data: any): Observable<{}> {
    return this.http.post(`${Config.API_URL}/api/Setting/about`, data);
  }

  public GetHomeContent(): Observable<any> {
    return this.http.get(`${Config.API_URL}/api/Setting/about`);
  }

  //Export Master
  public GetExport_Master(apiName:string): Observable<any> {
    return this.http.get(`${Config.API_URL}/api/export-data/${apiName}`);
  }

  //Export Data
  public GetExport_Data(apiName:string,year: number): Observable<any> {
    return this.http.get(`${Config.API_URL}/api/export-data/${apiName}/${year}`);
  }


  //Export  Result
  public GetExport_Result(apiName:string,year: number, version: number): Observable<any> {
    return this.http.get(`${Config.API_URL}/api/export-data/${apiName}/${year}/${version}`);
  }


  //PMI
  public PostPMI(data: any): Observable<{}> {
    let userJson = { 'UserId': this.authenticationService.getUserId() };
    const body = Object.assign(userJson, data);
    return this.http.post(`${Config.API_URL}/api/pmi`, body);
  }

  //#############################################################
  //########################## Survey #########################
  //#############################################################

  public SurveySave(data: any): Observable<{}> {
    let userJson = { 'UserId': this.authenticationService.getUserId() };
    const body = Object.assign(userJson, data);
    return this.http.post(`${Config.API_URL}/api/survey`, body);
  }


}

