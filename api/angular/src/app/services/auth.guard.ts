﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent } from 'rxjs';
import { map, filter, flatMap, scan, catchError } from 'rxjs/operators';

import { AuthenticationService } from './authentication.service';

/**
 * Decides if a route can be activated.
 */
@Injectable() export class AuthGuard implements CanActivate {

    private signedIn: boolean;

    constructor(private authenticationService: AuthenticationService, private router: Router) { }

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        return this.authenticationService.isSignedIn()
            .pipe(
                map(
                    (signedIn: boolean) => { this.signedIn = signedIn; }
                ), flatMap(
                    () => this.authenticationService.userChanged()
                        .pipe(map(() => {
                            const url: string = state.url;

                            if (this.signedIn) {
                                let admin = this.authenticationService.isInRole("Administrator");
                                let oieOfficer = this.authenticationService.isInRole("OIE Officer");
                                let registeredUser = this.authenticationService.isInRole("Registered User");
                                if (url.startsWith("/survey")) {
                                    return admin || oieOfficer;
                                }
                                else if (url.startsWith("/pmi")) {
                                    return admin || oieOfficer;
                                }
                                else if (url.startsWith("/self-assessment")) {
                                    return admin || oieOfficer || registeredUser;
                                }
                                else if (url.startsWith("/admin")) {
                                    return admin;
                                }
                                else {
                                    this.router.navigate(['/home']);
                                    return false;
                                }
                            }

                            // Stores the attempted URL for redirecting.
                            this.authenticationService.redirectUrl = url;

                            // Not signed in so redirects to signin page.
                            this.router.navigate(['/home']);
                            return false;
                        }))
                )
            );
    }
}
