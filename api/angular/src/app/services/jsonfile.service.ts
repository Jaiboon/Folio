import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

export const CONTENT_URL_PREFIX = 'assets/';
export const DOC_CONTENT_URL_PREFIX = CONTENT_URL_PREFIX + 'content/';
@Injectable()

export class JsonFileService {

  constructor(
    private http: HttpClient
  ) {
    // Whenever the URL changes we try to get the appropriate doc
    // this.currentDocument = location.currentUrl.switchMap(path => this.getDocument(path));
  }

  public getDocument(file: string): Observable<any> {
    const requestPath = `${DOC_CONTENT_URL_PREFIX}${file}`;

    return this.http
      .get(requestPath);
  }

}
