﻿import { Injectable } from '@angular/core';

import { Observable, BehaviorSubject, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent, timer } from 'rxjs';
import { map, filter, scan, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Config } from '../../config';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
}
/**
 * Identity service (to Identity Web API controller).
 */
@Injectable() export class IdentityService {

    public users: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);

    constructor(private http: HttpClient) {
    }

    /**
     * Gets all users through http.
     */
    public getAll(roleName: string): void {
        // Sends an authenticated request.
        this.http.get(Config.API_URL + `/api/identity/GetAll?roleName=${roleName}`)
            .subscribe((value: any) => {
                this.users.next(value);
            },
                (error: any) => {
                });
    }

    public get(id: string): any {
        // Sends an authenticated request.
        return this.http.get(Config.API_URL + `/api/Identity/Get?Id=${id}`);
    }
    /**
     * Creates a new user.
     * @param model User's data
     * @return An IdentityResult
     */
    public create(model: any): Observable<any> {
        const body: string = JSON.stringify(model);

        return this.http.post(Config.API_URL + "/api/identity/Create", body, httpOptions)
            .pipe(
                map((res: Response) => {
                    return res;
                }), catchError((error: any) => {
                    return Observable.throw(error);
                })
            );

        // .map((res: Response) => {
        //     return res.json();
        // })
        // .catch((error: any) => {
        //     return Observable.throw(error);
        // });
    }

    /**
     * Deletes a user through http.
     * @param username Username of the user
     * @return An IdentityResult
     */
    public delete(username: string): void {
        const body: string = JSON.stringify(username);

        // Sends an authenticated request.
        this.http.post(Config.API_URL + "/api/api/identity/Delete", body, httpOptions)
            .subscribe(() => {
                // Refreshes the users.
                this.getAll('*');
            },
                (error: any) => {
                });
    }

    // Add other methods.

}
