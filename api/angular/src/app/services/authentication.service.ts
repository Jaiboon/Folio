﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
// import { Observable } from 'rxjs';
import { Observable, BehaviorSubject, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent, timer, throwError } from 'rxjs';
import { map, filter, scan, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

// import { HttpErrorHandler, HandleError } from '../http-error-handler.service';
import { Config } from '../../config';
import { User } from '../models/user';
import { BrowserStorage } from './browser-storage';
import { JwtHelperService } from '@auth0/angular-jwt';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
    })
}
/**
 * ROPC Authentication service.
 */
@Injectable() export class AuthenticationService {

    /**
     * Stores the URL so we can redirect after signing in.
     */
    public redirectUrl: string;

    /**
     * Behavior subjects of the user's status & data.
     */
    private signinStatus = new BehaviorSubject<boolean>(this.tokenNotExpired());
    private user = new BehaviorSubject<User>(this.getUser());

    /**
     * Token info.
     */
    private expiresIn: number;
    private authTime: number;

    /**
     * Scheduling of the refresh token.
     */
    private refreshSubscription: any;

    /**
     * Offset for the scheduling to avoid the inconsistency of data on the client.
     */
    private offsetSeconds: number = 30;
    ;

    constructor(
        private router: Router,
        private http: HttpClient,
        private browserStorage: BrowserStorage
    ) {
        if (!this.tokenNotExpired()) {
            // Removes user's info.
            this.browserStorage.remove("user_info");
            // Revokes tokens.
            this.revokeToken();
            this.revokeRefreshToken();
        }
    }

    public signin(username: string, password: string): Observable<any> {
        const tokenEndpoint: string = Config.TOKEN_ENDPOINT;

        const params: any = {
            client_id: Config.CLIENT_ID,
            grant_type: Config.GRANT_TYPE,
            username: username,
            password: password,
            scope: Config.SCOPE
        };

        const body: string = this.encodeParams(params);

        this.authTime = new Date().valueOf();

        return this.http.post(tokenEndpoint, body, httpOptions)
            .pipe(
                map((body: any) => {
                    if (typeof body.access_token !== "undefined") {
                        // Stores access token & refresh token.
                        this.store(body);
                        // Tells all the subscribers about the new status.
                        this.signinStatus.next(true);
                    }
                }), catchError(this.handleError)
            );
    }


    // refreshToken(): Observable<any> {
    //     const body = new HttpParams().set('refresh_token', this.getRefreshToken());

    //     const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

    //     const refreshObservable = this.httpClient.post<LoginResponse>(this.refreshTokenUrl, body.toString(), { headers });

    //     const refreshSubject = new ReplaySubject<LoginResponse>(1);
    //     refreshSubject.subscribe((r: LoginResponse) => {
    //         this.setAccessToken(r.token);
    //         this.setRefreshToken(r.refresh_token);
    //     }, (err) => {
    //         this.handleAuthenticationError(err);
    //     });

    //     refreshObservable.subscribe(refreshSubject);
    //     return refreshSubject;
    // }
    // /**
    //  * Unsubscribes from the scheduling of the refresh token.
    //  */
    // public unscheduleRefresh(): void {
    //     if (this.refreshSubscription) {
    //         this.refreshSubscription.unsubscribe();
    //     }
    // }

    // /**
    //  * Handles errors on refresh token, like expiration.
    //  */
    // public handleRefreshTokenError(): void {
    //     this.redirectUrl = this.router.url;

    //     // Removes user's info.
    //     this.browserStorage.remove("user_info");

    //     // Tells all the subscribers about the new status & data.
    //     this.signinStatus.next(false);
    //     this.user.next(new User());

    //     // Unschedules the refresh token.
    //     this.unscheduleRefresh();

    //     // Revokes tokens.
    //     this.revokeToken();
    //     this.revokeRefreshToken();

    //     // The user is forced to sign in again.
    //     this.router.navigate(['/account/signin']);
    // }

    /**
     * Tries to get a new token using refresh token.
     */
    public getNewToken(): Observable<any> {
        const refreshToken: string = this.browserStorage.get("refresh_token");

        const tokenEndpoint: string = Config.TOKEN_ENDPOINT;

        const params: any = {
            client_id: Config.CLIENT_ID,
            grant_type: "refresh_token",
            refresh_token: refreshToken
        };

        const body: string = this.encodeParams(params);

        this.authTime = new Date().valueOf();

        return this.http.post(tokenEndpoint, body, httpOptions)
            .pipe(
                map((body: any) => {
                    if (typeof body.access_token !== "undefined") {
                        // Stores access token & refresh token.
                        this.store(body);
                    }
                }), catchError(this.handleError)
            );
    }

    /**
     * Revokes token.
     */
    public revokeToken(): void {
        this.browserStorage.remove("access_token");
        this.browserStorage.remove("expires");
    }

    public revokeRefreshToken(): void {
        const refreshToken: string = this.browserStorage.get("refresh_token");

        if (refreshToken != null) {
            const revocationEndpoint: string = Config.REVOCATION_ENDPOINT;

            const params: any = {
                client_id: Config.CLIENT_ID,
                token_type_hint: "refresh_token",
                token: refreshToken
            };

            const body: string = this.encodeParams(params);

            this.http.post(revocationEndpoint, body, httpOptions)
                .subscribe(
                    () => {
                        this.browserStorage.remove("refresh_token");
                    });
        }
    }

    /**
     * Removes user and revokes tokens.
     */
    public signout(): void {
        this.redirectUrl = null;

        // Removes user's info.
        this.browserStorage.remove("user_info");

        // Tells all the subscribers about the new status & data.
        this.signinStatus.next(false);
        this.user.next(new User());

        // Revokes tokens.
        this.revokeToken();
        this.revokeRefreshToken();
    }

    /**
     * Calls UserInfo endpoint to retrieve user's data.
     */
    public getUserInfo(): Observable<any> {
        return this.http.get(Config.USERINFO_ENDPOINT)
            .pipe(
                map((res: Response) => {
                    return res;
                }), catchError(this.handleError)
            );
    }

    public changeUser(userInfo: any): void {
        const user: User = new User();

        user.firstname = userInfo.firstname;
        user.lastname = userInfo.lastname;
        user.username = userInfo.name;
        user.roles = userInfo.role;

        // Stores user info.
        this.storeUser(user);
        // Tells all the subscribers about the new data.
        this.user.next(user);
    }

    /**
     * Checks if user is signed in.
     */
    public isSignedIn(): Observable<boolean> {
        return this.signinStatus.asObservable();
    }

    public userChanged(): Observable<User> {
        return this.user.asObservable();
    }

    /**
     * Checks if user is in the given role.
     */
    public isInRole(role: string): boolean {
        const user: User = this.getUser();
        const roles: string[] = (user && typeof user.roles !== "undefined" ? user.roles : []);
        return roles.indexOf(role) != -1;
    }

    /**
     * Checks for presence of token and that token hasn't expired.
     */
    private tokenNotExpired(): boolean {
        const token: string = this.browserStorage.get("access_token");
        return token != null && (this.getExpiry() > new Date().valueOf());
    }

    private encodeParams(params: any): string {
        let body: string = "";
        for (const key in params) {
            if (body.length) {
                body += "&";
            }
            body += key + "=";
            body += encodeURIComponent(params[key]);
        }
        return body;
    }

    /**
     * Stores access token & refresh token.
     */
    private store(body: any): void {
        this.browserStorage.set("access_token", body.access_token);
        this.browserStorage.set("refresh_token", body.refresh_token);

        // Calculates token expiration.
        this.expiresIn = body.expires_in as number * 1000; // To milliseconds.
        this.storeExpiry(this.authTime + this.expiresIn);

        const helper = new JwtHelperService();
        const decodedToken = helper.decodeToken(body.access_token);
        // const decodedToken = helper.decodeToken(body.access_token);
        // const expirationDate = helper.getTokenExpirationDate(body.access_token);
        // const isExpired = helper.isTokenExpired(body.access_token);
        this.browserStorage.set("sub", decodedToken.sub);

        // const body = Object.assign(tokenJson, paramJson);
    }

    /**
     * Returns token expiration in milliseconds.
     */
    private getExpiry(): number {
        return parseInt(this.browserStorage.get("expires"));
    }

    private storeExpiry(exp: number): void {
        this.browserStorage.set("expires", exp.toString());
    }

    public getUserId(): number {
        if (this.tokenNotExpired()) {
            return parseInt(this.browserStorage.get("sub"));
        }
        return 0;
    }

    public getUser(): User {
        if (this.tokenNotExpired()) {
            return JSON.parse(this.browserStorage.get("user_info"));
        }
        return new User();
    }

    private storeUser(user: User): void {
        this.browserStorage.set("user_info", JSON.stringify(user));
    }

    private handleError(errorResponse: HttpErrorResponse) {
        if (errorResponse.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            return throwError('An error occurred:' + errorResponse.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            if (errorResponse.error.error == "invalid_grant")
                return throwError('Invalid username or password');
            else
                return throwError(
                    `Backend returned code ${errorResponse.status}, ` +
                    `body was: ${errorResponse.message}`);
        }
    }
}