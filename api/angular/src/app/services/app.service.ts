﻿import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import swal from 'sweetalert2'

import { BrowserStorage } from './browser-storage';
import { environment } from './../../environments/environment';
import { Config } from '../../config';
import { ChartFilterModel } from '../models/chart-filter-model';
import { Router } from '@angular/router';
declare let $: any;

@Injectable()
export class ApplicationService {

    constructor(
        private browserStorage: BrowserStorage,
        private router: Router
    ) {

    }

    public getFilter(): ChartFilterModel {
        let filter = this.browserStorage.get("filter");
        if (filter != null)
            return JSON.parse(filter);
        else {
            let filterModel = new ChartFilterModel();
            return filterModel;
        }
    }


    public setFilter(filterModel: ChartFilterModel): void {
        this.browserStorage.set("filter", JSON.stringify(filterModel));
    }


    static IsDialogShowing = false;

    // public showInfo(data: any, path?: string) {
    //     if (!ApplicationService.IsDialogShowing) {
    //         ApplicationService.IsDialogShowing = true;
    //         if (path == null) {
    //             swal({
    //                 title: '',
    //                 html: data,
    //                 confirmButtonColor: '#7cd1f9',
    //                 showLoaderOnConfirm: true
    //             }).then((result) => {
    //                 ApplicationService.IsDialogShowing = false;
    //             });
    //         }
    //         else {
    //             swal({
    //                 title: '',
    //                 html: data,
    //                 confirmButtonColor: '#7cd1f9',
    //                 showLoaderOnConfirm: true
    //             }).then((result) => {
    //                 ApplicationService.IsDialogShowing = false;
    //                 // this.navigationService.navigate(path, this.getLanguage());
    //             });
    //         }
    //     }
    // }
    public showError(title: any, message: any, path?: string) {
        if (!ApplicationService.IsDialogShowing) {
            ApplicationService.IsDialogShowing = true;
            if (path == null) {
                swal({
                    title: '',
                    html: message,
                    type: 'error',
                    allowOutsideClick: false,
                    confirmButtonColor: '#d33',
                    showLoaderOnConfirm: true
                }).then((result) => {
                    ApplicationService.IsDialogShowing = false;
                });
            }
            else {
                swal({
                    title: '',
                    html: message,
                    type: 'error',
                    allowOutsideClick: false,
                    confirmButtonColor: '#d33',
                    showLoaderOnConfirm: true
                }).then((result) => {
                    ApplicationService.IsDialogShowing = false;
                    this.router.navigate([path]);
                });
            }
        }
    }


    public showSuccess(title: any, message: any, path?: string) {
        if (path == null) {
            swal({
                title: title,
                html: message,
                type: "success",
                allowOutsideClick: false,
                confirmButtonColor: "#5cb85c",
                showLoaderOnConfirm: true
            });
        }
        else {
            swal({
                title: '',
                html: message,
                type: "success",
                allowOutsideClick: false,
                confirmButtonColor: "#5cb85c",
                showLoaderOnConfirm: true
            }).then((result) => {
                this.router.navigate([path]);
            });
        }
    }

    public getActualLink(url: string) {
        //หากเป็นลิงค์ภายใน จะทำการ replace domain name ออกเพื่อใช้งาน Angular ได้ไว
        //ตัวอย่าง
        // http://localhost:4200/th/home  =>  /th/home
        // /th/home                       =>  /th/home
        // http://www.google.co.th        =>  http://www.google.co.th
        return url == null ? "" : url.replace(window.location.origin, '');
    }

    formatDate(date: Date) {
        return ("0" + new Date(date).getDate()).slice(-2) + '/' + ("0" + (new Date(date).getMonth() + 1)).slice(-2) + '/' + new Date(date).getFullYear();
    }

    IsEmpty(value: any) {
        if (value === undefined || value === null || value === '' || value === '0' || value === 0) {
            return true;
        }
        else {
            return false;
        }
    }

    IsValidEmail(email) {
        var re = /^(([^\u0E00-\u0E7F<>()\[\]\\.,;:/\s@"]+(\.[^\u0E00-\u0E7F<>()\[\]\\.,;:/\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    goToTop() {
        $("html,body").stop(true, false).animate({ scrollTop: $('#mainNav').offset().top }, 500);
    }
    goTo(id:string) {
        $("html,body").stop(true, false).animate({ scrollTop: $(id).offset().top }, 500);
    }
}