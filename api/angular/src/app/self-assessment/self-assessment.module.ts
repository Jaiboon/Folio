import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { SelfAssessmentRoutingModule } from './self-assessment-routing.module';
import { SelfAssessmentReportComponent } from './self-assessment-report/self-assessment-report.component';
import { SelfAssessmentComponent } from './self-assessment/self-assessment.component';


@NgModule({
    imports: [
        SelfAssessmentRoutingModule,
        SharedModule
    ],
    declarations: [
        SelfAssessmentReportComponent,
        SelfAssessmentComponent
    ],
    exports: [
        SelfAssessmentReportComponent,
        SelfAssessmentComponent
    ]
})
export class SelfAssessmentModule { }
