﻿import { Component } from '@angular/core';
import { OIEService } from '../../services';
import {ElementRef,Renderer2,ViewChild} from '@angular/core';


@Component({
    selector: 'self-assessment-report',
    templateUrl: './self-assessment-report.component.html',
    styleUrls: ['./self-assessment-report.component.scss']
})
export class SelfAssessmentReportComponent {
    yearNow: number = new Date().getFullYear();
    @ViewChild("focusdv", {read: ElementRef}) focusdv: ElementRef;
    constructor(private oieService: OIEService) {

    }

    ngOnInit() {
        //TODO
        this.oieService
            .GetSelfAssessmentReport(this.yearNow)
            .subscribe(
                result => {
                    for (let lIndex = 0; lIndex < result.labels.length; lIndex++) {
                        this.barChartLabels[lIndex] = result.labels[lIndex];
                    }
                    this.barChartData = result.data;
                    this.focusdv.nativeElement.scrollTo(0, 0);
                }
            );
       
           
    }

    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true,
        legend: {
            display: false
        }
        , scales: {
            yAxes: [
                {
                    display: true,
                    ticks: {
                        fontSize: 13,
                        fontFamily: "kanit"
                    }
                }
            ],
            xAxes: [
                {
                    display: true,
                    ticks: {
                        steps: 0.5,
                        stepValue: 0.5,
                        min: 0,
                        max: 4,
                        fontSize: 13,
                        fontFamily: "kanit"
                    }
                }
            ]
        }
    };

    public barChartLabels: any = [];
    public barChartType: string = "horizontalBar";
    public barChartLegend: boolean = true;

    public barChartData: any[] = [{ data: [], label: "" }, { data: [], label: "" }];

    // events
    public chartClicked(e: any): void {
        console.log(e);
    }

    public chartHovered(e: any): void {
        console.log(e);
    }

    public barChartColors: Array<any> = [
        { backgroundColor: 'rgba(255, 128, 62,0.7)' }, { backgroundColor: 'rgba(0, 100, 189, 0.7)' }
    ]

  

}
