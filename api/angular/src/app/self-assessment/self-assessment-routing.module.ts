import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../services/auth.guard';
import { SelfAssessmentComponent } from './self-assessment/self-assessment.component';
import { SelfAssessmentReportComponent } from './self-assessment-report/self-assessment-report.component';

const routes: Routes = [
    { path: "", component: SelfAssessmentComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SelfAssessmentRoutingModule { }
