import { Component, OnInit } from '@angular/core';
import { JsonFileService } from '../../services/jsonfile.service';
import { OIEService } from '../../services/index';
import swal from 'sweetalert2';

class Company {
  industryId: number;
  companyName: string;
  companyNameTh: string;
  companyCard: string;
  addressNo: string;
  building: string;
  floor: string;
  soi: string;
  street: string;
  tumbon: string;
  amphur: string;
  province: string;
  postCode: string;
  phone: string;
  fax: string;
  website: string;
  contactName: string;
  contactSurname: string;
  contactPosition: string;
  contactNo: string;
  contactEmail: string;
  industrialEstate: boolean;
  industrialEstInfo: string;
  mainProduct: string;
  otherProduct: string;
  thaiPercentage: number;
  foreignPercentage: number;
  employeeNo: string;
  valueChainRawMat: boolean;
  valueChainComponent: boolean;
  valueChainFinishgood: boolean;
  valueChainServices: boolean;
}
class IndicatorAssesment {
  score: number;
  pillarNo: string;
  pillarName: string;
  groupOfIndicatorNo: string;
  groupOfIndicatorName: string;
  indicatorNo: string;
  indicatorName: string;
  description: string;
  assNo: number;
}

class SelfAssesment {
  assessment: Assesment;
  company: Company;
  assessmentData: IndicatorAssesment[];
  constructor() {
    this.company = new Company();
    this.assessment = new Assesment();
  }

}
class Assesment {
  UserId: number;
  id: number;
  remark: string;
  year: number;
  industryId: number;
}


@Component({
  selector: 'self-assessment',
  templateUrl: './self-assessment.component.html',
  styleUrls: ['./self-assessment.component.css']
})
export class SelfAssessmentComponent implements OnInit {

  model: SelfAssesment = new SelfAssesment();
  isPreviewReport: boolean = false;
  yearNow: number = new Date().getFullYear();
  SumPercentage : number = 0;
  constructor(
    private jsonFileService: JsonFileService,
    private oieService: OIEService,
  ) {
    this.jsonFileService.getDocument('/assessment.json')
      .subscribe((json: any) => {
        this.model = json;
      });
  }

  ngOnInit() {
    this.getEditData();
  }

  submit() {
    let dataSave = JSON.parse(JSON.stringify(this.model));
    let data = dataSave;
    data.assessmentData.forEach(assessment => {
      assessment.pillarNo =
        assessment.pillarName =
        assessment.indicatorNo =
        assessment.indicatorName =
        assessment.groupOfIndicatorNo =
        assessment.groupOfIndicatorName =
        assessment.description = "";

    });
    data.company.id = 0;
    data.assessment.id = 0;
    data.assessment.year = this.yearNow;

    data.assessment.industryId = data.company.industryId;
    //---Check Data-------------------------
    if (data.company.industryId == 0 || data.company.industryId == "") {
      swal({ title: 'กรุณากรอกข้อมูล', text: "กรุณาเลือกประเภทอุตสาหกรรม", type: 'warning', confirmButtonColor: '#d33' });
      return;
    }
    let errormsg: string = "";
    data.assessmentData.filter(ass => ass.assNo != 0).forEach(choose => {
      let chkValue: string = '';

      if (choose.score == undefined || choose.score == '' || choose.score == 0) {
        errormsg = "กรุณาเลือกคะแนน";
        return;
      }

    })

    if (errormsg != "") {
      swal({ title: errormsg, text: "", type: 'warning', confirmButtonColor: '#d33' });
      return;
    }
    //--------------------------
    this.oieService.PostAssessment(data)
      .subscribe(
        (result) => {
          this.isPreviewReport = true;
        },
        (error: any) => {
          swal({ title: '', text: error.message, type: 'error', confirmButtonColor: '#d33' });
        });
  }

  getEditData() {
    this.oieService.GetAssessment(this.yearNow)
      .subscribe(
        (dataDB) => {
          this.model.company = dataDB.company;
          this.model.assessment = dataDB.assessment;
          this.model.assessmentData.forEach(amData => {
            let amDB = dataDB.assessmentData.filter(n => n.assNo == amData.assNo);
            if (amDB.length > 0) {
              amData.score = amDB[0].score;
            }
             this.calPercentage();
            this.isPreviewReport = true;
          });
        }
      )
      
  }

  createNewSelfAssessment() {
    this.isPreviewReport = false;
  }

  calPercentage(){
    this.SumPercentage = Number(this.model.company.thaiPercentage) + Number(this.model.company.foreignPercentage);
  }
}
