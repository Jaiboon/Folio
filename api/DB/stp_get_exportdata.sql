USE [OIE_DB]
GO
/****** Object:  StoredProcedure [dbo].[stp_get_exportdata]    Script Date: 6/7/2561 16:42:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--CHOM 2018-07-04 Export Data
-- =============================================
ALTER PROCEDURE  [dbo].[stp_get_exportdata]
	@VIEWNAME NVARCHAR(300),
	@YEAR VARCHAR(4) = NULL,
	@VERSION VARCHAR(6) = NULL

AS
BEGIN
    DECLARE @TEXTSELECT NVARCHAR(1000)
	SET @TEXTSELECT =   N'SELECT * FROM ' + @VIEWNAME  
	
	--WHERE
	IF @YEAR IS NOT NULL
	BEGIN 
	 SET  @TEXTSELECT += ' WHERE Year = '+ ISNULL(@YEAR,0) 
	END
	

	IF @VERSION IS NOT NULL
	BEGIN 
	 SET  @TEXTSELECT += ' AND  Version = '+ ISNULL(@VERSION,-1) 
	END
	
	--ODER BY
	IF @VIEWNAME ='V_D_ASSESSEMENT'
	BEGIN 
	 SET  @TEXTSELECT += ' Order by Year
							,Industry_ID
							,Company_ID
							,[Assessment_ID]
							,[Assessment_Base_ID]' 
	END
	IF @VIEWNAME ='V_D_PMI_PART1'
	BEGIN 
	 SET  @TEXTSELECT += ' Order by 
								[Year]
								,[ID]'
	END
	IF @VIEWNAME ='V_D_PMI_PART2'
	BEGIN 
	 SET  @TEXTSELECT += '  ORDER BY 
							  Year 
							 ,[PMI_ID]
							 ,[Tbl_PMI_Data].[Code_ID]'
	END
	IF @VIEWNAME ='V_D_PRIMARY'
	BEGIN 
	 SET  @TEXTSELECT += '   ORDER BY [Year]
							  ,[Industry_ID]
							  ,[Country_ID]
							  ,[Indicator_ID]'
	END
	IF @VIEWNAME ='V_D_SECONDARY'
	BEGIN 
	 SET  @TEXTSELECT += '    ORDER BY [Year]
							  ,[Industry_ID]
							  ,[Country_ID]
							  ,[Indicator_ID]'
	END
	IF @VIEWNAME ='V_D_SECONDARY_THONLY'
	BEGIN 
	 SET  @TEXTSELECT += '      ORDER BY [Year]
							  ,[Industry_ID]
							  ,[Country_ID]
							  ,[Indicator_ID]'
	END
	IF @VIEWNAME ='V_D_SURVEY'
	BEGIN 
	 SET  @TEXTSELECT += '    	ORDER BY  
								[Year]
								,[Industry_ID]
								,[Country_ID]
								,[Company_ID]
								,[Indicator_ID]'
	END

	IF @VIEWNAME ='V_D_SURVEY_RECOMEND'
	BEGIN 
	 SET  @TEXTSELECT += '    	ORDER BY 
								   [ID]
								  ,[Year]
								  ,[Industry_ID]
								  ,[Company_ID]'
	END
	
	IF @VIEWNAME ='V_M_Company'
	BEGIN 
	 SET  @TEXTSELECT += '    	ORDER BY 
									[Industry_Name]
									, Company_ID	'
	END
	IF @VIEWNAME ='V_M_Index'
	BEGIN 
	 SET  @TEXTSELECT += '    	ORDER BY [Index_ID]	'
	END
	IF @VIEWNAME ='V_M_Indicator'
	BEGIN 
	 SET  @TEXTSELECT += '    	ORDER BY [ID]	'
	END
	IF @VIEWNAME ='V_M_Industry'
	BEGIN 
	 SET  @TEXTSELECT += '    	ORDER BY [ID]	'
	END
	IF @VIEWNAME ='V_R_ASSESSMENT_BASE'
	BEGIN 
	 SET  @TEXTSELECT += '    	ORDER BY [Year]
										  ,Version
										  ,[Industry_ID]
										  ,[Ass_No]	'
	END
	IF @VIEWNAME ='V_R_COMPETITIVENESS_INDEX'
	BEGIN 
	 SET  @TEXTSELECT += '    ORDER BY hierarchy_code'
	END




	EXECUTE sp_executesql   @TEXTSELECT
 
  
  
	
END
