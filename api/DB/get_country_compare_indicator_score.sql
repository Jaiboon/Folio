DROP PROCEDURE [dbo].[get_country_compare_indicator_score]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =============================================
OLE 2018-06-27 
   =============================================*/
CREATE PROCEDURE [dbo].[get_country_compare_indicator_score]
	@year int,
	@industry int,
	@country int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select 
		Thai.Index_Name, Thai.Index_ID, Thai.Index_Parent_ID, Thai.Index_Level,  Thai.GroupOfIndicatorId, Thai.GroupOfIndicatorParentId, Thai.Base_Score Thai_Base_Score, countryToCompare.Base_Score Compare_Base_Score
	from
	(
		select Base_Score, Index_Name, dx.Index_ID, dx.Index_Parent_ID, dx.Index_Level, null GroupOfIndicatorId, null GroupOfIndicatorParentId, Hierarchy_Code
		from Tbl_Inds_Indicator_det iid
			inner join Tbl_Index dx on dx.Index_ID = iid.Index_Id
			inner join Tbl_Inds_Indicator ii on ii.ID = iid.Inds_Indicator_ID
		where ii.Use_Flag = 'Y' and ii.Year = @year
			and Country_Id = 14 and Industry_ID = @industry
			and iid.Indicator_ID is null

			union all
	
		select Base_Score, i.Indicator_Name, null Index_ID, i.Index_ID Index_Parent_ID, i.Indicator_Level, i.ID GroupOfIndicatorId, i.Indicator_Parent_ID GroupOfIndicatorParentId, Hierarchy_Code
		from Tbl_Inds_Indicator_det iid
			inner join Tbl_Indicator i on i.ID = iid.Indicator_ID
			inner join Tbl_Inds_Indicator ii on ii.ID = iid.Inds_Indicator_ID
		where ii.Use_Flag = 'Y' and ii.Year = @year
			and Country_Id = 14 and Industry_ID = @industry
	) Thai
	left join
	(
		select Base_Score, Index_Name, dx.Index_ID, dx.Index_Parent_ID, null GroupOfIndicatorId, null GroupOfIndicatorParentId 
		from Tbl_Inds_Indicator_det iid
			inner join Tbl_Index dx on dx.Index_ID = iid.Index_Id
			inner join Tbl_Inds_Indicator ii on ii.ID = iid.Inds_Indicator_ID
		where ii.Use_Flag = 'Y' and ii.Year = @year
			and Country_Id = @country and Industry_ID = @industry
			and iid.Indicator_ID is null

		union all
	
		select Base_Score, i.Indicator_Name, null Index_ID, i.Index_ID Index_Parent_ID, i.ID GroupOfIndicatorId, Indicator_Parent_ID GroupOfIndicatorParentId 
		from Tbl_Inds_Indicator_det iid
			inner join Tbl_Indicator i on i.ID = iid.Indicator_ID
			inner join Tbl_Inds_Indicator ii on ii.ID = iid.Inds_Indicator_ID
		where ii.Use_Flag = 'Y' and ii.Year = @year
			and Country_Id = @country and Industry_ID = @industry
	) countryToCompare
	on 
	ISNULL(Thai.Index_ID, '') = ISNULL(countryToCompare.Index_ID, '')
	and ISNULL(Thai.Index_Parent_ID, '') = ISNULL(countryToCompare.Index_Parent_ID, '')
	and ISNULL(Thai.GroupOfIndicatorId, '') = ISNULL(countryToCompare.GroupOfIndicatorId, '')
	and ISNULL(Thai.GroupOfIndicatorParentId, '') = ISNULL(countryToCompare.GroupOfIndicatorParentId, '')
	order by Hierarchy_Code
END
GO

exec sp_executesql N'get_country_compare_indicator_score @year, @industry, @country',N'@year int,@industry int,@country int',@year=2018,@industry=2,@country=8