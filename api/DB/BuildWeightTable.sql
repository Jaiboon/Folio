IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BuildWeightTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[BuildWeightTable]
GO


CREATE PROCEDURE [dbo].[BuildWeightTable]
	@Year int,
	@UserId int
AS

declare @IndusIndicatorId int
begin tran
BEGIN TRY
	declare @CntPri int, @CntSec int , @NextVersion int 

	declare @GetForNewVersion table (CntPri int, CntSec int , NextVersion int )
	insert into @GetForNewVersion exec Get_For_NewVersion @Year
	select @CntPri = CntPri, @CntSec =CntSec , @NextVersion = NextVersion
	From @GetForNewVersion


	----count primary
	--select @CntPri = COUNT(*)
	--from (
	--	select distinct industry_id, Company_ID
	--	from Tbl_PrimaryData
	--	where YEAR = @Year
	--)t

	----count secondary
	--select @CntSec = COUNT(*)
	--from Tbl_SecondaryData
	--where YEAR = @Year
	--and ndFlag = 'Y'


	insert Tbl_Inds_Indicator(Year, Version, COUNT_PRIMARY, COUNT_SECONDARY, Use_Flag, Create_User, Create_Date, Modify_User, Modify_Date)
		--select @Year, isnull(max(version), -1) + 1, @CntPri, @CntSec, 'N', @UserId, getdate(), @UserId, getdate()
		select @Year, @NextVersion , @CntPri, @CntSec, 'N', @UserId, getdate(), @UserId, getdate()
		--from Tbl_Inds_Indicator --mod by jeab 2018-07-03
		--where [YEAR] = @Year -- mod by jeab  2018-07-03
		
	set @IndusIndicatorId = @@IDENTITY

	-------------- ���ӧҹ��鹵͹��� �  -------------------
	exec Build_Indus_indicator_Version_Mapping	@IndusIndicatorId, @UserId -- �� mapping �ͧ version ���

	exec BuildIndicatorWeightStructure @IndusIndicatorId, @Year, @UserId, 6
	exec BuildBaseScoreStructure @IndusIndicatorId, @Year, @UserId, 6
	exec CalculateSummaryBaseScore @IndusIndicatorId, @UserId

	exec BuildTHFactor @IndusIndicatorId, @UserId
	exec BuildAssBase @IndusIndicatorId, @UserId
	
	---------------------------------------------------

	--	
	declare @IndustryShow int = 1 ---����纤�Ңͧ�ص��ˡ����ҹ¹��
	declare @toplevlidx int, @avgindex numeric(27,9)
	
	select @toplevlidx = Index_ID
	from Tbl_Index
	where Index_Level = 1

	select @avgindex = Base_Score
	from Tbl_Inds_Indicator_det
	where Inds_Indicator_ID = @IndusIndicatorId
		and Index_Id = @toplevlidx
		and Country_Id = 98
		and Industry_ID = @IndustryShow
	
	update Tbl_Inds_Indicator
	set avg_index = @avgindex
	where ID = @IndusIndicatorId

	commit
END TRY
BEGIN CATCH
	rollback
	declare @ErrorMsg varchar(max), @ErrorSeverity int, @ErrorState int
	select @ErrorMsg = ERROR_MESSAGE()
		, @ErrorSeverity = ERROR_SEVERITY()
		, @ErrorState = ERROR_STATE()

	RAISERROR(@ErrorMsg, @ErrorSeverity, @ErrorState)
END CATCH

