IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateBaseScore]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CalculateBaseScore]
GO


CREATE PROCEDURE [dbo].[CalculateBaseScore]
	@IndusIndicatorId int,
	@UserId int
AS
begin tran
BEGIN TRY
	declare @Year int
	
	select @Year = [YEAR]
	from Tbl_Inds_Indicator
	where ID = @IndusIndicatorId
	
	exec BuildBaseScoreStructure @IndusIndicatorId, @Year, @UserId, 6
		
	commit
END TRY
BEGIN CATCH
	rollback
	declare @ErrorMsg varchar(max), @ErrorSeverity int, @ErrorState int
	select @ErrorMsg = ERROR_MESSAGE()
		, @ErrorSeverity = ERROR_SEVERITY()
		, @ErrorState = ERROR_STATE()

	RAISERROR(@ErrorMsg, @ErrorSeverity, @ErrorState)
END CATCH

