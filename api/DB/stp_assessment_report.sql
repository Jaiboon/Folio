DROP PROCEDURE [dbo].[stp_assessment_report]
GO

/****** Object:  StoredProcedure [dbo].[stp_assessment_report]    Script Date: 28/6/2561 11:23:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
--CHOM 2018-06-27  ����Ѻ���Ң����������͡��§ҹ��ҿ

-- =============================================
CREATE PROCEDURE [dbo].[stp_assessment_report]
	@UserId int,
	@YEAR int
AS
BEGIN
	WITH TblAssNo AS (
			SELECT 1 assNo, '��Ҿ�Ǵ�����ҧ�ѧ��������ɰ�Ԩ���Ҥ (���е�ҧ�����)' AS  IndexName
			UNION ALL SELECT 2 assNo, '����Է���Ҿ��С�ô��Թ��º���Ҥ�Ѱ'  
			UNION ALL SELECT 3 assNo, '�к���������ç���ҧ��鹰ҹ'  
			UNION ALL SELECT 4 assNo, '�к�����֡��'  
			UNION ALL SELECT 5 assNo, '�Ѩ����ç�ҹ: ����ҳ��Фس�Ҿ'  
			UNION ALL SELECT 6 assNo, '�ѡ��Ҿ�ͧ����ͧ�ѡ�'  
			UNION ALL SELECT 7 assNo, '�ӹҨ����ͧ�Ѻ�����'  
			UNION ALL SELECT 8 assNo, '���ŧ�ع��ҹ�Ԩ����оѲ��'  
			UNION ALL SELECT 9 assNo, '���ط����Ҿ���'  
			UNION ALL SELECT 10 assNo, '��Ե�Ҿ��ü�Ե'  
			UNION ALL SELECT 11 assNo, '��ü�Ե������Եõ������Ǵ����'  
			UNION ALL SELECT 12 assNo, '��ú����èѴ���ͧ���'  
			UNION ALL SELECT 13 assNo, '����ҳ��è�˹��� (���е�ҧ�����)'  
			UNION ALL SELECT 14 assNo, '��÷ӡ���'  
			UNION ALL SELECT 15 assNo, '�ӹҨ����ͧ�Ѻ�١��� '  
			UNION ALL SELECT 16 assNo, '������͹Ҥ'  
	)	

	SELECT   
	 --A.ID
		--, A.[Year]
		--, A.Industry_ID 
		--, AB.Ass_No
		 AB.Th_Score AS BaseScore
		, AD.Score AS MyScore
		, ID.IndexName
		, A.ID, AB.ID
	FROM Tbl_Assessment AS A
		INNER JOIN Tbl_Assessment_Base AS AB ON A.Year = AB.Year AND A.Industry_ID = AB.Industry_ID
		INNER JOIN TblAssNo AS ID ON ID.assNo= AB.Ass_No
		--INNER JOIN Tbl_Inds_Indicator II ON II.ID = AB.Inds_Indicator_ID 
		INNER JOIN  Tbl_Assessment_Data AS AD ON AD.Assessment_ID = A.ID AND AD.Assessment_Base_ID = AB.ID
	WHERE A.[Modify_User] = @UserId AND A.[Year] = @YEAR
END

GO


--exec sp_executesql N'stp_assessment_report @userId, @year',N'@userId int,@year int',@userId=1,@year=2018


--		SELECT * FROM Tbl_Assessment_Base AB
--		INNER JOIN Tbl_Inds_Indicator II ON II.ID = AB.Inds_Indicator_ID
--		WHERE II.Use_Flag = 'N' AND II.Year = 2018 AND Industry_ID = 1

exec sp_executesql N'stp_assessment_report @userId, @year',N'@userId int,@year int',@userId=1,@year=2018