DROP PROCEDURE stp_IndustryIndicatorUseSave
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =============================================
Nipon 2018-07-03 
   =============================================*/
CREATE PROCEDURE stp_IndustryIndicatorUseSave
	-- Add the parameters for the stored procedure here
	@year int,
	@industry_Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update Tbl_Inds_Indicator
	set Use_Flag='N'
	where year= @year
	and Use_Flag='Y'

	update Tbl_Inds_Indicator
	set Use_Flag='Y'
	where year= @year 
	and ID = @industry_Id
	and Use_Flag='N'

END
GO