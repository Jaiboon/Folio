IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Build_Indus_indicator_Version_Mapping]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Build_Indus_indicator_Version_Mapping]
GO


CREATE PROCEDURE [dbo].[Build_Indus_indicator_Version_Mapping]
	@Inds_Indicator_ID int,
	@User int
AS

begin tran
BEGIN TRY

/*
����Ѻ	1. �ҡ��鹻����� �ѧ����բ����Żչ��
		1.1	Insert Tbl_PrimaryData �ҡ�չ���ѧ����բ����� Tbl_PrimaryData  
		1.2 Insert Tbl_SecondaryData �ҡ�չ���ѧ����բ����� Tbl_SecondaryData 
		2. �ӹǹ
		2.1 �ӹǹ Min Max ���� update Tbl_SecondaryData
		2.2 �ӹǹ Rescale ���� update Tbl_SecondaryData
		3. insert Tbl_Inds_Indicator_Primary �纤�������ѹ�ͧ Tbl_Inds_Indicator �Ѻ Tbl_PrimaryData
		4. Insert Tbl_Inds_Indicator_Secondary �纤�������ѹ�ͧ Tbl_Inds_Indicator �Ѻ Tbl_SecondaryData
*/ 
--declare @Inds_Indicator_ID int , @User int
--select @Inds_Indicator_ID = 4 , @User = '0' 
	
If ISNULL(@User,'') = ''
	Select @User = '0'
	
declare @ThisYear int , @Version int
Select @ThisYear = YEAR(getdate())
if @ThisYear > 2500
	select @ThisYear = @ThisYear - 543
	
----TEST-----------------
--select @Inds_Indicator_ID = 3 , @ThisYear = 2019

	
----1. �ҡ�������� �ѧ����բ����Żչ��
----1.1 Insert Tbl_PrimaryData �ҡ�չ���ѧ����բ����� Tbl_PrimaryData  
declare @cntPrimaryData int 
select top 1 @cntPrimaryData = 1 from Tbl_PrimaryData   a where a.[Year] = @ThisYear and a.DropFlag = '0'
if ISNULL(@cntPrimaryData,0) < 1
   begin
		declare @LastVersion int  
		--�� version �����ͧ������ش
		select top 1 @LastVersion = [id]  from dbo.Tbl_Inds_Indicator a (nolock)
		where a.Use_Flag ='Y' and [YEAR] < @ThisYear order by [YEAR] desc , Create_Date desc 
		
		Insert Into [dbo].[Tbl_PrimaryData]([Year],[Industry_ID],[Country_ID],[Company_ID],[Indicator_ID],[Score],[Remark],[ImportFlag],[DropFlag],[Create_User],	[Create_Date],[Modify_User] ,[Modify_Date] )
		Select @ThisYear [Year],a.[Industry_ID],a.[Country_ID],a.[Company_ID],a.[Indicator_ID],a.[Score],a.[Remark],a.[ImportFlag],'0'[DropFlag],@user[Create_User] ,getdate()[Create_Date],@user [Modify_User] ,getdate()[Modify_Date] 
		FROM [OIE_DB].[dbo].[Tbl_PrimaryData] a inner join  dbo.Tbl_Inds_Indicator_Primary m (nolock) on a.[id] = m.Primary_Data_ID
		where m.Inds_Indicator_ID =@LastVersion  
	End 

----1.2 Insert Tbl_SecondaryData �ҡ�չ���ѧ����բ����� Tbl_SecondaryData 
declare @cntSecondaryData int 
select top 1 @cntSecondaryData = 1 from Tbl_SecondaryData   a where a.[Year] = @ThisYear
if ISNULL(@cntSecondaryData,0) < 1
   begin
		Insert Into [dbo].[Tbl_SecondaryData](	[Year],	[Industry_ID],[Country_ID] ,[Indicator_ID] ,[Score] ,[MaxScore],[MinScore] ,[SumScore],[CntScore] ,	[Recale_Score]	,[ImportFlag] ,	[RefYear] ,	[ndFlag] ,	[ndSource] ,[Create_User],	[Create_Date],[Modify_User] ,[Modify_Date] )
		Select [Year],	[Industry_ID],[Country_ID] ,[Indicator_ID] ,avg([Score])  [Score] ,NULL [MaxScore],NULL [MinScore] ,sum([Score]) [SumScore],count([Score]) [CntScore] ,	NULL [Recale_Score]	,NULL [ImportFlag] ,	NULL [RefYear] 
		,'C' [ndFlag] ,	NULL [ndSource] ,@user[Create_User] ,getdate()[Create_Date],@user [Modify_User] ,getdate()[Modify_Date] 
		FROM [OIE_DB].[dbo].[Tbl_PrimaryData] a
		where a.[Year] = @ThisYear and a.DropFlag = '0'  -- ੾�з����
		group by [YEAR], Industry_ID   ,Country_ID  ,Indicator_ID
	end 
	
--2. �ӹǹ
--2.1 �ӹǹ Min Max ���� update Tbl_SecondaryData
	--select MaxScore  , s.maxs, 		MinScore  , s.mins
	Update a 	Set MaxScore = s.maxs, 		MinScore = s.mins
	from Tbl_SecondaryData a inner join
		(select sec.year, sec.Industry_ID, sec.Indicator_ID, 
				MAX(sec.Score) maxs, 	MIN(sec.Score) mins
		from Tbl_SecondaryData sec 
		where sec.[Year] = @ThisYear and sec.Country_ID <> 99
		group by sec.year, sec.Industry_ID, Sec.Indicator_ID) s on a.Year = s.Year and a.Industry_ID = s.Industry_ID and a.Indicator_ID = s.Indicator_ID 	and a.ImportFlag = 'C'	and a.[Year] = @ThisYear
	where a.[Year] = @ThisYear
	
--2.2 �ӹǹ Rescale ���� update Tbl_SecondaryData

  ---2.2.1 Rescale Formula = 0
	--select '2.2.1',sec.Recale_Score ,((((sec.Score-sec.MinScore)/(sec.MaxScore - sec.MinScore)) * 9 ) + 1 ) newScore
	Update Sec Set Sec.Recale_Score = ((((sec.Score-sec.MinScore)/(sec.MaxScore - sec.MinScore)) * 9 ) + 1 )
	from Tbl_SecondaryData Sec
	inner join Tbl_Indicator Indi on Sec.Indicator_ID = indi.ID
	where indi.Rescale_Formula = 0
	and sec.ImportFlag = 'C'
	and sec.[Year] = @ThisYear

  --- 2.2.2 Rescale Formula = 1
	--select '2.2.2' ,Sec.Recale_Score ,((((sec.Score-sec.MaxScore)/( sec.MinScore - sec.MaxScore )) * 9 ) + 1 ) NewScale
	Update Sec Set Sec.Recale_Score = ((((sec.Score-sec.MaxScore)/( sec.MinScore - sec.MaxScore )) * 9 ) + 1 )
	from Tbl_SecondaryData Sec
	inner join Tbl_Indicator Indi on Sec.Indicator_ID = indi.ID
	where indi.Rescale_Formula = 1
	and sec.ImportFlag = 'C'
	and sec.[Year] = @ThisYear

----3. insert Tbl_Inds_Indicator_Primary �纤�������ѹ�ͧ Tbl_Inds_Indicator �Ѻ Tbl_PrimaryData � �͹���
Insert into dbo.[Tbl_Inds_Indicator_Primary](Inds_Indicator_ID  ,	Primary_Data_ID ,	[Create_User],	[Create_Date],[Modify_User] ,[Modify_Date] )
	select @Inds_Indicator_ID Inds_Indicator_ID  , a.[id] Primary_Data_ID ,@User [Create_User],getdate()[Create_Date],@User [Modify_User] ,getdate()[Modify_Date] 
	From Tbl_PrimaryData a where a.[Year] = @ThisYear and a.DropFlag ='0'  -- ੾�з����
	order by a.[id]
----4. Insert Tbl_Inds_Indicator_Secondary �纤�������ѹ�ͧ Tbl_Inds_Indicator �Ѻ Tbl_SecondaryData  � �͹���
 insert into [dbo].[Tbl_Inds_Indicator_Secondary](Inds_Indicator_ID  ,Secondary_Data_ID ,[Create_User],	[Create_Date],[Modify_User] ,[Modify_Date] )
	select @Inds_Indicator_ID Inds_Indicator_ID  ,a.[id] Secondary_Data_ID ,@user [Create_User], GETDATE()	[Create_Date],@user [Modify_User] , getdate() [Modify_Date] 
	From dbo.Tbl_SecondaryData a where a.[Year] = @ThisYear  order by a.[id]
 
	
--select 'pri', * from [Tbl_PrimaryData] where YEAR=@ThisYear
--select 'snd', * from [Tbl_SecondaryData] where YEAR=@ThisYear
--select 'mp',* from [Tbl_Inds_Indicator_Primary] where	Inds_Indicator_ID = @Inds_Indicator_ID
--select 'mp',* from [Tbl_Inds_Indicator_secondary] where	Inds_Indicator_ID = @Inds_Indicator_ID
--rollback tran

	commit
END TRY
BEGIN CATCH
	rollback
	declare @ErrorMsg varchar(max), @ErrorSeverity int, @ErrorState int
	select @ErrorMsg = ERROR_MESSAGE()
		, @ErrorSeverity = ERROR_SEVERITY()
		, @ErrorState = ERROR_STATE()

	RAISERROR(@ErrorMsg, @ErrorSeverity, @ErrorState)
END CATCH


GO


