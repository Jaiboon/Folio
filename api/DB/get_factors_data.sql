DROP PROCEDURE [dbo].[get_factors_data]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =============================================
Nipon 2018-06-27 
   =============================================*/
CREATE PROCEDURE [dbo].[get_factors_data]
	-- Add the parameters for the stored procedure here
	@year int,
	@industry int,
	@country int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT tf.[year]
      , tf.[Industry_ID]
      , tf.[Country_ID]
      , tf.[Indicator_ID]
      , tf.[Indicator_Name] Index_Name
      , tf.[Score] Base_Score
      , tf.[DisplayFlag]
	  , tf.[Score] * CASE WHEN DisplayFlag = 'S' THEN 1 ELSE -1 END
	FROM [Tbl_ThFactor] tf
		inner join [Tbl_Inds_Indicator] ii on tf.Inds_Indicator_ID=ii.ID
			and ii.Year=@year
			and ii.Use_Flag='Y'
	WHERE tf.[year] = @year
		and tf.[Industry_ID] = @industry
		and tf.[Country_ID] = 14
	ORDER BY 8 DESC 
END
GO

exec sp_executesql N'get_factors_data @year, @industry, @country',N'@year int,@industry int,@country int',@year=2018,@industry=2,@country=2