IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BuildIndicatorWeightStructure]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[BuildIndicatorWeightStructure]
GO


CREATE   PROCEDURE [dbo].[BuildIndicatorWeightStructure]
	@IndusIndicatorId int,
	@Year int,
	@UserId int,
	@Level int
AS

--Build index/indicator structure
IF(@Level in (5, 6))
  BEGIN
	--Add Data
	insert Tbl_Inds_Indicator_det(Inds_Indicator_ID, Country_Id, Industry_ID, Index_Id, Indicator_ID, Indi_wg, Create_User, Create_Date, Modify_User, Modify_Date)
		select @IndusIndicatorId, d.Country_ID, d.Industry_ID, idt.Index_ID, d.Indicator_ID, 0, @UserId, getdate(), @UserId, getdate()
		from Tbl_SecondaryData d
			inner join Tbl_Indicator idt on d.Indicator_ID = idt.ID
		where [YEAR] = @Year and idt.Indicator_Level = @Level
			and Country_ID <> 99
	
	--Add Parent
	insert Tbl_Inds_Indicator_det(Inds_Indicator_ID, Country_Id, Industry_ID, Index_Id, Indicator_ID, Indi_wg, Create_User, Create_Date, Modify_User, Modify_Date)
		select @IndusIndicatorId, d.Country_Id, d.Industry_ID, idt.Index_ID, idt.Indicator_Parent_ID, 0, @UserId, getdate(), @UserId, getdate()
		from Tbl_Inds_Indicator_det d
			inner join Tbl_Indicator idt on d.Indicator_ID = idt.ID
		where d.Inds_Indicator_ID = @IndusIndicatorId and idt.Indicator_Level = @Level
		group by d.Country_Id, d.Industry_ID, idt.Index_ID, idt.Indicator_Parent_ID
	
	--Build Next up level
	set @Level = @Level - 1
	exec BuildIndicatorWeightStructure @IndusIndicatorId, @Year, @UserId, @Level
  END
ELSE IF(@Level > 1)
  BEGIN
	--Add Parent
	insert Tbl_Inds_Indicator_det(Inds_Indicator_ID, Country_Id, Industry_ID, Index_Id, Indicator_ID, Indi_wg, Create_User, Create_Date, Modify_User, Modify_Date)
		select @IndusIndicatorId, d.Country_Id, d.Industry_ID
			, case when d.Indicator_ID is not null then d.Index_Id else i.Index_Parent_ID end
			, NULL
			, max(isnull(ii.Index_Weight, 1))
			, @UserId, getdate(), @UserId, getdate()
		from Tbl_Inds_Indicator_det d
			left join Tbl_Indicator idt on isnull(d.Indicator_ID, '') = idt.ID
			inner join Tbl_Index i on i.Index_ID = d.Index_ID
			left join Tbl_Industry_Index ii on ii.Year = @Year and ii.Industry_ID = d.Industry_ID and ii.Index_ID = case when d.Indicator_ID is not null then d.Index_Id else i.Index_Parent_ID end -- i.Index_ID
		where d.Inds_Indicator_ID = @IndusIndicatorId and isnull(idt.Indicator_Level, i.Index_Level) = @Level
		group by d.Country_Id, d.Industry_ID, case when d.Indicator_ID is not null then d.Index_Id else i.Index_Parent_ID end

	--Build Next Up Level
	set @Level = @Level - 1
	exec BuildIndicatorWeightStructure @IndusIndicatorId, @Year, @UserId, @Level
  END

--Fill Up Weight Level 4
update d
set Indi_wg = d3.WG
from (
	select d4.Country_Id, d4.Industry_ID, d4.Index_ID, max(d3.indi_wg)/count(d3.indi_wg) as WG
	from Tbl_Inds_Indicator_det d4
	inner join Tbl_Indicator idt on d4.Indicator_ID = idt.ID and idt.Indicator_Level = 4
	inner join Tbl_Inds_Indicator_det d3 on d3.Inds_Indicator_ID = d4.Inds_Indicator_ID and d3.Country_Id = d4.Country_Id and d3.Industry_ID = d4.Industry_ID and d3.Index_Id = d4.Index_ID and d3.Indicator_ID is null
	inner join Tbl_Index idx on d3.Index_Id = idx.Index_ID and idx.Index_Level = 3 
	where d4.Inds_Indicator_ID = @IndusIndicatorId
	group by d4.Country_Id, d4.Industry_ID, d4.Index_ID
--order by d.Country_Id, d.Industry_ID, d.Indicator_ID
) d3
inner join Tbl_Inds_Indicator_det d on d.Country_Id = d3.Country_Id and d.Industry_ID = d3.Industry_ID and d.Index_ID = d3.Index_ID
inner join Tbl_Indicator idt on d.Indicator_ID = idt.ID and idt.Indicator_Level = 4
where d.Inds_Indicator_ID = @IndusIndicatorId


--Fill Up Weight Level 5

--update d
--set Indi_wg = d3.WG
--from (
--	select d4.Country_Id, d4.Industry_ID, idt.Indicator_Parent_ID, max(d3.indi_wg)/count(d3.indi_wg) as WG
--	from Tbl_Inds_Indicator_det d4
--	inner join Tbl_Indicator idt on d4.Indicator_ID = idt.ID and idt.Indicator_Level = 5
--	inner join Tbl_Inds_Indicator_det d3 on d3.Inds_Indicator_ID = d4.Inds_Indicator_ID and d3.Country_Id = d4.Country_Id and d3.Industry_ID = d4.Industry_ID and d3.Indicator_ID = idt.Indicator_Parent_ID
--	inner join Tbl_Indicator idx on d3.Indicator_ID = idx.ID and idx.Indicator_Level = 4
--	where d4.Inds_Indicator_ID = @IndusIndicatorId
--	group by d4.Country_Id, d4.Industry_ID, idt.Indicator_Parent_ID
----order by d.Country_Id, d.Industry_ID, d.Indicator_ID
--) d3
--inner join Tbl_Inds_Indicator_det d on d.Country_Id = d3.Country_Id and d.Industry_ID = d3.Industry_ID 
--inner join Tbl_Indicator idt on d.Indicator_ID = idt.ID and idt.Indicator_Level = 5 and idt.Indicator_Parent_ID = d3.Indicator_Parent_ID
--where d.Inds_Indicator_ID = @IndusIndicatorId

--src 1,2 --> 1
--src 3	  --> 2
--src 4	  --> 4

--124 [1,2,4] [3,5,6] [7] �� set �� sum list
--1 --> (1) 1/1 [x/y] -->avg count
--2 --> (2) 1/1
--4 --> (4) 1/1

--3 --> (1) 2/3, (2) 1/3
--5 --> (1) 2/3, (4) 1/3
--6 --> (2) 1/2, (4) 1/2

--7	--> (1) 2/4, (2) 1/4, (4) 1/4

declare @xCalcSet xml = '
<CALC_SET>
	<SET ID="1" SUBID="1" X="1" Y="1" />
	<SET ID="2" SUBID="2" X="1" Y="1" />
	<SET ID="4" SUBID="4" X="1" Y="1" />
	<SET ID="3" SUBID="1" X="2" Y="3" />
	<SET ID="3" SUBID="2" X="1" Y="3" />
	<SET ID="5" SUBID="1" X="2" Y="3" />
	<SET ID="5" SUBID="4" X="1" Y="3" />
	<SET ID="6" SUBID="2" X="1" Y="2" />
	<SET ID="6" SUBID="4" X="1" Y="2" />
	<SET ID="7" SUBID="1" X="1" Y="2" />
	<SET ID="7" SUBID="2" X="1" Y="4" />
	<SET ID="7" SUBID="4" X="1" Y="4" />
</CALC_SET>
'

--CalcSet
update d
set Indi_wg = d3.WG
from (
	select d5.Country_Id, d5.Industry_ID, idt.Indicator_Parent_ID
		, case idt.Indicator_Source
					when 1 then 1
					when 2 then 1
					when 3 then 2
					when 4 then 4
					else 0
				  end Indicator_Source
		, (max(d4.indi_wg * c.value('(@X)[1]', 'int') / c.value('(@Y)[1]', 'int')) )/count(d4.indi_wg) as WG
	from Tbl_Inds_Indicator_det d5
	inner join Tbl_Indicator idt on d5.Indicator_ID = idt.ID and idt.Indicator_Level = 5
	inner join Tbl_Inds_Indicator_det d4 on d4.Inds_Indicator_ID = d5.Inds_Indicator_ID and d4.Country_Id = d5.Country_Id and d4.Industry_ID = d5.Industry_ID and d4.Indicator_ID = idt.Indicator_Parent_ID
	inner join Tbl_Indicator idx on d4.Indicator_ID = idx.ID and idx.Indicator_Level = 4
	inner join (
		select d.Country_Id, d.Industry_ID, d.Indicator_Parent_ID, sum(srclist) CalcSet
		from (
			select distinct d.Country_Id, d.Industry_ID, idt.Indicator_Parent_ID
				, case idt.Indicator_Source
					when 1 then 1
					when 2 then 1
					when 3 then 2
					when 4 then 4
					else 0
				  end as SrcList
			from Tbl_Inds_Indicator_det d
			inner join Tbl_Indicator idt on d.Indicator_ID = idt.ID and idt.Indicator_Level = 5
			where d.Inds_Indicator_ID = @IndusIndicatorId
		) d
		group by d.Country_Id, d.Industry_ID, d.Indicator_Parent_ID
	) cs on d5.Country_Id = cs.Country_Id and d5.Industry_ID = cs.Industry_ID and idt.Indicator_Parent_ID = cs.Indicator_Parent_ID
	inner join @xCalcSet.nodes('/CALC_SET/SET') AS T(c) on  c.value('(@ID)[1]', 'int') = cs.CalcSet and c.value('(@SUBID)[1]', 'int') = case idt.Indicator_Source
					when 1 then 1
					when 2 then 1
					when 3 then 2
					when 4 then 4
					else 0
				  end
	where d5.Inds_Indicator_ID = @IndusIndicatorId
	group by d5.Country_Id, d5.Industry_ID, idt.Indicator_Parent_ID, case idt.Indicator_Source
					when 1 then 1
					when 2 then 1
					when 3 then 2
					when 4 then 4
					else 0
				  end

) d3
inner join Tbl_Inds_Indicator_det d on d.Country_Id = d3.Country_Id and d.Industry_ID = d3.Industry_ID 
inner join Tbl_Indicator idt on d.Indicator_ID = idt.ID and idt.Indicator_Level = 5 and idt.Indicator_Parent_ID = d3.Indicator_Parent_ID and case idt.Indicator_Source
					when 1 then 1
					when 2 then 1
					when 3 then 2
					when 4 then 4
					else 0
				  end = d3.Indicator_Source
where d.Inds_Indicator_ID = @IndusIndicatorId



--Fill Up Weight Level 6
update d
set Indi_wg = d3.WG
from (
	select d4.Country_Id, d4.Industry_ID, idt.Indicator_Parent_ID, max(d3.indi_wg)/count(d3.indi_wg) as WG
	from Tbl_Inds_Indicator_det d4
	inner join Tbl_Indicator idt on d4.Indicator_ID = idt.ID and idt.Indicator_Level = 6
	inner join Tbl_Inds_Indicator_det d3 on d3.Inds_Indicator_ID = d4.Inds_Indicator_ID and d3.Country_Id = d4.Country_Id and d3.Industry_ID = d4.Industry_ID and d3.Indicator_ID = idt.Indicator_Parent_ID
	inner join Tbl_Indicator idx on d3.Indicator_ID = idx.ID and idx.Indicator_Level = 5
	where d4.Inds_Indicator_ID = @IndusIndicatorId
	group by d4.Country_Id, d4.Industry_ID, idt.Indicator_Parent_ID
--order by d.Country_Id, d.Industry_ID, d.Indicator_ID
) d3
inner join Tbl_Inds_Indicator_det d on d.Country_Id = d3.Country_Id and d.Industry_ID = d3.Industry_ID 
inner join Tbl_Indicator idt on d.Indicator_ID = idt.ID and idt.Indicator_Level = 6 and idt.Indicator_Parent_ID = d3.Indicator_Parent_ID
where d.Inds_Indicator_ID = @IndusIndicatorId



GO
