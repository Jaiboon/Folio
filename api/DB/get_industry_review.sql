DROP PROCEDURE [dbo].[get_industry_review]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =============================================
MoVarin 2018-06-28
   =============================================*/
CREATE PROCEDURE [dbo].[get_industry_review]
	-- Add the parameters for the stored procedure here
	@year int,
	@industryId int,
	@countryId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [IR].[ID],
		[IR].[Year],
		[I].[Industry_Name],
		[I].[Industry_Name_Thai],
		[C].[Country_Name],
		[C].[Country_Name_Thai],
		[IR].[Html_Information]
    FROM [Tbl_Industry_Review] AS [IR]
		INNER JOIN [Tbl_Industry] AS [I] ON [IR].[Industry_ID] = [I].[ID]
		INNER JOIN [Tbl_Country] AS [C] ON [IR].[Country_ID] = [C].[ID]
	WHERE [IR].[Year] = @year
		AND [IR].[Industry_ID] = @industryId
		AND [IR].[Country_ID] = @countryId
END
GO

exec [get_industry_review] @year='',@industryId='' ,@countryId=''

