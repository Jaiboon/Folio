DROP PROCEDURE stp_delete_assessment_data
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =============================================
Nipon 2018-07-03 
   =============================================*/
CREATE PROCEDURE stp_delete_assessment_data
	@AssessmentId INT
AS
BEGIN

	DELETE Tbl_Assessment_Data
	WHERE Assessment_ID = @AssessmentId

END

GO