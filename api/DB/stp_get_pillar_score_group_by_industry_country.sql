DROP PROCEDURE [dbo].[stp_get_pillar_score_group_by_industry_country]
GO
/* =============================================
OLE 2018-06-27 
   =============================================*/
CREATE PROCEDURE [dbo].[stp_get_pillar_score_group_by_industry_country]
	@year int,
	@pillar int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
		--select -1 BaseScore, null CountryName, null CompetitiveType, i.Industry_Name_Thai IndustryName
		--from [Tbl_Industry] i
		
		select Base_Score BaseScore, c.Country_Name_Thai CountryName, null CompetitiveType, i.ID Industry_ID, i.Industry_Name_Thai IndustryName,--+ '('+ c.Country_Name_Thai + ')' IndustryName
		i.id industryId, 0 countrySeq, 0 mainSeq
		from Tbl_Inds_Indicator_det iid
			inner join Tbl_Index dx on dx.Index_ID = iid.Index_Id
			inner join Tbl_Inds_Indicator ii on ii.ID = iid.Inds_Indicator_ID
			inner join [Tbl_Industry] i on i.ID = iid.Industry_ID
			inner join [Tbl_Country] c on c.ID = iid.Country_ID
		where ii.Use_Flag = 'Y' and Index_Level = 3	 and ii.Year = @year and iid.Index_Id = @pillar and iid.Country_ID = 14
			and iid.Indicator_ID is null
		union all
		select top 100 percent Base_Score BaseScore, c.Country_Name_Thai CountryName, ic.CompetitiveType, i.ID Industry_ID, i.Industry_Name_Thai IndustryName,
		i.id industryId, c.Id countrySeq, 1 mainSeq
		from Tbl_Inds_Indicator_det iid
			inner join Tbl_Index dx on dx.Index_ID = iid.Index_Id
			inner join Tbl_Inds_Indicator ii on ii.ID = iid.Inds_Indicator_ID
			inner join [Tbl_Industry_Country] ic on ic.Country_ID = iid.Country_Id and ic.Industry_ID = iid.Industry_ID
			inner join [Tbl_Country] c on c.ID = ic.Country_ID
			inner join [Tbl_Industry] i on i.ID = iid.Industry_ID
		where ii.Use_Flag = 'Y' and Index_Level = 3	 and ii.Year = @year and iid.Index_Id = @pillar
			and iid.Indicator_ID is null
		order by mainSeq, industryId, competitiveType desc, countrySeq
		
		--select *
		--from Tbl_Inds_Indicator_det iid
		--	inner join Tbl_Index dx on dx.Index_ID = iid.Index_Id
		--	inner join Tbl_Inds_Indicator ii on ii.ID = iid.Inds_Indicator_ID
		--	inner join [Tbl_Industry_Country] ic on ic.Country_ID = iid.Country_Id and ic.Industry_ID = iid.Industry_ID
		--	inner join [Tbl_Country] c on c.ID = ic.Country_ID
		--	inner join [Tbl_Industry] i on i.ID = iid.Industry_ID
		--where ii.Use_Flag = 'Y' and Index_Level = 3	 and ii.Year = @year

END

