DROP PROCEDURE stp_save_assessment_data_xml
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =============================================
Nipon 2018-07-03 
   =============================================*/
CREATE PROCEDURE stp_save_assessment_data_xml
	@XMLAssessmentData XML
AS
BEGIN

	INSERT INTO Tbl_Assessment_Data (Assessment_ID, Assessment_Base_ID, Score, Create_User, Create_Date, Modify_User, Modify_Date)
	SELECT
			Tbl.Col.value('AssessmentId[1]', 'int') AssessmentId,
			Tbl.Col.value('AssessmentBaseId[1]', 'int') AssessmentBaseId,
			Tbl.Col.value('Score[1]', 'int') Score,
			Tbl.Col.value('UserId[1]', 'int') Create_User,
			GETDATE() Create_Date,
			Tbl.Col.value('UserId[1]', 'int') Modify_User,
			GETDATE() Modify_Date
	FROM   @XMLAssessmentData.nodes('/ArrayOfAssessmentData/AssessmentData') Tbl(Col)  

END

GO