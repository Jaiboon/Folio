DROP PROCEDURE [dbo].[get_pillar_index_by_industry_and_country]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =============================================
OLE 2018-06-27 
   =============================================*/
CREATE PROCEDURE [dbo].[get_pillar_index_by_industry_and_country]
	@year int,
	@industry int,
	@country int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select Base_Score, Index_Name 
	from Tbl_Inds_Indicator_det iid
		inner join Tbl_Index dx on dx.Index_ID = iid.Index_Id
		inner join Tbl_Inds_Indicator ii on ii.ID = iid.Inds_Indicator_ID
	where ii.Use_Flag = 'Y' and ii.Year = @year
		and dx.Index_Level = 3
		and Country_Id = @country and Industry_ID = @industry
		and iid.Indicator_ID is null
	order by dx.Index_ID
END
GO

exec sp_executesql N'get_pillar_index_by_industry_and_country @year, @industry, @country',N'@year int,@industry int,@country int',@year=2018,@industry=1,@country=1

