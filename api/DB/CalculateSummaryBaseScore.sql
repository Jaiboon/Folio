IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateSummaryBaseScore]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CalculateSummaryBaseScore]
GO


CREATE PROCEDURE [dbo].[CalculateSummaryBaseScore]
	@IndusIndicatorId int,
	@UserId char(10)
AS

begin tran
BEGIN TRY
	--country_id = 98 for avg all country 
	insert Tbl_Inds_Indicator_det(Inds_Indicator_ID, Country_Id, Industry_ID, Index_Id, Indicator_ID, Indi_wg, Base_Score, Create_User, Create_Date, Modify_User, Modify_Date)
		select Inds_Indicator_ID, '98', Industry_ID, Index_ID, Indicator_ID, null, AVG(Base_Score), @UserId, getdate(), @UserId, getdate()
		from Tbl_Inds_Indicator_det
		where Inds_Indicator_ID = @IndusIndicatorId
			and Country_Id not in (98, 99)
		group by Inds_Indicator_ID, Industry_ID, Indicator_ID, Index_Id
		
	commit
END TRY
BEGIN CATCH
	rollback
	declare @ErrorMsg varchar(max), @ErrorSeverity int, @ErrorState int
	select @ErrorMsg = ERROR_MESSAGE()
		, @ErrorSeverity = ERROR_SEVERITY()
		, @ErrorState = ERROR_STATE()

	RAISERROR(@ErrorMsg, @ErrorSeverity, @ErrorState)
END CATCH

