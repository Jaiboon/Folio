IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BuildTHFactor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[BuildTHFactor]
GO


CREATE PROCEDURE [dbo].[BuildTHFactor]
	@IndusIndicatorId int,
	@UserId int
AS
begin tran
BEGIN TRY
	declare @Year int
	
	select @Year = [YEAR]
	from Tbl_Inds_Indicator
	where ID = @IndusIndicatorId	
	
	insert Tbl_ThFactor(Inds_Indicator_Id, year, Country_ID, Industry_ID, Indicator_ID, Indicator_Name, Score, DisplayFlag)
		select @IndusIndicatorId, @Year, rk.Country_ID, rk.Industry_ID, rk.Indicator_ID, i.Indicator_Name, rk.Score, 'S'
		from (
			select RANK() OVER(Partition By Industry_ID order by Score desc) Rank --Top Rank 5
				,Country_Id, Industry_ID, Indicator_ID, Score
			from (
				select Country_Id, Industry_ID, Indicator_ID, AVG(Score) Score
				from Tbl_PrimaryData
				where YEAR = @Year
					and Country_Id = 14
					and DropFlag = '0'
				group by Country_Id, Industry_ID, Indicator_ID
			) t
		) rk
		inner join Tbl_Indicator i on rk.Indicator_ID = i.ID
		where rk.Rank <= 5

		union all

		select @IndusIndicatorId, @Year, rk.Country_ID, rk.Industry_ID, rk.Indicator_ID, i.Indicator_Name, rk.Score, 'W'
		from (
			select RANK() OVER(Partition By Industry_ID order by Score) Rank --Less Rank 5
				,Country_Id, Industry_ID, Indicator_ID, Score
			from (
				select Country_Id, Industry_ID, Indicator_ID, AVG(Score) Score
				from Tbl_PrimaryData
				where YEAR = @Year
					and Country_Id = 14
					and DropFlag = '0'
				group by Country_Id, Industry_ID, Indicator_ID
			) t
		) rk
		inner join Tbl_Indicator i on rk.Indicator_ID = i.ID
		where rk.Rank <= 5
	commit
END TRY
BEGIN CATCH
	rollback
	declare @ErrorMsg varchar(max), @ErrorSeverity int, @ErrorState int
	select @ErrorMsg = ERROR_MESSAGE()
		, @ErrorSeverity = ERROR_SEVERITY()
		, @ErrorState = ERROR_STATE()

	RAISERROR(@ErrorMsg, @ErrorSeverity, @ErrorState)
END CATCH

