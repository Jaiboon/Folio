IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BuildAssBase]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[BuildAssBase]
GO


CREATE PROCEDURE [dbo].[BuildAssBase]
	@IndusIndicatorId int,
	@UserId int
AS
begin tran
BEGIN TRY
	declare @xAssMap xml = '
	  <ASSMAP>
		<ASS ID="1" INDICATOR="442" />
		<ASS ID="1" INDICATOR="443" />
		<ASS ID="2" INDICATOR="446" />
		<ASS ID="2" INDICATOR="450" />
		<ASS ID="2" INDICATOR="453" />
		<ASS ID="3" INDICATOR="455" />
		<ASS ID="3" INDICATOR="456" />
		<ASS ID="4" INDICATOR="444" />
		<ASS ID="5" INDICATOR="460" />
		<ASS ID="5" INDICATOR="461" />
		<ASS ID="6" INDICATOR="463" />
		<ASS ID="7" INDICATOR="468" />
		<ASS ID="8" INDICATOR="470" />
		<ASS ID="9" INDICATOR="447" />
		<ASS ID="10" INDICATOR="474" />
		<ASS ID="11" INDICATOR="478" />
		<ASS ID="12" INDICATOR="479" />
		<ASS ID="13" INDICATOR="486" />
		<ASS ID="13" INDICATOR="487" />
		<ASS ID="14" INDICATOR="489" />
		<ASS ID="15" INDICATOR="490" />
		<ASS ID="16" INDICATOR="491" />
	  </ASSMAP>
	'


	declare @Year int
	select @Year = [YEAR]
	from Tbl_Inds_Indicator
	where ID = @IndusIndicatorId	
	
	insert Tbl_Assessment_Base(Inds_Indicator_ID, Year, Industry_ID, Ass_No, Th_Score, Create_User, Create_Date, Modify_User, Modify_Date)
		select @IndusIndicatorId, @Year, Industry_ID, assmap.ASSNO, AVG(Score), @UserId, getdate(), @UserId, getdate()
		from Tbl_PrimaryData p
			inner join (
					select c.value('(@ID)[1]', 'int') ASSNO,  c.value('(@INDICATOR)[1]', 'int') IND
					from @xAssMap.nodes('/ASSMAP/ASS') AS T(c) 
				) assmap on p.Indicator_ID = assmap.IND
		where YEAR = @Year
			and Country_Id = 14
			and DropFlag = '0'
		group by Industry_ID, assmap.ASSNO

	commit
END TRY
BEGIN CATCH
	rollback
	declare @ErrorMsg varchar(max), @ErrorSeverity int, @ErrorState int
	select @ErrorMsg = ERROR_MESSAGE()
		, @ErrorSeverity = ERROR_SEVERITY()
		, @ErrorState = ERROR_STATE()

	RAISERROR(@ErrorMsg, @ErrorSeverity, @ErrorState)
END CATCH

