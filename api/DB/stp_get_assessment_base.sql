DROP PROCEDURE [dbo].[stp_get_assessment_base]
GO

/****** Object:  StoredProcedure [dbo].[stp_get_assessment_base]    Script Date: 28/6/2561 11:23:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================

-- =============================================
CREATE PROCEDURE [dbo].[stp_get_assessment_base]
	@industry int,
	@year int
AS
BEGIN		
	SELECT AB.Id, AB.Year, AB.Industry_ID, AB.Ass_No, AB.Th_Score
	FROM Tbl_Assessment_Base AB
	INNER JOIN Tbl_Inds_Indicator II ON II.ID = AB.Inds_Indicator_ID
	WHERE II.Use_Flag = 'Y' AND II.Year = @year AND Industry_ID = @industry
	ORDER BY Ass_No
END

GO

exec sp_executesql N'stp_get_assessment_base @industry, @year ',N'@industry int,@year int',@industry=4,@year=2018