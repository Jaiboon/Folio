DROP PROCEDURE stp_get_industry_indicator
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =============================================
Nipon 2018-07-02 
   =============================================*/
CREATE PROCEDURE stp_get_industry_indicator
	-- Add the parameters for the stored procedure here
	@year int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT ID Id ,
	Year Year ,
	isnull(Version,0) Version,
	isnull(COUNT_PRIMARY,0)  CountSurvey,
	isnull(COUNT_SECONDARY,0) CountSecondary,
	isnull(AVG_INDEX,0) AvgIndex ,
	case Use_Flag when 'Y' then 1 else 0 end Use_Flag 
	FROM Tbl_Inds_Indicator
	WHERE year=@year

END
GO