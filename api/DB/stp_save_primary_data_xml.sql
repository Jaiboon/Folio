DROP PROCEDURE stp_save_primary_data_xml
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =============================================
Nipon 2018-07-03 
   =============================================*/
CREATE PROCEDURE stp_save_primary_data_xml
	@XMLPrimaryData XML
AS
BEGIN

	INSERT INTO Tbl_PrimaryData (PrimaryH_ID ,Year ,Industry_ID ,Country_ID ,Company_ID ,Indicator_ID ,Score ,Remark ,ImportFlag ,DropFlag, Create_User, Create_Date, Modify_User, Modify_Date)
	SELECT
			Tbl.Col.value('PrimaryHId[1]', 'int') PrimaryHId,
			Tbl.Col.value('Year[1]', 'int') Year,
			Tbl.Col.value('IndustryId[1]', 'int') IndustryId,
			Tbl.Col.value('CountryId[1]', 'int') CountryId,
			Tbl.Col.value('CompanyId[1]', 'int') CompanyId,
			Tbl.Col.value('IndicatorId[1]', 'int') IndicatorId,
			Tbl.Col.value('Score[1]', 'decimal(9,2)') Score,
			Tbl.Col.value('Remark[1]', 'nvarchar(255)') Remarke,
			Tbl.Col.value('ImportFlag[1]', 'nvarchar(1)') ImportFlage,
			Tbl.Col.value('DropFlag[1]', 'nchar(1)') DropFlage,
			Tbl.Col.value('UserId[1]', 'int') Create_User,
			GETDATE() Create_Date,
			Tbl.Col.value('UserId[1]', 'int') Modify_User,
			GETDATE() Modify_Date
	FROM   @XMLPrimaryData.nodes('/ArrayOfPrimaryData/PrimaryData') Tbl(Col)  

END

GO