DROP PROCEDURE [dbo].[stp_get_trans_indi_graph]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =============================================
OLE 2018-06-27 
   =============================================*/
CREATE PROCEDURE [dbo].[stp_get_trans_indi_graph]
	@industry int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT Industry_ID
	, I.ID
	, I.Indicator_Name
      , [GraphPos]
      ,[Year]
      ,ISNULL([Score], -1) Score
	FROM [OIE_DB].[dbo].[V_TRANS_INDI_GRAPH] IG
	inner join Tbl_Indicator I ON I.ID = IG.Indicator_ID
	WHERE [GraphPos] IS NOT NULL and Industry_ID = @Industry
	ORDER BY I.ID, [Year]
END
GO


exec sp_executesql N'stp_get_trans_indi_graph @industry ',N'@industry int',@industry=5

