DROP PROCEDURE [dbo].[get_industry_review_list]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =============================================
MoVarin 2018-06-28
   =============================================*/
CREATE PROCEDURE [dbo].[get_industry_review_list]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [IR].[ID],
		[IR].[Year],
		[I].[Industry_Name],
		[I].[Industry_Name_Thai],
		[C].[Country_Name],
		[C].[Country_Name_Thai]
    FROM [Tbl_Industry_Review] AS [IR]
		INNER JOIN [Tbl_Industry] AS [I] ON [IR].[Industry_ID] = [I].[ID]
		INNER JOIN [Tbl_Country] AS [C] ON [IR].[Country_ID] = [C].[ID]
END
GO

exec [get_industry_review_list]