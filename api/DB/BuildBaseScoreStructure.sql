IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BuildBaseScoreStructure]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[BuildBaseScoreStructure]
GO


CREATE PROCEDURE [dbo].[BuildBaseScoreStructure]
	@IndusIndicatorId int,
	@Year int,
	@UserId int,
	@Level int
AS

--�ӹǳ��Ƿ���դ�ҡ�͹
IF(@Level in (5, 6))
  BEGIN
	update d
	set Base_Score = src.Recale_Score -- * d.Indi_wg
	from Tbl_SecondaryData src
		inner join Tbl_Indicator i on src.Indicator_ID = i.ID
		inner join Tbl_Inds_Indicator_det d on src.Country_ID = d.Country_Id and src.Industry_ID = d.Industry_ID and src.Indicator_ID = d.Indicator_ID
	where src.[YEAR] = @Year
		and d.Inds_Indicator_ID = @IndusIndicatorId
		and i.Indicator_Level = @Level
  END

--Pass 2
IF(@Level between 4 and 5)
  BEGIN
	update d
	set Base_Score = Score / Indi_wg
	from (
		select d.ID, sum(d2.Indi_wg * d2.Base_Score) Score
		from Tbl_Inds_Indicator_det d
			inner join Tbl_Indicator di on d.Indicator_ID = di.ID
			inner join Tbl_Inds_Indicator_det d2 on d.Inds_Indicator_ID = d2.Inds_Indicator_ID and d.Country_ID = d2.Country_Id and d.Industry_ID = d2.Industry_ID --and d.Indicator_ID = d2.Indicator_ID
			inner join Tbl_Indicator di2 on d2.Indicator_ID = di2.ID and d.Indicator_ID = di2.Indicator_Parent_ID
		where d.Inds_Indicator_ID = @IndusIndicatorId
			and di.Indicator_Level = @Level
			and di2.Indicator_Level = @Level + 1
		group by d.ID
	) sc
	inner join Tbl_Inds_Indicator_det d on sc.ID = d.ID 
  END
ELSE IF(@Level = 3)
  BEGIN
	update d
	set Base_Score = Score / Indi_wg
	from (
		select d.ID, sum(d2.Indi_wg * d2.Base_Score) Score
		from Tbl_Inds_Indicator_det d
			inner join Tbl_Index di on d.Index_ID = di.Index_ID 
			inner join Tbl_Inds_Indicator_det d2 on d.Inds_Indicator_ID = d2.Inds_Indicator_ID and d.Country_ID = d2.Country_Id and d.Industry_ID = d2.Industry_ID --and d.Indicator_ID = d2.Indicator_ID
			inner join Tbl_Indicator di2 on d2.Indicator_ID = di2.ID and d.Index_ID = di2.Index_ID
		where d.Inds_Indicator_ID = @IndusIndicatorId
			and d.Indicator_ID is null
			and di.Index_Level = @Level
			and di2.Indicator_Level = @Level + 1
		group by d.ID
	) sc
	inner join Tbl_Inds_Indicator_det d on sc.ID = d.ID 
  END
ELSE IF(@Level between 1 and 3)
  BEGIN
	update d
	set Base_Score = Score / Indi_wg
	from (
		select d.ID, sum(d2.Indi_wg * d2.Base_Score) Score
		from Tbl_Inds_Indicator_det d
			inner join Tbl_Index di on d.Index_ID = di.Index_ID
			inner join Tbl_Inds_Indicator_det d2 on d.Inds_Indicator_ID = d2.Inds_Indicator_ID and d.Country_ID = d2.Country_Id and d.Industry_ID = d2.Industry_ID --and d.Indicator_ID = d2.Indicator_ID
			inner join Tbl_Index idx2 on d2.Index_ID = idx2.Index_ID and d.Index_ID = idx2.Index_Parent_ID
		where d.Inds_Indicator_ID = @IndusIndicatorId
			and d.Indicator_ID is null
			and d2.Indicator_ID is null
			and di.Index_Level = @Level
			and idx2.Index_Level = @Level + 1
		group by d.ID
	) sc
	inner join Tbl_Inds_Indicator_det d on sc.ID = d.ID   
  END

IF(@Level > 1)
  BEGIN
	set @Level = @Level - 1
	exec BuildBaseScoreStructure @IndusIndicatorId,	@Year, @UserId,	@Level	
  END

GO