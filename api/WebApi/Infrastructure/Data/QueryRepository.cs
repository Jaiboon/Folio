﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.ViewModels;
using System.Collections.Generic;
using Dapper;
using System.Data.SqlClient;
using ApplicationCore;
using ApplicationCore.Dtos;
using System;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
    public class QueryRepository : EfRepository<Document>, IQueryRepository
    {
        int CommandTimeout = 300;
        public QueryRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }


        //         public async Task<ChartDataModel> GetSelfAssessmentReport(int userId, int year)
        //         {
        //             //  using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
        //             // {
        //             //     return Dapper.SqlMapper.Query<TestSPModel>(dbConnection, "stp_assessment_report @username @year", new { parm_test = 1 }, commandTimeout: this.CommandTimeout).ToList();
        //             // }

        //             var thailand = new DataModel()
        //             {
        //                 Data = new double[] { 7.48, 5.15, 4.94, 0.22, 3.74, 3.07, 6.67, 0.42, 8.40, 0.94, 2.43, 9.93, 9.50, 0.96, 9.37, 1 },
        //                 Label = "ค่าเฉลี่ย",
        //             };
        //             var indonesia = new DataModel()
        //             {
        //                 Data = new double[] { 9.81, 5.44, 4.08, 6.66, 4.03, 0.89, 0.86, 0.04, 5.33, 2.97, 7.38, 4.16, 0.97, 1.47, 9.10, 1 },
        //                 Label = "ผู้ประกอบการประเมินตนเอง",
        //             };

        //             return new ChartDataModel()
        //             {
        //                 Data = new DataModel[] { thailand, indonesia },
        //                 Labels = new string[]
        //                 {
        // "สภาพแวดล้อมทางสังคมและเศรษฐกิจมหภาค (ในและต่างประเทศ)",
        // "ประสิทธิภาพและการดำเนินนโยบายภาครัฐ",
        // "ระบบขนส่งและโครงสร้างพื้นฐาน",
        // "ระบบการศึกษา",
        // "ปัจจัยแรงงาน: ปริมาณและคุณภาพ",
        // "ศักยภาพของเครื่องจักร",
        // "อำนาจต่อรองกับผู้ขาย",
        // "การลงทุนด้านวิจัยและพัฒนา",
        // "กลยุทธ์ในภาพรวม",
        // "ผลิตภาพการผลิต",
        // "การผลิตที่เป็นมิตรต่อสิ่งแวดล้อม",
        // "การบริหารจัดการองค์กร",
        // "ปริมาณการจำหน่าย (ในและต่างประเทศ)",
        // "การทำกำไร",
        // "อำนาจต่อรองกับลูกค้า ",
        // "แนวโน้มในอนาคต"
        //                 }
        //             };
        //         }
        //public Page GetByIdWithItems(int id)
        //{
        //    return null;
        //    //return _dbContext.Pages
        //    //    //.Include(o => o.OrderItems)
        //    //    //.Include("OrderItems.ItemOrdered")
        //    //    .FirstOrDefault();
        //}

        public async Task<IList<AssessmentScoreModel>> GetSelfAssessmentReport(int userId, int year)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<AssessmentScoreModel>(dbConnection, "stp_assessment_report @userId, @year",
                    new { userId = userId, year = year }, commandTimeout: this.CommandTimeout).ToList();
            }
        } 
        public async Task<IList<IndexScoreModel>> GetRadarReportAsync(int year, int industry, int country)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<IndexScoreModel>(dbConnection, "get_pillar_index_by_industry_and_country @year, @industry, @country",
                    new { year = year, industry = industry, country = country }, commandTimeout: this.CommandTimeout).ToList();
            }
        }

        public async Task<IList<CountryCompareIndicatorScoreModel>> GetCountryCompareIndicatorScore(int year, int industry, int country)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<CountryCompareIndicatorScoreModel>(dbConnection, "get_country_compare_indicator_score @year, @industry, @country",
                    new { year = year, industry = industry, country = country }, commandTimeout: this.CommandTimeout).ToList();
            }
        }

        public async Task<IList<IndexScoreModel>> GetFactorsDataAsync(int year, int industry, int country)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<IndexScoreModel>(dbConnection, "get_factors_data @year, @industry, @country",
                    new { year = year, industry = industry, country = country }, commandTimeout: this.CommandTimeout).ToList();
            }
        }

        public async Task<IList<IndustryReviewModel>> GetListIndustryReview()
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<IndustryReviewModel>(dbConnection, "get_industry_review_list", commandTimeout: this.CommandTimeout).ToList();
            }
        }

        public async Task<IndustryReviewModel> GetIndustryReview(int year, int industryId, int countryId)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<IndustryReviewModel>(dbConnection, "get_industry_review @year, @industryId, @countryId",
                    new { year = year, industryId = industryId, countryId = countryId }).FirstOrDefault();
            }
        }
        //Export Master
        public async Task<IList<V_M_Industry>> Get_V_M_Industry()
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
              return Dapper.SqlMapper.Query<V_M_Industry>(dbConnection,"stp_get_exportdata @VIEWNAME" ,new { viewname="V_M_Industry" }, commandTimeout: this.CommandTimeout).ToList(); 

            }
        }

        public async Task<IList<V_M_Country>> Get_V_M_Country()
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                 return Dapper.SqlMapper.Query<V_M_Country>(dbConnection,"stp_get_exportdata @VIEWNAME" ,new { viewname="V_M_Country" }, commandTimeout: this.CommandTimeout).ToList(); 

            }
        }

        public async Task<IList<V_M_Company>> Get_V_M_Company()
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<V_M_Company>(dbConnection,"stp_get_exportdata @VIEWNAME" ,new { viewname="V_M_Company" }, commandTimeout: this.CommandTimeout).ToList(); 
            }
        }

        public async Task<IList<V_M_Index>> Get_V_M_Index()
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                  return Dapper.SqlMapper.Query<V_M_Index>(dbConnection,"stp_get_exportdata @VIEWNAME" ,new { viewname="V_M_Index" }, commandTimeout: this.CommandTimeout).ToList(); 
            }
        }

        public async Task<IList<V_M_Indicator>> Get_V_M_Indicator()
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                 return Dapper.SqlMapper.Query<V_M_Indicator>(dbConnection,"stp_get_exportdata @VIEWNAME" ,new { viewname="V_M_Indicator" }, commandTimeout: this.CommandTimeout).ToList();             
            }
        }
        public async Task<IList<V_M_COMPETITIVENESS_INDEX>> Get_V_M_COMPETITIVENESS_INDEX()
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<V_M_COMPETITIVENESS_INDEX>(dbConnection,"stp_get_exportdata @VIEWNAME" ,new { viewname="V_M_COMPETITIVENESS_INDEX" }, commandTimeout: this.CommandTimeout).ToList();             
               
            }
        }


        public async Task<IList<PillarScoreByIndustryAndCountry>> GetPillarScoreGroupByIndustryAndCountry(int year, int pillar)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<PillarScoreByIndustryAndCountry>(dbConnection, "stp_get_pillar_score_group_by_industry_country @year, @pillar",
                    new { year = year, pillar = pillar }, commandTimeout: this.CommandTimeout).ToList();
            }
        }
        //Export Data
        public async Task<IList<V_D_SURVEY>> Get_V_D_SURVEY(int year)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
               return Dapper.SqlMapper.Query<V_D_SURVEY>(dbConnection,"stp_get_exportdata @VIEWNAME, @YEAR" ,new { viewname="V_D_SURVEY",year = year }, commandTimeout: this.CommandTimeout).ToList(); 
            }
        }
        public async Task<IList<V_D_PRIMARY>> Get_V_D_PRIMARY(int year)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<V_D_PRIMARY>(dbConnection,"stp_get_exportdata @VIEWNAME, @YEAR" ,new { viewname="V_D_PRIMARY",year = year }, commandTimeout: this.CommandTimeout).ToList(); 
            }
        }
        public async Task<IList<V_D_SECONDARY>> Get_V_D_SECONDARY(int year)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<V_D_SECONDARY>(dbConnection,"stp_get_exportdata @VIEWNAME, @YEAR" ,new { viewname="V_D_SECONDARY",year = year }, commandTimeout: this.CommandTimeout).ToList(); 
            }
        }
        public async Task<IList<V_D_ASSESSEMENT>> Get_V_D_ASSESSEMENT(int year)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
             return Dapper.SqlMapper.Query<V_D_ASSESSEMENT>(dbConnection,"stp_get_exportdata @VIEWNAME, @YEAR" ,new { viewname="V_D_ASSESSEMENT",year = year }, commandTimeout: this.CommandTimeout).ToList(); 
            }
        }
          public async Task<IList<V_D_SURVEY_RECOMEND>> Get_V_D_SURVEY_RECOMEND(int year)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                 return Dapper.SqlMapper.Query<V_D_SURVEY_RECOMEND>(dbConnection,"stp_get_exportdata @VIEWNAME, @YEAR" ,new { viewname="V_D_SURVEY_RECOMEND",year = year }, commandTimeout: this.CommandTimeout).ToList(); 
            }
        }
          public async Task<IList<V_D_SECONDARY_THONLY>> Get_V_D_SECONDARY_THONLY()
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                 return Dapper.SqlMapper.Query<V_D_SECONDARY_THONLY>(dbConnection,"stp_get_exportdata @VIEWNAME" ,new { viewname="V_D_SECONDARY_THONLY"}, commandTimeout: this.CommandTimeout).ToList(); 

            }
        }
               public async Task<IList<V_D_PMI_PART1>> Get_V_D_PMI_PART1(int year)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<V_D_PMI_PART1>(dbConnection,"stp_get_exportdata @VIEWNAME, @YEAR" ,new { viewname="V_D_PMI_PART1",year = year }, commandTimeout: this.CommandTimeout).ToList(); 
            }
        }
          public async Task<IList<V_D_PMI_PART2>> Get_V_D_PMI_PART2(int year)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<V_D_PMI_PART2>(dbConnection,"stp_get_exportdata @VIEWNAME, @YEAR" ,new { viewname="V_D_PMI_PART2",year = year }, commandTimeout: this.CommandTimeout).ToList(); 
            }
        }
        //Export  Result
        public async Task<IList<V_R_INDUSTRY_INDICATOR>> Get_V_R_INDUSTRY_INDICATOR(int year, int version)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<V_R_INDUSTRY_INDICATOR>(dbConnection,"stp_get_exportdata @VIEWNAME, @YEAR, @VERSION" ,new { viewname="V_R_INDUSTRY_INDICATOR",year = year, version = version }, commandTimeout: this.CommandTimeout).ToList(); 
            }
        }
        public async Task<IList<V_R_ASSESSMENT_BASE>> Get_V_R_ASSESSMENT_BASE(int year, int version)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<V_R_ASSESSMENT_BASE>(dbConnection,"stp_get_exportdata @VIEWNAME, @YEAR, @VERSION" ,new { viewname="V_R_ASSESSMENT_BASE",year = year, version = version }, commandTimeout: this.CommandTimeout).ToList(); 
            }
        }
   
        public async Task<IList<V_R_TH_5SW>> Get_V_R_TH_5SW(int year, int version)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                  return Dapper.SqlMapper.Query<V_R_TH_5SW>(dbConnection,"stp_get_exportdata @VIEWNAME, @YEAR, @VERSION" ,new { viewname="V_R_TH_5SW",year = year, version = version }, commandTimeout: this.CommandTimeout).ToList(); 
            }
        }
        public async Task<IList<V_R_COMPETITIVENESS_INDEX>> Get_V_R_COMPETITIVENESS_INDEX(int year, int version)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<V_R_COMPETITIVENESS_INDEX>(dbConnection,"stp_get_exportdata @VIEWNAME, @YEAR, @VERSION" ,new { viewname="V_R_COMPETITIVENESS_INDEX",year = year, version = version }, commandTimeout: this.CommandTimeout).ToList(); 
            }
        }

        public async Task<IList<TransIndiGraphModel>> GetTransIndicatorGraph(int industry)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<TransIndiGraphModel>(dbConnection, "stp_get_trans_indi_graph @industry ",
                    new { industry = industry }, commandTimeout: this.CommandTimeout).ToList();
            }
        }

        public async Task<IList<ListViewModel>> GetPillarIndex()
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<ListViewModel>(dbConnection, "select Index_ID ID, Index_Name Name, Index_Name NameThai  from Tbl_Index where Index_Level = 3 order by Hierarchy_Code", commandTimeout: this.CommandTimeout).ToList();
            }
        }


        public async Task<IList<ListViewModel>> GetIndustries()
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<ListViewModel>(dbConnection, "select ID, Industry_Name Name, Industry_Name_Thai NameThai from Tbl_Industry order by case when ID = 99 then 0 else ID end", commandTimeout: this.CommandTimeout).ToList();
            }
        }

        public async Task<IList<ListViewModel>> GetCountries()
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<ListViewModel>(dbConnection, "select ID, Country_Name Name, Country_Name_Thai NameThai from Tbl_Country", commandTimeout: this.CommandTimeout).ToList();
            }
        }

        public async Task<IList<IndustryIndicatorModel>> GetIndustryIndicatorList()
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<IndustryIndicatorModel>(dbConnection, @"select Year, Version from Tbl_Inds_Indicator order by Year, Version", commandTimeout: this.CommandTimeout).ToList();
            }
        }


        public async Task<IList<IndustryCountryListViewModel>> GetIndustryCountryList()
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<IndustryCountryListViewModel>(dbConnection, @"select Industry_ID, CompetitiveType, Industry_Name , Country_ID, Country_Name, Country_Name_Thai from Tbl_Industry_Country ic 
inner join Tbl_Country c on c.ID = ic.Country_ID
inner join Tbl_Industry i on i.ID = ic.Industry_ID
", commandTimeout: this.CommandTimeout).ToList();
            }
        }

        public async Task<IList<AssessmentBase>> GetAssessmentBase(int industry, int year)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<AssessmentBase>(dbConnection, "stp_get_assessment_base @industry, @year ",
                    new { industry = industry, year = year }, commandTimeout: this.CommandTimeout).ToList();
            }
        }

        public async Task<IList<IndustryIndicatorModel>> GetIndustryIndicatorModelList(int year)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<IndustryIndicatorModel>(dbConnection, "stp_get_industry_indicator @year", new { year = year }, commandTimeout: this.CommandTimeout).ToList();
            }
        }

        public async Task<IndustryIndicatorDataCountModel> GetIndustryIndicatorDataCount(int year)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<IndustryIndicatorDataCountModel>(dbConnection, "Get_For_NewVersion @year",
                    new { year = year }, commandTimeout: this.CommandTimeout).FirstOrDefault();
            }
        }

        public async Task<IndustryIndicatorModel> GetIndustryIndicatorCalculate(int year,string userId)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<IndustryIndicatorModel>(dbConnection, "BuildWeightTable @year, @UserId",
                    new { year = year, userid = userId },commandTimeout: this.CommandTimeout).FirstOrDefault();
            }
        }

        public async Task<IndustryIndicatorModel> IndustryIndicatorUseSave(int year, int industry_Id)
        {
            using (IDbConnection dbConnection = new SqlConnection(ServerUtils.ConfigInfo.ConnectionString))
            {
                return Dapper.SqlMapper.Query<IndustryIndicatorModel>(dbConnection, "stp_IndustryIndicatorUseSave @year, @industry_Id",
                    new { year = year, industry_Id = industry_Id },commandTimeout: this.CommandTimeout).FirstOrDefault();
            }
        }

        public async Task SaveXMLPrimaryData(string XMLPrimaryData)
        {
            SqlParameter[] arParams = new SqlParameter[1];
            arParams[0] = new SqlParameter("@XMLPrimaryData", XMLPrimaryData);

            _dbContext.Database.ExecuteSqlCommand("stp_save_primary_data_xml @XMLPrimaryData", arParams);
        }

        public async Task DeletePrimaryData(int PrimaryH_ID)
        {
            SqlParameter[] arParams = new SqlParameter[1];
            arParams[0] = new SqlParameter("@PrimaryH_ID", PrimaryH_ID);

            _dbContext.Database.ExecuteSqlCommand("stp_delete_primary_data @PrimaryH_ID", arParams);
        }

        public async Task SaveXMLAssessmentData(string XMLAssessmentData)
        {
            SqlParameter[] arParams = new SqlParameter[1];
            arParams[0] = new SqlParameter("@XMLAssessmentData", XMLAssessmentData);

            _dbContext.Database.ExecuteSqlCommand("stp_save_assessment_data_xml @XMLAssessmentData", arParams);
        }

        public async Task DeleteAssessmentData(int AssessmentId)
        {
            SqlParameter[] arParams = new SqlParameter[1];
            arParams[0] = new SqlParameter("@AssessmentId", AssessmentId);

            _dbContext.Database.ExecuteSqlCommand("stp_delete_assessment_data @AssessmentId", arParams);
        }


    }
}
