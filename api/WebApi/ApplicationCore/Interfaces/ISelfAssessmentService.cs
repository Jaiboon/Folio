﻿using ApplicationCore.Entities;
using ApplicationCore.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface ISelfAssessmentService
    {
        Task SaveAsync(SelfAssessmentModel category);
        //Task<IList<AssessmentBase>> GetAssessmentBaseByFilter(AssessmentBaseFilterModel filter);
        Task<SelfAssessmentModel> GetSelfAssessmentByUserIdAndYear(int UserId, int Year);
    }
}
