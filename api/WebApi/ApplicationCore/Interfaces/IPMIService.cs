using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.ViewModels;

namespace ApplicationCore.Interfaces
{
    public interface IPMIService
    {
        Task SavePMI(PMIModel pmi);
      
    }
}