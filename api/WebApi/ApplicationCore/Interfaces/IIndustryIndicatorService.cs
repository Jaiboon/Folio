using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.ViewModels;

namespace ApplicationCore.Interfaces
{
    public interface IIndustryIndicatorService
    {
        Task<IList<IndustryIndicatorModel>> GetIndustryIndicator(int year);
        Task<IndustryIndicatorDataCountModel> GetIndustryIndicatorDataCount(int year);
        Task IndustryIndicatorCalculate(int year ,string userid);
        Task IndustryIndicatorUseSave(int year ,int industryId);
    }
}