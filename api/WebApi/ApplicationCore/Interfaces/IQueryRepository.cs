﻿using ApplicationCore.Dtos;
using ApplicationCore.Entities;
using ApplicationCore.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{

    public interface IQueryRepository : IRepository<Document>, IAsyncRepository<Document>
    {
        Task<IList<IndexScoreModel>> GetRadarReportAsync(int year, int industry, int country);
        Task<IList<AssessmentScoreModel>> GetSelfAssessmentReport(int userId, int year);
        Task<IList<CountryCompareIndicatorScoreModel>> GetCountryCompareIndicatorScore(int year, int industry, int country);
        Task<IList<PillarScoreByIndustryAndCountry>> GetPillarScoreGroupByIndustryAndCountry(int year, int pillar);

        Task<IList<IndexScoreModel>> GetFactorsDataAsync(int year, int industry, int country);
        Task<IList<IndustryReviewModel>> GetListIndustryReview();
        Task<IList<IndustryIndicatorModel>> GetIndustryIndicatorList();
        Task<IndustryReviewModel> GetIndustryReview(int year, int industryId, int countryId);
        //Master
        Task<IList<V_M_Industry>> Get_V_M_Industry();
        Task<IList<V_M_Country>> Get_V_M_Country();
        Task<IList<V_M_Company>> Get_V_M_Company();
        Task<IList<V_M_Index>> Get_V_M_Index();
        Task<IList<V_M_Indicator>> Get_V_M_Indicator();
        Task<IList<V_M_COMPETITIVENESS_INDEX>> Get_V_M_COMPETITIVENESS_INDEX();
        //Data  
        Task<IList<V_D_SURVEY>> Get_V_D_SURVEY(int year);
        Task<IList<V_D_PRIMARY>> Get_V_D_PRIMARY(int year);
        Task<IList<V_D_SECONDARY>> Get_V_D_SECONDARY(int year);
        Task<IList<V_D_ASSESSEMENT>> Get_V_D_ASSESSEMENT(int year);
        Task<IList<V_D_SURVEY_RECOMEND>> Get_V_D_SURVEY_RECOMEND(int year);
        Task<IList<V_D_SECONDARY_THONLY>> Get_V_D_SECONDARY_THONLY();
        Task<IList<V_D_PMI_PART1>> Get_V_D_PMI_PART1(int year);
        Task<IList<V_D_PMI_PART2>> Get_V_D_PMI_PART2(int year);
        //Result
        Task<IList<V_R_INDUSTRY_INDICATOR>> Get_V_R_INDUSTRY_INDICATOR(int year, int version);
        Task<IList<V_R_ASSESSMENT_BASE>> Get_V_R_ASSESSMENT_BASE(int year, int version);
        Task<IList<V_R_TH_5SW>> Get_V_R_TH_5SW(int year, int version);
        Task<IList<V_R_COMPETITIVENESS_INDEX>> Get_V_R_COMPETITIVENESS_INDEX(int year, int version);

        Task<IList<TransIndiGraphModel>> GetTransIndicatorGraph(int industry);
        Task<IList<ListViewModel>> GetPillarIndex();
        Task<IList<ListViewModel>> GetCountries();
        Task<IList<ListViewModel>> GetIndustries();
        Task<IList<IndustryCountryListViewModel>> GetIndustryCountryList();
        Task<IList<IndustryIndicatorModel>> GetIndustryIndicatorModelList(int year);
        Task<IndustryIndicatorDataCountModel> GetIndustryIndicatorDataCount(int year);
        Task<IndustryIndicatorModel> GetIndustryIndicatorCalculate(int year, string userId);
        Task<IndustryIndicatorModel> IndustryIndicatorUseSave(int year, int industry_Id);
        Task<IList<AssessmentBase>> GetAssessmentBase(int industry, int year);
        Task SaveXMLPrimaryData(string XMLPrimaryData);
        Task DeletePrimaryData(int PrimaryHId);
        Task SaveXMLAssessmentData(string XMLAssessmentData);
        Task DeleteAssessmentData(int AssessmentId);

    }
}