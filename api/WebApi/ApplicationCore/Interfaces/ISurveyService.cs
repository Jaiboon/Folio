﻿using ApplicationCore.Entities;
using ApplicationCore.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface ISurveyService
    {
        Task SaveAsync(SurveyViewModel surveyModel);
    }
}
