﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ApplicationCore.Entities
{
    [Table("skill")]
    public class Skill : BaseEntity
    {
        public int Score { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}