﻿using ApplicationCore.Interfaces;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using System.Collections.Generic;
using Ardalis.GuardClauses;
using ApplicationCore.Specifications;
using Microsoft.AspNetCore.Mvc;
using ApplicationCore.ViewModels;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ApplicationCore.Exceptions;
using System.Data;
using System.Data.SqlClient;

namespace ApplicationCore.Services
{
    public class SurveyService : ISurveyService
    {
        private readonly IAppLogger<SurveyService> _logger;

        private readonly IAsyncRepository<Company> _companyRepository;
        private readonly IAsyncRepository<PrimaryData> _primaryDataRepository;
        private readonly IAsyncRepository<PrimaryDataHead> _primaryDataHeadRepository;
        private readonly IQueryRepository _queryRepository;

        private IUnitOfWork _uow;

        public SurveyService(
            IUnitOfWork uow,
            IAsyncRepository<Company> companyRepository,
            IAsyncRepository<PrimaryData> primaryDataRepository,
            IAsyncRepository<PrimaryDataHead> primaryDataHeadRepository,
            IQueryRepository queryRepository,
            IAppLogger<SurveyService> logger)
        {
            _uow = uow;
            _companyRepository = companyRepository;
            _primaryDataRepository = primaryDataRepository;
            _primaryDataHeadRepository = primaryDataHeadRepository;
            _queryRepository = queryRepository;
            _logger = logger;
        }

        public async Task SaveAsync(SurveyViewModel surveyModel)
        {
            if(surveyModel.UserId==0)
            throw new DataNotFoundException("User not found");

            try
            {
                _uow.BeginTransaction();

                //###################### Delete Data ######################
                //---------- Search PrimaryDataHead ----------                
                IList<PrimaryDataHead> primaryDataHeadList = await _primaryDataHeadRepository.ListAsync(new PrimaryDataHeadFilterSpecification(surveyModel.primaryDataHead.Year, surveyModel.UserId));
                PrimaryDataHead primaryDataHead = primaryDataHeadList.FirstOrDefault();
                if (primaryDataHead != null)
                {
                    //---------- Delete PrimaryData ----------
                    await _queryRepository.DeletePrimaryData(primaryDataHead.Id);

                    //---------- Delete PrimaryDataHead ----------
                    await _primaryDataHeadRepository.DeleteAsync(primaryDataHead);

                    //---------- Delete Compnay ----------
                    Company company = await _companyRepository.GetByIdAsync(primaryDataHead.CompanyId);
                    await _companyRepository.DeleteAsync(company);
                }



                //###################### Insert Data ######################

                //---------- Insert Compnay ----------
                surveyModel.company.ModifyUser = surveyModel.UserId;
                surveyModel.company.CreateUser = surveyModel.UserId;

                await _companyRepository.AddAsync(surveyModel.company);

                //---------- Insert PrimaryDataHead ----------
                surveyModel.primaryDataHead.ImportFlag = "I";//
                surveyModel.primaryDataHead.CompanyId = surveyModel.company.Id;
                surveyModel.primaryDataHead.IndustryId = surveyModel.company.IndustryId;
                surveyModel.primaryDataHead.ModifyUser = surveyModel.UserId;
                surveyModel.primaryDataHead.CreateUser = surveyModel.UserId;
                
                await _primaryDataHeadRepository.AddAsync(surveyModel.primaryDataHead);

                //---------- Insert PrimaryDataHead Data ----------
                string xmlPrimaryData = "<ArrayOfPrimaryData>";
                foreach (PrimaryData item in surveyModel.primaryData)
                {
                    if (item.IndicatorId != 0)
                    {
                        xmlPrimaryData += "<PrimaryData>";

                        xmlPrimaryData += "<PrimaryHId>" + surveyModel.primaryDataHead.Id + "</PrimaryHId>";
                        xmlPrimaryData += "<Year>" + item.Year + "</Year>";
                        xmlPrimaryData += "<IndustryId>" + item.IndustryId + "</IndustryId>";
                        xmlPrimaryData += "<CountryId>" + item.CountryId + "</CountryId>";
                        xmlPrimaryData += "<CompanyId>" + surveyModel.company.Id + "</CompanyId>";
                        xmlPrimaryData += "<IndicatorId>" + item.IndicatorId + "</IndicatorId>";
                        xmlPrimaryData += "<Score>" + item.Score + "</Score>";
                        xmlPrimaryData += "<Remark>" + item.Remark + "</Remark>";
                        xmlPrimaryData += "<ImportFlag>I</ImportFlag>";
                        xmlPrimaryData += "<DropFlag>0</DropFlag>";
                        xmlPrimaryData += "<UserId>" + surveyModel.UserId + "</UserId>";

                        xmlPrimaryData += "</PrimaryData>";
                    }
                }
                xmlPrimaryData += "</ArrayOfPrimaryData>";
                await _queryRepository.SaveXMLPrimaryData(xmlPrimaryData);

                _uow.CommitTransaction();
            }
            catch (System.Exception e)
            {
                _uow.RollbackTransaction();
                throw;
            }
        }

    }
}
