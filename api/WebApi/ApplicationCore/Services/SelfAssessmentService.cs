﻿using ApplicationCore.Interfaces;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using System.Collections.Generic;
using Ardalis.GuardClauses;
using ApplicationCore.Specifications;
using Microsoft.AspNetCore.Mvc;
using ApplicationCore.ViewModels;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ApplicationCore.Exceptions;

namespace ApplicationCore.Services
{
    public class SelfAssessmentService : ISelfAssessmentService
    {
        private readonly IAppLogger<SelfAssessmentService> _logger;

        private readonly IAsyncRepository<Company> _companyRepository;
        private readonly IAsyncRepository<Assessment> _assessmentRepository;
        private readonly IAsyncRepository<AssessmentData> _assessmentDataRepository;
        private readonly IAsyncRepository<AssessmentBase> _assessmentBaseRepository;
        private readonly IQueryRepository _queryRepository;

        private IUnitOfWork _uow;

        public SelfAssessmentService(
            IUnitOfWork uow,
            IAsyncRepository<Company> companyRepository,
            IAsyncRepository<Assessment> assessmentRepository,
            IAsyncRepository<AssessmentData> assessmentDataRepository,
            IAsyncRepository<AssessmentBase> assessmentBaseRepository,
            IQueryRepository queryRepository,
            IAppLogger<SelfAssessmentService> logger)
        {
            _uow = uow;
            _companyRepository = companyRepository;
            _assessmentRepository = assessmentRepository;
            _assessmentDataRepository = assessmentDataRepository;
            _assessmentBaseRepository = assessmentBaseRepository;
            _queryRepository = queryRepository;
            _logger = logger;
        }

        //public async Task<IList<AssessmentBase>> GetAssessmentBaseByFilter(AssessmentBaseFilterModel filter)
        //{
        //    return await _assessmentBaseRepository.ListAsync(new AssessmentBaseFilterSpecification(filter));
        //}

        public async Task<SelfAssessmentModel> GetSelfAssessmentByUserIdAndYear(int UserId, int Year)
        {
            // - - - - - Assessment - - - - -
            Assessment assessment = (await _assessmentRepository.ListAsync(new AssessmentFilterSpecification(Year, UserId))).FirstOrDefault();
            Guard.Against.Null(UserId, assessment);

            // - - - - - Company - - - - -
            Company company = await _companyRepository.GetByIdAsync(assessment.CompanyId);

            // - - - - - Assessment Base - - - - -
            //AssessmentBaseFilterModel assessmentBaseFilter = new AssessmentBaseFilterModel() { Year = Year, IndustryId = assessment.IndustryId };
            //IList<AssessmentBase> assessmentBase = await _assessmentBaseRepository.ListAsync(new AssessmentBaseFilterSpecification(assessmentBaseFilter));
            IList<AssessmentBase> assessmentBase = await _queryRepository.GetAssessmentBase(assessment.IndustryId, Year);

            // - - - - - Assessment Data - - - - -
            IList<AssessmentData> assessmentData = await _assessmentDataRepository.ListAsync(new AssessmentDataFilterSpecification(assessment.Id));
            foreach (AssessmentData item in assessmentData)
            {
                item.AssNo = assessmentBase.Where(o => o.Id == item.AssessmentBaseId).FirstOrDefault().AssNo;
            }

            SelfAssessmentModel selfAssessment = new SelfAssessmentModel()
            {
                company = company,
                assessment = assessment,
                assessmentData = assessmentData
            };

            return selfAssessment;
        }

        public async Task SaveAsync(SelfAssessmentModel selfAssessmentModel)
        {
            if(selfAssessmentModel.UserId==0)
            throw new DataNotFoundException("User not found");

            try
            {
                _uow.BeginTransaction();

                //###################### Delete Data ######################
                //---------- Search Assessment ----------
                IList<Assessment> assessmentList = await _assessmentRepository.ListAsync(new AssessmentFilterSpecification(selfAssessmentModel.assessment.Year, selfAssessmentModel.UserId));
                Assessment assessment = assessmentList.FirstOrDefault();
                if(assessment != null)
                {
                    //---------- Delete Assessment Data ----------
                    await _queryRepository.DeleteAssessmentData(assessment.Id);

                    //---------- Delete Assessment ----------
                    await _assessmentRepository.DeleteAsync(assessment);

                    //---------- Delete Compnay ----------
                    Company company = await _companyRepository.GetByIdAsync(assessment.CompanyId);
                    await _companyRepository.DeleteAsync(company);
                    
                }

                //###################### Insert Data ######################

                //---------- Insert Compnay ----------
                selfAssessmentModel.company.ModifyUser = selfAssessmentModel.UserId;
                selfAssessmentModel.assessment.ModifyUser = selfAssessmentModel.UserId;

                await _companyRepository.AddAsync(selfAssessmentModel.company);

                //---------- Insert Assessment ----------
                selfAssessmentModel.assessment.CompanyId = selfAssessmentModel.company.Id;
                await _assessmentRepository.AddAsync(selfAssessmentModel.assessment);

                //---------- Insert Assessment Data ----------
                //AssessmentBaseFilterModel AssessmentBaseFilter = new AssessmentBaseFilterModel() { Year = selfAssessmentModel.assessment.Year, IndustryId = selfAssessmentModel.assessment.IndustryId };
                //IList<AssessmentBase> assessmentBase = await _assessmentBaseRepository.ListAsync(new AssessmentBaseFilterSpecification(AssessmentBaseFilter));
                IList<AssessmentBase> assessmentBase = await _queryRepository.GetAssessmentBase(selfAssessmentModel.assessment.IndustryId, selfAssessmentModel.assessment.Year);
                string xmlAssessmentData = "<ArrayOfAssessmentData>";
                foreach (AssessmentData item in selfAssessmentModel.assessmentData)
                {
                    if (item.AssNo != 0)
                    {

                        int AssessmentBaseId = assessmentBase.Where(o => o.AssNo == item.AssNo
                                                                   && o.IndustryId == selfAssessmentModel.assessment.IndustryId
                                                                   && o.Year == selfAssessmentModel.assessment.Year
                                                               ).FirstOrDefault().Id;

                        xmlAssessmentData += "<AssessmentData>";

                        xmlAssessmentData += "<AssessmentId>" + selfAssessmentModel.assessment.Id + "</AssessmentId>";
                        xmlAssessmentData += "<AssessmentBaseId>" + AssessmentBaseId + "</AssessmentBaseId>";
                        xmlAssessmentData += "<Score>" + item.Score + "</Score>";
                        xmlAssessmentData += "<UserId>" + selfAssessmentModel.UserId + "</UserId>";

                        xmlAssessmentData += "</AssessmentData>";
                    }
                }
                xmlAssessmentData += "</ArrayOfAssessmentData>";
                await _queryRepository.SaveXMLAssessmentData(xmlAssessmentData);
                
                _uow.CommitTransaction();
            }
            catch (System.Exception e)
            {
                _uow.RollbackTransaction();
                throw;
            }
        }

    }
}
