using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Interfaces;
using ApplicationCore.ViewModels;

namespace ApplicationCore.Services
{
    public class IndustryIndicatorService : IIndustryIndicatorService
    {
        private readonly IQueryRepository _queryRepository;
        private readonly IUnitOfWork _uow;
        public IndustryIndicatorService(IQueryRepository queryRepository, IUnitOfWork uow)
        {
            _uow = uow;
            _queryRepository = queryRepository;
        }
        public async Task<IList<IndustryIndicatorModel>> GetIndustryIndicator(int year)
        {
            var industryIndicators = (await _queryRepository.GetIndustryIndicatorModelList(year));
            foreach (var ii in industryIndicators)
            {
                var resurt = new List<IndustryIndicatorModel>()
                {
                    new IndustryIndicatorModel(){
                        Id = ii.Id,
                        Year = ii.Year,
                        Version = ii.Version,
                        CountSurvey = ii.CountSurvey,
                        CountSecondary = ii.CountSecondary,
                        AvgIndex = ii.AvgIndex,
                        UseFlag = ii.UseFlag
                    }
                };
            }
            return industryIndicators;
        }

        public async Task<IndustryIndicatorDataCountModel> GetIndustryIndicatorDataCount(int year)
        {
            var data = await _queryRepository.GetIndustryIndicatorDataCount(year);
            return new IndustryIndicatorDataCountModel()
            {
                Year = year,
                NextVersion = data.NextVersion,
                CntPri = data.CntSec,
                CntSec = data.CntPri,
            };
        }

        public async Task IndustryIndicatorCalculate(int year, string userId)
        {
            try
            {
                _uow.BeginTransaction();
                await _queryRepository.GetIndustryIndicatorCalculate(year, userId);
                _uow.CommitTransaction();
            }
            catch (System.Exception ex)
            {
                _uow.RollbackTransaction();
                throw;
            }
        }

        public async Task IndustryIndicatorUseSave(int year, int industryId)
        {
            try
            {
                _uow.BeginTransaction();
                await _queryRepository.IndustryIndicatorUseSave(year, industryId);
                _uow.CommitTransaction();
            }
            catch (System.Exception ex)
            {
                _uow.RollbackTransaction();
                throw;
            }
        }
    }
}