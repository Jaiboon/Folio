﻿using ApplicationCore.Interfaces;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using System.Collections.Generic;
using Ardalis.GuardClauses;
using ApplicationCore.Specifications;
using Microsoft.AspNetCore.Mvc;
using ApplicationCore.ViewModels;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ApplicationCore.Exceptions;

namespace ApplicationCore.Services
{
    public class PMIService : IPMIService
    {
        private readonly IAppLogger<PMIService> _logger;


        private readonly IAsyncRepository<PMIHEAD> _PMIRepository;
        private readonly IAsyncRepository<PMIDATA> _PMIDataRepository;
        private readonly IQueryRepository _queryRepository;

        private IUnitOfWork _uow;

        public PMIService(
            IUnitOfWork uow,
            IAsyncRepository<PMIHEAD> PMIRepository,
            IAsyncRepository<PMIDATA> PMIDataRepository,
            IQueryRepository queryRepository,
            IAppLogger<PMIService> logger)
        {
            _uow = uow;

            _PMIRepository = PMIRepository;
            _PMIDataRepository = PMIDataRepository;
            _queryRepository = queryRepository;
            _logger = logger;
        }




        public async Task SavePMI(PMIModel PMIModel)
        {
            // if (PMIModel.UserId == 0)
            //     throw new DataNotFoundException("User not found");

            try
            {
                _uow.BeginTransaction();

                //###################### Insert Data ######################

                //---------- Insert PMI ----------
                PMIModel.PMIHEAD.ModifyUser = PMIModel.UserId;
                

                await _PMIRepository.AddAsync(PMIModel.PMIHEAD);

                //---------- Insert PMI DATA ----------
                foreach (PMIDATA item in PMIModel.PMIData)
                {
                        item.ModifyUser = PMIModel.UserId;
                        item.PMI_ID = PMIModel.PMIHEAD.Id;
                        await _PMIDataRepository.AddAsync(item);
                  
                }
              


                _uow.CommitTransaction();
            }
            catch (System.Exception e)
            {
                _uow.RollbackTransaction();
                throw;
            }
        }

    }
}
