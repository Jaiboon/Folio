﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Services
{
    public class SkillService : ISkillService
    {
        private readonly IAsyncRepository<Skill> _iAsyncRepository;
        public SkillService(IAsyncRepository<Skill> iAsyncRepository)
        {
            _iAsyncRepository = iAsyncRepository;
        }

        public Task CreateAsync(Skill skill)
        {
            return _iAsyncRepository.AddAsync(skill);
        }

        public Task DeleteAsync(Skill skill)
        {
            return _iAsyncRepository.DeleteAsync(skill);
        }

        public async Task<Skill> Get(int id)
        {
            return await _iAsyncRepository.GetByIdAsync(id);
        }

        public async Task<IList<Skill>> GetListAsync()
        {
            return await _iAsyncRepository.ListAllAsync();
        }

        public Task UpdateAsync(Skill skill)
        {
            return _iAsyncRepository.UpdateAsync(skill);
        }
    }
}
