﻿using ApplicationCore.Entities;
using ApplicationCore.ViewModels;
using System;

namespace ApplicationCore.Specifications
{
    public class PrimaryDataHeadFilterSpecification : BaseSpecification<PrimaryDataHead>
    {
        public PrimaryDataHeadFilterSpecification(int year, int userId)
            : base(
                  p => p.Year == year
                    && p.CreateUser == userId
            )
        {
        }

    }
}
