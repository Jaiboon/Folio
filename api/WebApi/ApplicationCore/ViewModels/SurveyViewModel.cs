using ApplicationCore.Entities;
using System;
using System.Collections.Generic;

namespace ApplicationCore.ViewModels
{
    public class SurveyViewModel
    {
        public int UserId { get; set; }
        public Company company { get; set; }
        public PrimaryDataHead primaryDataHead { get; set; }
        public IList<PrimaryData> primaryData { get; set; }
    }
}