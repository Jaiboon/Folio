using ApplicationCore.Entities;
using System;
using System.Collections.Generic;

namespace ApplicationCore.ViewModels
{
    public class PMIModel
    {
        public int UserId { get; set; }
        public PMIHEAD PMIHEAD { get; set; }
        public IList<PMIDATA> PMIData { get; set; }
    }
}