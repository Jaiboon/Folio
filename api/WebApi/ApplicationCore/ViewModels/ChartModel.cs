using ApplicationCore.Dtos;
using ApplicationCore.ViewModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApplicationCore.Entities
{
    public class DataModel : Entity
    {
        public IList<double> Data { get; set; }
        public string Label { get; set; }
    }

    public class ChartDataModel
    {
        public IList<DataModel> Data { get; set; }
        public IList<string> Labels { get; set; }
    }

    public class AppSettingViewModel
    {
        public int[] Year { get; set; }
        public IList<ListViewModel> Pillar { get; set; }
        public IList<ListViewModel> Country { get; set; }
        public IList<ListViewModel> Industry { get; set; }
        public IList<IndustryIndicatorModel> IndustryIndicator { get; set; }
        public IList<IndustryCountryListViewModel> IndustryCountry { get; set; }
    }


    public class Entity
    {
        public int Id { get; set; }
    }

    public class IndexScoreModel
    {
        public double Base_Score { get; set; }
        public string Index_Name { get; set; }

        public string DisplayFlag { get; set; }


    }

    public class IndustryReviewModel
    {
        public int Id { get; set; }

        public int Year { get; set; }

        public string IndustryName { get; set; }
        public string IndustryNameThai { get; set; }

        public string CountryName { get; set; }
        public string CountryNameThai { get; set; }

        public string HtmlInformation { get; set; }
    }

    public class AssessmentScoreModel
    {
        // public double ID { get; set; }
        // public string Year { get; set; }
        // public string Industry_ID  { get; set; }
        // public string Ass_No  { get; set; }
        public double BaseScore { get; set; }
        public double MyScore { get; set; }
        public string IndexName { get; set; }
    }


    public class IndustryGroupModel
    {
        public int Groupid { get; set; }
        public string Title { get; set; }
        public double Value2 { get; set; }
        public IList<GroupOfCountryModel> GroupOfCountryItems { get; set; }

    }
    public class GroupOfCountryModel
    {
        public int Groupid { get; set; }
        public string Title { get; set; }
        public double Value2 { get; set; }
        public IList<CountryIndicatorModel> CountryIndicatorItems { get; set; }
    }
    public class CountryIndicatorModel
    {
        public string title { get; set; }
        public double value2 { get; set; }
    }

    public class TransIndiGraphModel
    {
        public string Indicator_Name { get; set; }
        public int GraphPos { get; set; }
        public int Year { get; set; }
        public double Score { get; set; }
    }
    
}