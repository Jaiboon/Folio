using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Interfaces;
using ApplicationCore.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace OIE.Api.Controllers
{
    [Route("api/IndustryIndicator")]
    public class IndustryIndicatorController : Controller
    {
        private readonly IIndustryIndicatorService _iIndustryIndicator;
        public IndustryIndicatorController(IIndustryIndicatorService iIndustryIndicator)
        {
            _iIndustryIndicator = iIndustryIndicator;

        }

        [HttpGet("get/{year}")]
        public async Task<IList<IndustryIndicatorModel>> GetIndustryIndicator(int year)
        {
            return await _iIndustryIndicator.GetIndustryIndicator(year);
        }

        [HttpGet("{year}")]
        public async Task<IndustryIndicatorDataCountModel> GetIndustryIndicatorDataCount(int year)
        {
            return await _iIndustryIndicator.GetIndustryIndicatorDataCount(year);
        }

        [HttpPost("{year}/{userid}")]
        public async Task GetIndustryIndicatorCalculate(int year, string userid)
        {
            await _iIndustryIndicator.IndustryIndicatorCalculate(year, userid);
        }

        [HttpPut("{year}/{industryId}")]
        public async Task IndustryIndicatorSave(int year, int industryId)
        {
             await _iIndustryIndicator.IndustryIndicatorUseSave(year, industryId);
        }

        
    }
}