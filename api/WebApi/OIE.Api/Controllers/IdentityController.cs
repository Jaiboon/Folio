﻿using IdentityModel;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ApplicationCore.Interfaces;
using Infrastructure.Identity;
using OIE.Api.Models.AccountViewModels;
using ApplicationCore.Entities;
using OIE.Api.Extensions;

namespace OIE.Api.Controllers
{
    /// <summary>
    /// Identity Web API controller.
    /// </summary>
    [Route("api/[controller]")]
    // Authorization policy for this API.
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme, Policy = "Manage Accounts")]
    public class IdentityController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;

        public IdentityController(
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ILogger<IdentityController> logger)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
        }
        /// <summary>
        /// Gets all the users.
        /// </summary>
        /// <returns>Returns all the users</returns>
        // GET api/identity/GetAll
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll(string roleName)
        {
            var role = await _roleManager.FindByNameAsync(roleName);
            var users = await _userManager.GetUsersInRoleAsync(role.Name);

            return new JsonResult(users);
        }


        [HttpGet("Get")]
        public async Task<IActionResult> Get(string Id)
        {
            System.Console.WriteLine(Id);
            if (Id == "changepassword")
            {
                Id = User.GetId().ToString();
            }
            System.Console.WriteLine(Id);
            var user = await _userManager.FindByIdAsync(Id);
            var roles = await _userManager.GetRolesAsync(user);
            var userModel = new UserViewModel()
            {
                id = user.Id.ToString(),
                firstname = user.Firstname,
                lastname = user.Lastname,
                username = user.UserName
            };

            userModel.IsAdministrator = roles.Contains(ApplicationRole.Administrator);
            userModel.IsOIEOfficer = roles.Contains(ApplicationRole.OIEOfficer);
            userModel.IsCompanyUser = roles.Contains(ApplicationRole.RegisteredUser);

            return new JsonResult(userModel);
        }

        /// <summary>
        /// Registers a new user.
        /// </summary>
        /// <returns>IdentityResult</returns>
        // POST: api/identity/Create
        [HttpPost("Create")]
        [AllowAnonymous]
        public async Task<IActionResult> Create([FromBody]UserViewModel model)
        {
            if (!model.IsAdministrator && !model.IsCompanyUser && !model.IsOIEOfficer)
                model.IsCompanyUser = true;

            if (model.id == null)
            {

                var user = await _userManager.FindByNameAsync(model.username);
                if (user == null)
                {
                    user = new ApplicationUser
                    {
                        Firstname = model.firstname,
                        Lastname = model.lastname,
                        AccessFailedCount = 0,
                        Email = model.username,
                        EmailConfirmed = false,
                        LockoutEnabled = true,
                        NormalizedEmail = model.username.ToUpper(),
                        NormalizedUserName = model.username.ToUpper(),
                        TwoFactorEnabled = false,
                        UserName = model.username
                    };
                    var result = await _userManager.CreateAsync(user, model.password);

                    if (result.Succeeded)
                    {
                        if (model.IsAdministrator)
                            await addToRole(model.username, ApplicationRole.Administrator);
                        if (model.IsCompanyUser)
                            await addToRole(model.username, ApplicationRole.RegisteredUser);
                        if (model.IsOIEOfficer)
                            await addToRole(model.username, ApplicationRole.OIEOfficer);

                        //await addClaims(model.username);
                    }
                    return new JsonResult(result);
                }
                return BadRequest("User already exists");
            }
            else
            {
                var user = await _userManager.FindByIdAsync(model.id.ToString());
                user.Firstname = model.firstname;
                user.Lastname = model.lastname;
                var result = await _userManager.UpdateAsync(user);
                await _userManager.RemovePasswordAsync(user);
                await _userManager.AddPasswordAsync(user, model.password);

                if (result.Succeeded)
                {

                    if (model.IsAdministrator)
                        await _userManager.AddToRoleAsync(user, ApplicationRole.Administrator);
                    else
                        await _userManager.RemoveFromRoleAsync(user, ApplicationRole.Administrator);


                    if (model.IsOIEOfficer)
                        await _userManager.AddToRoleAsync(user, ApplicationRole.OIEOfficer);
                    else
                        await _userManager.RemoveFromRoleAsync(user, ApplicationRole.OIEOfficer);


                    if (model.IsCompanyUser)
                        await _userManager.AddToRoleAsync(user, ApplicationRole.RegisteredUser);
                    else
                        await _userManager.RemoveFromRoleAsync(user, ApplicationRole.RegisteredUser);

                }
                return new JsonResult(result);
            }
        }

        /// <summary>
        /// Deletes a user.
        /// </summary>
        /// <returns>IdentityResult</returns>
        // POST: api/identity/Delete
        [HttpPost("Delete")]
        public async Task<IActionResult> Delete([FromBody]string username)
        {
            var user = await _userManager.FindByNameAsync(username);

            var result = await _userManager.DeleteAsync(user);

            return new JsonResult(result);
        }

        private async Task addToRole(string userName, string roleName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            await _userManager.AddToRoleAsync(user, roleName);
        }

        private async Task addClaims(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            var claims = new List<Claim> {
                new Claim(type: JwtClaimTypes.GivenName, value: user.Firstname),
                new Claim(type: JwtClaimTypes.FamilyName, value: user.Lastname),
            };
            await _userManager.AddClaimsAsync(user, claims);
        }
    }
}