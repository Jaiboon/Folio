using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.Services;
using Infrastructure.Data;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using ApplicationCore.Dtos;
using Ardalis.GuardClauses;
using ApplicationCore.ViewModels;
using System.IO;
using OIE.Api.Models;
using ApplicationCore;
using System;

namespace OIE.Api.Controllers
{
    [Route("api/[controller]")]
    public class SettingController : Controller
    {
        private readonly IReportService _repository;
        public SettingController(IReportService repository, IQueryRepository queryRepository)
        {
            _repository = repository;
        }

        [HttpGet("app-setting")]
        public async Task<AppSettingViewModel> GetFilter()
        {
            var result = await _repository.GetAppSettingAsync();
            return result;
        }


        [HttpGet("about")]
        public async Task<IActionResult> Get()
        {
            try
            {
                using (StreamReader sr = new StreamReader(ServerUtils.ConfigInfo.MediaFolder + "/home.html", System.Text.Encoding.UTF8))
                {
                    string line = await sr.ReadToEndAsync();
                    return Ok(new PageViewModel() { Html = line });
                }
            }
            catch { }
            return Ok(new PageViewModel());
        }


        [HttpPost("about")]
        public async Task<IActionResult> Post([FromBody]PageViewModel page)
        {
            try
            {
                using (StreamWriter outputFile = new StreamWriter(ServerUtils.ConfigInfo.MediaFolder + "/home.html"))
                {
                    outputFile.WriteLine(page.Html);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}