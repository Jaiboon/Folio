﻿using ApplicationCore.Entities;
using ApplicationCore.Exceptions;
using ApplicationCore.Interfaces;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OIE.Api.Extensions;
using ApplicationCore.ViewModels;

namespace OIE.Api.Controllers
{
    /// <summary>
    /// Resources Web API controller.
    /// </summary>
    [Route("api/[controller]")]
    // Authorization policy for this API.
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme, Policy = "Access Resources")]
    public class PMIController : Controller
    {
        private readonly IPMIService _PMIService;
        private readonly ILogger _logger;

        public PMIController(
           IPMIService PMIService,
           ILogger<IdentityController> logger)
        {
            _PMIService = PMIService;
            _logger = logger;
        }




        [HttpPost]
        public async Task<IActionResult> SavePMI([FromBody]PMIModel PMI)
        {
            try
            {
                await _PMIService.SavePMI(PMI);
                return Ok();
            }
            catch (DataNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
