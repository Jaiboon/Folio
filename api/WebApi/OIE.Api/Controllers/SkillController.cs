using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace OIE.Api.Controllers
{
    [Route("api/skill")]
    public class SkillController : Controller
    {
        private readonly ISkillService _iSkillService;
        public SkillController(ISkillService iSkillService)
        {
            _iSkillService = iSkillService;

        }

         /// <summary>
        /// สร้างทักษะ ความสามารถ ความถนัด
        /// </summary>
        [HttpPost]
        public Task Create(Skill Skills)
        {
            return _iSkillService.CreateAsync(Skills);
        }

        /// <summary>
        /// แก้ไขทักษะ ความสามารถ ความถนัด
        /// </summary>
        [HttpPut]
        public Task Update(Skill Skills)
        {
            return _iSkillService.UpdateAsync(Skills);
        }

         /// <summary>
        /// แสดงทักษะ ความสามารถ ความถนัด ทั้งหมดในแต่ละด้าน
        /// </summary>
        [HttpGet]
        public Task<IList<Skill>> GetAll()
        {
            return _iSkillService.GetListAsync();
        }

        /// <summary>
        /// แสดงทักษะ ความสามารถ ความถนัด ในแต่ละด้าน ด้วย Id ที่ต้องการค้นหา
        /// </summary>
        [HttpGet("{id}")]
        public async Task<Skill> GetSkillById(int id)
        {
            return await _iSkillService.Get(id);
        }

        /// <summary>
        /// ลบทักษะ ความสามารถ ความถนัด ในแต่ละด้าน ด้วย Id ที่ต้องการ
        /// </summary>
        [HttpDelete]
        public Task Delete(Skill Skills)
        {
            return _iSkillService.DeleteAsync(Skills);
        }

    }
}