﻿using ApplicationCore.Entities;
using ApplicationCore.Exceptions;
using ApplicationCore.Interfaces;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OIE.Api.Extensions;
using ApplicationCore.ViewModels;

namespace OIE.Api.Controllers
{
    /// <summary>
    /// Resources Web API controller.
    /// </summary>
    [Route("api/[controller]")]
    // Authorization policy for this API.
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme, Policy = "Access Resources")]
    public class SelfAssessmentController : Controller
    {
        private readonly ISelfAssessmentService _SelfAssessmentService;
        private readonly ILogger _logger;

        public SelfAssessmentController(
           ISelfAssessmentService SelfAssessmentService,
           ILogger<IdentityController> logger)
        {
            _SelfAssessmentService = SelfAssessmentService;
            _logger = logger;
        }
         
        [HttpGet]
        public async Task<IActionResult> GetSelfAssessment(int UserId, int Year)
        {
            try
            {
                return Ok(await _SelfAssessmentService.GetSelfAssessmentByUserIdAndYear(UserId, Year));
            }
            catch (DataNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> SaveSelfAssessment([FromBody]SelfAssessmentModel SelfAssessment)
        {
            try
            {
                await _SelfAssessmentService.SaveAsync(SelfAssessment);
                return Ok();
            }
            catch (DataNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
