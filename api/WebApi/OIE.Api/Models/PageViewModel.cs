﻿using System;

namespace OIE.Api.Models
{
    public class PageViewModel
    {
        public string PageTitle { get; set; }
        public string Html { get; set; }
    }
}
